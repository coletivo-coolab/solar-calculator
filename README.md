# Calculadora Solar

## Iniciando o projeto
```
yarn install
```

ou 

```
npm i
```

### Compilar a auto-recarregadar o projeto

Update the `.env` file with your defaults.

```
cp example.env .env
yarn dev
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
