const fs = require("fs");
const path = require("path");
const scrapeIt = require("scrape-it");
const slugify = require("slugify");
const throttle = require("lodash.throttle");

const basePage = "https://openwrt.org/toh";

async function scrapeRouterList() {
  return scrapeIt(`${basePage}/start`, {
    routers: {
      listItem: "tr",
      name: "routers",
      data: {
        brand: ".brand",
        model: ".model",
        page: {
          selector: ".device_page a",
          attr: "href"
        },
        techData: {
          selector: ".device_techdata a",
          attr: "href"
        }
      }
    }
  })
    .then(({ data, response }) => {
      console.log(`Status Code: ${response.statusCode}`);
      const filtered = data.routers.filter(i => i.page.length > 0);
      return filtered.map(i => {
        if (!i.techData.split("/toh/hwdata/")[1]) return;
        i.routerPath = i.techData
          .split("/toh/hwdata/")[1]
          .split("/")
          .join("~");
        return i;
      });
    })
    .catch(err => {
      console.log("Error", err);
    });
}

async function scrapeRouterData(router) {
  const url = `${basePage}/hwdata/${router}`;
  return scrapeIt(url, {
    name: "h1",
    brand: ".brand",
    model: ".model",
    version: ".version",
    powerSupply: ".power_supply"
  })
    .then(({ data, response }) => {
      const name = data.name.split("OpenWrt ProjectTechdata: ")[1];
      const brand = data.brand.split("Brand: ")[1];
      const model = data.model.split("Model: ")[1];
      const versions = data.version.split("Version: ")[1];
      const id = `${slugify(brand)}--${slugify(model)}`;
      let type = "router";
      if (brand === "Raspberry Pi Foundation") type = "single-board-computer";
      const powerSupplyData = data.powerSupply
        .split("Power Supply: ")[1]
        .split(",");
      const powerSupply = {
        volts: parseFloat(powerSupplyData[0].split(" ")[0]),
        amps: parseFloat(powerSupplyData[1].split(" ")[1])
      };
      console.log(`${name}: Status Code: ${response.statusCode}`);
      const formatedData = {
        name,
        brand,
        model,
        versions,
        powerSupply,
        url,
        id,
        type
      };
      if (!powerSupply.volts || !powerSupply.amps) return;
      return formatedData;
    })
    .catch(err => {
      console.log("Err on fetching router data", err);
    });
}

async function allRouters() {
  const list = {
    ["Libre Router"]: [
      {
        name: "Libre Router v1",
        model: "Libre Router v1",
        brand: "Libre Router",
        version: "1",
        powerSupply: {
          volts: 12,
          amps: 2
        },
        url: "http://librerouter.org/",
        id: `Libre-Router--Libre-Router-v1`,
        type: "router"
      }
    ],
    ["TP Link"]: [
      {
        name: "WDR 3500",
        model: "WDR 3500",
        brand: "TP Link",
        version: "1",
        powerSupply: {
          volts: 12,
          amps: 1
        },
        url: "/toh/hwdata/tp-link/tp-link_tl-wdr3500_v1",
        id: `TP-Link--WDR-3500`,
        type: "router"
      },
      {
        name: "WDR 3600",
        model: "WDR 3600",
        brand: "TP Link",
        version: "1",
        powerSupply: {
          volts: 12,
          amps: 1.5
        },
        id: `TP-Link--WDR-3600`,
        type: "router"
      },
      {
        name: "WDR 4300",
        model: "WDR 4300",
        brand: "TP Link",
        version: "1",
        powerSupply: {
          volts: 12,
          amps: 1.5
        },
        id: `TP-Link--WDR-4300`,
        type: "router"
      },
      {
        name: "Archer C7",
        model: "Archer C7",
        brand: "TP Link",
        version: "1",
        powerSupply: {
          volts: 12,
          amps: 1.5
        },
        id: `TP-Link--Archer-C7`,
        type: "router"
      },
      {
        name: "CPE 210",
        model: "CPE 210",
        brand: "TP Link",
        version: "2",
        powerSupply: {
          volts: 24,
          amps: 0.6
        },
        id: `TP-Link--CPE-210`,
        type: "router"
      },
      {
        name: "CPE 510",
        model: "CPE 510",
        brand: "TP Link",
        version: "1",
        powerSupply: {
          volts: 24,
          amps: 0.6
        },
        id: `TP-Link--CPE-510`,
        type: "router"
      }
    ],
    Ubiquiti: [
      {
        name: "LiteBeam",
        model: "LiteBeam",
        brand: "Ubiquiti",
        version: "1",
        powerSupply: {
          volts: 24,
          amps: 0.3
        },
        url: "https://www.ui.com/airmax/litebeam-m5/",
        id: `Ubiquiti--LiteBeam`,
        type: "router"
      },
      {
        name: "Rocket",
        model: "Rocket",
        brand: "Ubiquiti",
        version: "1",
        powerSupply: {
          volts: 24,
          amps: 0.35
        },
        url: "https://www.ui.com/airmax/rocket-ac/",
        id: `Ubiquiti--Rocket`,
        type: "router"
      }
    ]
  };
  if (process.env.SCRAPE) {
    let routers = await scrapeRouterList();
    if (process.env.TEST) {
      const testNum =
        typeof process.env.TEST === "number" ? process.env.TEST : 5;
      routers = routers.slice(0, testNum);
    }

    if (process.env.SYNC) {
      console.log("Fetching data in synchronous manner");
      for await (let router of routers) {
        try {
          const routerUrl = router.routerPath.split("~").join("/");
          const data = await scrapeRouterData(routerUrl);
          if (data) {
            if (list[data.brand]) list[data.brand].push(data);
            else list[data.brand] = [data];
          }
        } catch (err) {
          console.log("Error on getting router data");
        }
      }
    } else {
      await Promise.all(
        routers.map(async router => {
          try {
            const routerUrl = router.routerPath.split("~").join("/");
            const slowScrape = throttle(scrapeRouterData, 1000);
            const data = await slowScrape(routerUrl);
            if (data) {
              if (list[data.brand]) list[data.brand].push(data);
              else list[data.brand] = [data];
            }
          } catch (err) {
            console.log("Error on getting router data:", err);
          }
        })
      );
    }
  }
  return list;
}

const storeData = async (data, path) => {
  try {
    await fs.writeFileSync(path, JSON.stringify(data));
  } catch (err) {
    console.error(err);
  }
};

async function run() {
  console.log("Starting!");
  const routerData = await allRouters();
  console.log("Done getting router data!");
  const dataDir = path.resolve(__dirname, "../src/assets/data/routers.json");
  await storeData(routerData, dataDir);
  console.log("Wrote to ", dataDir);
  console.log("Done!");
}

run();
