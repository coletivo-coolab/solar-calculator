module.exports = [
  {
    brand: "LibreRouter",
    model: "LibreRouter v1",
    page: "https://librerouter.org",
    techData: "https://librerouter.org",
    routerPath: "3com~3com_3crwer100-75"
  },
  {
    brand: "3Com",
    model: "3CRWER100-75",
    page: "/toh/3com/3crwer100_75",
    techData: "/toh/hwdata/3com/3com_3crwer100-75",
    routerPath: "3com~3com_3crwer100-75"
  },
  {
    brand: "4G Systems",
    model: "AccessCube (MeshCube)",
    page: "/toh/4g.systems/access.cube",
    techData: "/toh/hwdata/4gsystems/4gsystems_accesscube",
    routerPath: "4gsystems~4gsystems_accesscube"
  },
  {
    brand: "7Links",
    model: "PX-4885",
    page: "/toh/7links/px4885",
    techData: "/toh/hwdata/7links/7links_px-4885",
    routerPath: "7links~7links_px-4885"
  },
  {
    brand: "8devices",
    model: "Carambola 1",
    page: "/toh/8devices/carambola",
    techData: "/toh/hwdata/8devices/8devices_carambola1",
    routerPath: "8devices~8devices_carambola1"
  },
  {
    brand: "8devices",
    model: "Carambola 2",
    page: "/toh/8devices/carambola2",
    techData: "/toh/hwdata/8devices/8devices_carambola2",
    routerPath: "8devices~8devices_carambola2"
  },
  {
    brand: "Abicom International",
    model: "Freedom CPE",
    page: "/toh/abicom/freedom_cpe",
    techData: "/toh/hwdata/abicominternational/abicominternational_freedomcpe",
    routerPath: "abicominternational~abicominternational_freedomcpe"
  },
  {
    brand: "Abicom International",
    model: "Scorpion SC450",
    page: "/toh/abicom/scorpion450",
    techData:
      "/toh/hwdata/abicominternational/abicominternational_scorpion_sc450",
    routerPath: "abicominternational~abicominternational_scorpion_sc450"
  },
  {
    brand: "Abicom International",
    model: "Scorpion SC1750",
    page: "/toh/abicom/scorpion1750",
    techData:
      "/toh/hwdata/abicominternational/abicominternational_scorpion_sc1750",
    routerPath: "abicominternational~abicominternational_scorpion_sc1750"
  },
  {
    brand: "Abicom International",
    model: "Scorpion SC300M",
    page: "/toh/abicom/scorpion300m",
    techData:
      "/toh/hwdata/abicominternational/abicominternational_scorpion_sc300m",
    routerPath: "abicominternational~abicominternational_scorpion_sc300m"
  },
  {
    brand: "Accton",
    model: "MR3201A",
    page: "/toh/fon/fonera",
    techData: "/toh/hwdata/accton/accton_mr3201a",
    routerPath: "accton~accton_mr3201a"
  },
  {
    brand: "Actiontec",
    model: "GT701",
    page: "/toh/actiontec/gt701d",
    techData: "/toh/hwdata/actiontec/actiontec_gt701",
    routerPath: "actiontec~actiontec_gt701"
  },
  {
    brand: "Actiontec",
    model: "GT704WG",
    page: "/toh/actiontec/gt704wg",
    techData: "/toh/hwdata/actiontec/actiontec_gt704wg_1a",
    routerPath: "actiontec~actiontec_gt704wg_1a"
  },
  {
    brand: "Actiontec",
    model: "GT784WNV",
    page: "/toh/actiontec/gt784wnv",
    techData: "/toh/hwdata/actiontec/actiontec_gt784wnv",
    routerPath: "actiontec~actiontec_gt784wnv"
  },
  {
    brand: "Actiontec",
    model: "MI424WR",
    page: "/toh/actiontec/mi424wr",
    techData: "/toh/hwdata/actiontec/actiontec_mi424wr_a",
    routerPath: "actiontec~actiontec_mi424wr_a"
  },
  {
    brand: "Actiontec",
    model: "MI424WR",
    page: "/toh/actiontec/mi424wr",
    techData: "/toh/hwdata/actiontec/actiontec_mi424wr_c",
    routerPath: "actiontec~actiontec_mi424wr_c"
  },
  {
    brand: "Actiontec",
    model: "MI424WR",
    page: "/toh/actiontec/mi424wr",
    techData: "/toh/hwdata/actiontec/actiontec_mi424wr_d",
    routerPath: "actiontec~actiontec_mi424wr_d"
  },
  {
    brand: "ADB",
    model: "P.DG A4001N1",
    page: "/toh/adb/p.dg_a4001n1",
    techData: "/toh/hwdata/adb/adb_pdga4001n1",
    routerPath: "adb~adb_pdga4001n1"
  },
  {
    brand: "ADB",
    model: "P.DG AV4202N",
    page: "/toh/adb/p.dg_av4202n",
    techData: "/toh/hwdata/adb/adb_pdgav4202n",
    routerPath: "adb~adb_pdgav4202n"
  },
  {
    brand: "ADI Engineering",
    model: "Pronghorn SBC250",
    page: "/toh/adi_engineering/adi_engineering_pronghorn_sbc250",
    techData: "/toh/hwdata/adi_engineering/adi_engineering_pronghorn_sbc250",
    routerPath: "adi_engineering~adi_engineering_pronghorn_sbc250"
  },
  {
    brand: "Afoundry",
    model: "EW1200",
    page: "/toh/afoundry/afoundry_ew1200",
    techData: "/toh/hwdata/afoundry/afoundry_ew1200",
    routerPath: "afoundry~afoundry_ew1200"
  },
  {
    brand: "Agestar",
    model: "NSB3AS",
    page: "/toh/agestar/nsb3as",
    techData: "/toh/hwdata/agestar/agestar_nsb3as",
    routerPath: "agestar~agestar_nsb3as"
  },
  {
    brand: "Airlink101",
    model: "AR725W",
    page: "/toh/airlink101/ar725w",
    techData: "/toh/hwdata/airlink101/airlink101_ar725w",
    routerPath: "airlink101~airlink101_ar725w"
  },
  {
    brand: "Airlink101",
    model: "AR430W",
    page: "/toh/d-link/dir-300",
    techData: "/toh/hwdata/airlink101/airlink101_ar430w",
    routerPath: "airlink101~airlink101_ar430w"
  },
  {
    brand: "Airlink101",
    model: "AR670W",
    page: "/toh/airlink101/ar670w",
    techData: "/toh/hwdata/airlink101/airlink101_ar670w",
    routerPath: "airlink101~airlink101_ar670w"
  },
  {
    brand: "Alcatel-Sbell",
    model: "RG100A-AA",
    page: "/toh/alcatel-sbell/rg100a-aa",
    techData: "/toh/hwdata/alcatel-sbell/alcatel-sbell_rg100a-aa",
    routerPath: "alcatel-sbell~alcatel-sbell_rg100a-aa"
  },
  {
    brand: "ALFA Network",
    model: "Tube2H",
    page: "/toh/alfa_network/alfa_network_tube2hp",
    techData: "/toh/hwdata/alfa_network/alfa_network_tube2h",
    routerPath: "alfa_network~alfa_network_tube2h"
  },
  {
    brand: "ALFA Network",
    model: "Tube2HP",
    page: "/toh/alfa_network/alfa_network_tube2hp",
    techData: "/toh/hwdata/alfa_network/alfa_network_tube2hp",
    routerPath: "alfa_network~alfa_network_tube2hp"
  },
  {
    brand: "ALFA Network",
    model: "W502U / R36",
    page: "/toh/alfa_network/w502u",
    techData: "/toh/hwdata/alfa_network/alfanetwork_w502u",
    routerPath: "alfa_network~alfanetwork_w502u"
  },
  {
    brand: "ALFA Network",
    model: "N5",
    page: "/toh/alfa_network/n5",
    techData: "/toh/hwdata/alfa_network/alfanetwork_n5",
    routerPath: "alfa_network~alfanetwork_n5"
  },
  {
    brand: "ALFA Network",
    model: "N2",
    page: "/toh/alfa_network/n2",
    techData: "/toh/hwdata/alfa_network/alfanetwork_n2",
    routerPath: "alfa_network~alfanetwork_n2"
  },
  {
    brand: "ALFA Network",
    model: "Hornet-UB / AP121 / AP121U",
    page: "/toh/alfa_network/hornet-ub",
    techData: "/toh/hwdata/alfa_network/alfanetwork_hornetub",
    routerPath: "alfa_network~alfanetwork_hornetub"
  },
  {
    brand: "ALFA Network",
    model: "AP121",
    page: "/toh/alfa_network/ap121",
    techData: "/toh/hwdata/alfa_network/alfanetwork_ap121",
    routerPath: "alfa_network~alfanetwork_ap121"
  },
  {
    brand: "ALFA Network",
    model: "AP96",
    page: "/toh/alfa_network/ap96",
    techData: "/toh/hwdata/alfa_network/alfanetwork_ap96",
    routerPath: "alfa_network~alfanetwork_ap96"
  },
  {
    brand: "ALFA Network",
    model: "AP120C",
    page: "/toh/alfa_network/ap120c",
    techData: "/toh/hwdata/alfa_network/alfanetwork_ap120c",
    routerPath: "alfa_network~alfanetwork_ap120c"
  },
  {
    brand: "ALFA Network",
    model: "Tube2HP-F",
    page: "/toh/alfa_network/alfa_network_tube2hp",
    techData: "/toh/hwdata/alfa_network/alfa_network_tube2hp-f",
    routerPath: "alfa_network~alfa_network_tube2hp-f"
  },
  {
    brand: "ALFA Network",
    model: "Hornet-UB x2",
    page: "/toh/alfa_network/hornet-ub",
    techData: "/toh/hwdata/alfa_network/alfa_network_hornet-ub_x2",
    routerPath: "alfa_network~alfa_network_hornet-ub_x2"
  },
  {
    brand: "Allnet",
    model: "ALL0305",
    page: "/toh/allnet/all0305",
    techData: "/toh/hwdata/allnet/allnet_all0305",
    routerPath: "allnet~allnet_all0305"
  },
  {
    brand: "Allnet",
    model: "ALL0239-3G",
    page: "/toh/allnet/all0239-3g",
    techData: "/toh/hwdata/allnet/allnet_all0239-3g",
    routerPath: "allnet~allnet_all0239-3g"
  },
  {
    brand: "Allnet",
    model: "ALL0256N",
    page: "/toh/allnet/all0256n",
    techData: "/toh/hwdata/allnet/allnet_all0256n",
    routerPath: "allnet~allnet_all0256n"
  },
  {
    brand: "Allnet",
    model: "ALL0258N",
    page: "/toh/allnet/all0258n",
    techData: "/toh/hwdata/allnet/allnet_all0258n",
    routerPath: "allnet~allnet_all0258n"
  },
  {
    brand: "Alpha Networks",
    model: "ASL-26555",
    page: "/toh/alpha/asl26555",
    techData: "/toh/hwdata/alphanetworks/alphanetworks_asl-26555",
    routerPath: "alphanetworks~alphanetworks_asl-26555"
  },
  {
    brand: "ARC Flex",
    model: "FreeStation 5",
    page: "/toh/arc/freestation",
    techData: "/toh/hwdata/arcflex/arcflex_freestation5_v1",
    routerPath: "arcflex~arcflex_freestation5_v1"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV7510PW",
    page: "/toh/arcadyan/arv7510pw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv7510pw",
    routerPath: "arcadyanastoria~arcadyanastoria_arv7510pw"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "AR7516 (Orange / EE Bright Box)",
    page: "/toh/arcadyan/ar7516",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_ar7516",
    routerPath: "arcadyanastoria~arcadyanastoria_ar7516"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV452CPW",
    page: "/toh/arcadyan/arv452cpw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv452cpw",
    routerPath: "arcadyanastoria~arcadyanastoria_arv452cpw"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV4518PW",
    page: "/toh/arcadyan/arv4518pw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv4518pw_r01a",
    routerPath: "arcadyanastoria~arcadyanastoria_arv4518pw_r01a"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV7510PW22",
    page: "/toh/astoria/arv7510pw22",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv7510pw22",
    routerPath: "arcadyanastoria~arcadyanastoria_arv7510pw22"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV7518PW",
    page: "/toh/astoria/arv7518pw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv7518pw",
    routerPath: "arcadyanastoria~arcadyanastoria_arv7518pw"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV7519PW",
    page: "/toh/arcadyan/arv7519pw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv7519pw",
    routerPath: "arcadyanastoria~arcadyanastoria_arv7519pw"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV7519RW22 Livebox 2.1",
    page: "/toh/arcadyan/arv7519",
    techData:
      "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv7519rw22livebox21",
    routerPath: "arcadyanastoria~arcadyanastoria_arv7519rw22livebox21"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "VGV7510KW22 o2 Box 6431",
    page: "/toh/arcadyan/vgv7510kw22",
    techData:
      "/toh/hwdata/arcadyanastoria/arcadyanastoria_vgv7510kw22o2box6431",
    routerPath: "arcadyanastoria~arcadyanastoria_vgv7510kw22o2box6431"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV752DPW22 EasyBox 803A",
    page: "/toh/astoria/arv752dpw22",
    techData:
      "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv752dpw22_easybox803a",
    routerPath: "arcadyanastoria~arcadyanastoria_arv752dpw22_easybox803a"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV752DPW EasyBox 802",
    page: "/toh/arcadyan/arv752dpw",
    techData:
      "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv752dpw_easybox802_r01",
    routerPath: "arcadyanastoria~arcadyanastoria_arv752dpw_easybox802_r01"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV4518PW",
    page: "/toh/arcadyan/arv4518pw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv4518pw_r01",
    routerPath: "arcadyanastoria~arcadyanastoria_arv4518pw_r01"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV4519PW",
    page: "/toh/arcadyan/arv4519pw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv4519pw",
    routerPath: "arcadyanastoria~arcadyanastoria_arv4519pw"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV7506PW11",
    page: "/toh/arcadyan/arv7506",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv7506pw11",
    routerPath: "arcadyanastoria~arcadyanastoria_arv7506pw11"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "ARV4510PW",
    page: "/toh/arcadyan/arv4510pw",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_arv4510pw",
    routerPath: "arcadyanastoria~arcadyanastoria_arv4510pw"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "VGV7519KW (KPN Experia Box v8)",
    page: "/toh/arcadyan/vgv7519",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_vgv7519kw_r02",
    routerPath: "arcadyanastoria~arcadyanastoria_vgv7519kw_r02"
  },
  {
    brand: "Arcadyan / Astoria",
    model: "VGV7519KW (KPN Experia Box v8)",
    page: "/toh/arcadyan/vgv7519",
    techData: "/toh/hwdata/arcadyanastoria/arcadyanastoria_vgv7519kw_r01",
    routerPath: "arcadyanastoria~arcadyanastoria_vgv7519kw_r01"
  },
  {
    brand: "Arduino.cc",
    model: "Yun",
    page: "/toh/arduino.cc/yun",
    techData: "/toh/hwdata/arduino.cc/arduino.cc_yun",
    routerPath: "arduino.cc~arduino.cc_yun"
  },
  {
    brand: "Aruba",
    model: "AP-105",
    page: "/toh/aruba/aruba_ap-105",
    techData: "/toh/hwdata/aruba/aruba_ap-105",
    routerPath: "aruba~aruba_ap-105"
  },
  {
    brand: "Aruba",
    model: "AP-303",
    page: "/toh/aruba/aruba_ap-303",
    techData: "/toh/hwdata/aruba/aruba_ap-303",
    routerPath: "aruba~aruba_ap-303"
  },
  {
    brand: "AsiaRF",
    model: "AWM002",
    page: "/toh/asiarf/awm-evb",
    techData: "/toh/hwdata/asiarf/asiarf_awm002",
    routerPath: "asiarf~asiarf_awm002"
  },
  {
    brand: "AsiaRF",
    model: "AWM003",
    page: "/toh/asiarf/awm-evb",
    techData: "/toh/hwdata/asiarf/asiarf_awm003",
    routerPath: "asiarf~asiarf_awm003"
  },
  {
    brand: "AsiaRF",
    model: "AP7623-A02",
    page: "/toh/asiarf/asiarf_ap7623-a02",
    techData: "/toh/hwdata/asiarf/asiarf_ap7623-a02_1",
    routerPath: "asiarf~asiarf_ap7623-a02_1"
  },
  {
    brand: "AsiaRF",
    model: "AP7621-002",
    page: "/toh/asiarf/asiarf_ap7621-002",
    techData: "/toh/hwdata/asiarf/asiarf_ap7621-002_1",
    routerPath: "asiarf~asiarf_ap7621-002_1"
  },
  {
    brand: "Asmax",
    model: "AR 1004g",
    page: "/toh/asmax/ar_1004g",
    techData: "/toh/hwdata/asmax/asmax_ar1004g_1",
    routerPath: "asmax~asmax_ar1004g_1"
  },
  {
    brand: "Asus",
    model: "RP-N53",
    page: "/toh/asus/rp-n53",
    techData: "/toh/hwdata/asus/asus_rp-n53",
    routerPath: "asus~asus_rp-n53"
  },
  {
    brand: "Asus",
    model: "RT-AC51U",
    page: "/toh/asus/rt-ac51u",
    techData: "/toh/hwdata/asus/asus_rt-ac51u",
    routerPath: "asus~asus_rt-ac51u"
  },
  {
    brand: "Asus",
    model: "RT-G32",
    page: "/toh/asus/rt-g32",
    techData: "/toh/hwdata/asus/asus_rt-g32_b1",
    routerPath: "asus~asus_rt-g32_b1"
  },
  {
    brand: "Asus",
    model: "RT-G32",
    page: "/toh/asus/rt-g32",
    techData: "/toh/hwdata/asus/asus_rt-g32_c1",
    routerPath: "asus~asus_rt-g32_c1"
  },
  {
    brand: "Asus",
    model: "RT-N10P",
    page: "/toh/asus/rt-n10p",
    techData: "/toh/hwdata/asus/asus_rt-n10p_a1",
    routerPath: "asus~asus_rt-n10p_a1"
  },
  {
    brand: "Asus",
    model: "RT-N15U",
    page: "/toh/asus/rt-n15u",
    techData: "/toh/hwdata/asus/asus_rt-n15u",
    routerPath: "asus~asus_rt-n15u"
  },
  {
    brand: "Asus",
    model: "RT-N16",
    page: "/toh/asus/rt-n16",
    techData: "/toh/hwdata/asus/asus_rt-n16",
    routerPath: "asus~asus_rt-n16"
  },
  {
    brand: "Asus",
    model: "RT-N53",
    page: "/toh/asus/rt-n53",
    techData: "/toh/hwdata/asus/asus_rt-n53",
    routerPath: "asus~asus_rt-n53"
  },
  {
    brand: "Asus",
    model: "WL-320gE",
    page: "/toh/asus/wl320g",
    techData: "/toh/hwdata/asus/asus_wl-320ge",
    routerPath: "asus~asus_wl-320ge"
  },
  {
    brand: "Asus",
    model: "WL-320gP",
    page: "/toh/asus/wl320g",
    techData: "/toh/hwdata/asus/asus_wl-320gp",
    routerPath: "asus~asus_wl-320gp"
  },
  {
    brand: "Asus",
    model: "WL-330N3G",
    page: "/toh/asus/wl-330n3g",
    techData: "/toh/hwdata/asus/asus_wl-330n3g",
    routerPath: "asus~asus_wl-330n3g"
  },
  {
    brand: "Asus",
    model: "WL-330N",
    page: "/toh/asus/wl-330n",
    techData: "/toh/hwdata/asus/asus_wl-330n_a",
    routerPath: "asus~asus_wl-330n_a"
  },
  {
    brand: "Asus",
    model: "WL-500b",
    page: "/toh/asus/wl500b",
    techData: "/toh/hwdata/asus/asus_wl-500b_v1",
    routerPath: "asus~asus_wl-500b_v1"
  },
  {
    brand: "Asus",
    model: "WL-500g",
    page: "/toh/asus/wl500g",
    techData: "/toh/hwdata/asus/asus_wl-500g",
    routerPath: "asus~asus_wl-500g"
  },
  {
    brand: "Asus",
    model: "WL-500g Deluxe",
    page: "/toh/asus/wl500gd",
    techData: "/toh/hwdata/asus/asus_wl-500gdeluxe",
    routerPath: "asus~asus_wl-500gdeluxe"
  },
  {
    brand: "Asus",
    model: "WL-500g Premium",
    page: "/toh/asus/wl500gp",
    techData: "/toh/hwdata/asus/asus_wl-500gpremium_v1",
    routerPath: "asus~asus_wl-500gpremium_v1"
  },
  {
    brand: "Asus",
    model: "WL-500g Premium",
    page: "/toh/asus/wl500gp",
    techData: "/toh/hwdata/asus/asus_wl-500gpremium_v2",
    routerPath: "asus~asus_wl-500gpremium_v2"
  },
  {
    brand: "Asus",
    model: "WL-500W",
    page: "/toh/asus/wl500w",
    techData: "/toh/hwdata/asus/asus_wl-500w",
    routerPath: "asus~asus_wl-500w"
  },
  {
    brand: "Asus",
    model: "WL-520gU",
    page: "/toh/asus/wl520gu",
    techData: "/toh/hwdata/asus/asus_wl-520gu",
    routerPath: "asus~asus_wl-520gu"
  },
  {
    brand: "Asus",
    model: "WL-550gE",
    page: "/toh/asus/wl550ge",
    techData: "/toh/hwdata/asus/asus_wl-550ge",
    routerPath: "asus~asus_wl-550ge"
  },
  {
    brand: "Asus",
    model: "WL-600g",
    page: "/toh/asus/wl600g",
    techData: "/toh/hwdata/asus/asus_wl-600g",
    routerPath: "asus~asus_wl-600g"
  },
  {
    brand: "Asus",
    model: "WL-HDD2.5",
    page: "/toh/asus/wlhdd2.5",
    techData: "/toh/hwdata/asus/asus_wl-hdd25",
    routerPath: "asus~asus_wl-hdd25"
  },
  {
    brand: "Asus",
    model: "RT-N10U",
    page: "/toh/asus/rt-n10u",
    techData: "/toh/hwdata/asus/asus_rt-n10u_b",
    routerPath: "asus~asus_rt-n10u_b"
  },
  {
    brand: "Asus",
    model: "RT-N12",
    page: "/toh/asus/rt-n12b1",
    techData: "/toh/hwdata/asus/asus_rt-n12_b1",
    routerPath: "asus~asus_rt-n12_b1"
  },
  {
    brand: "Asus",
    model: "RT-N12",
    page: "/toh/asus/rt-n12d1",
    techData: "/toh/hwdata/asus/asus_rt-n12_d1",
    routerPath: "asus~asus_rt-n12_d1"
  },
  {
    brand: "Asus",
    model: "RT-N10U",
    page: "/toh/asus/rt-n10u",
    techData: "/toh/hwdata/asus/asus_rt-n10u",
    routerPath: "asus~asus_rt-n10u"
  },
  {
    brand: "Asus",
    model: "RT-N10+",
    page: "/toh/asus/rt-n10plus",
    techData: "/toh/hwdata/asus/asus_rt-n10plus_b1",
    routerPath: "asus~asus_rt-n10plus_b1"
  },
  {
    brand: "Asus",
    model: "RT-N18U",
    page: "/toh/asus/rt-n18u",
    techData: "/toh/hwdata/asus/asus_rt-n18u_a1",
    routerPath: "asus~asus_rt-n18u_a1"
  },
  {
    brand: "Asus",
    model: "RT-N12",
    page: "/toh/asus/rt-n12_a1",
    techData: "/toh/hwdata/asus/asus_rt-n12_a1",
    routerPath: "asus~asus_rt-n12_a1"
  },
  {
    brand: "Asus",
    model: "RT-N13",
    page: "/toh/asus/rt-n13",
    techData: "/toh/hwdata/asus/asus_rt-n13",
    routerPath: "asus~asus_rt-n13"
  },
  {
    brand: "Asus",
    model: "RT-N13U",
    page: "/toh/asus/rt-n13u",
    techData: "/toh/hwdata/asus/asus_rt-n13u_b1",
    routerPath: "asus~asus_rt-n13u_b1"
  },
  {
    brand: "Asus",
    model: "RT-N13U",
    page: "/toh/asus/rt-n13u",
    techData: "/toh/hwdata/asus/asus_rt-n13u",
    routerPath: "asus~asus_rt-n13u"
  },
  {
    brand: "Asus",
    model: "RT-N14U",
    page: "/toh/asus/rt-n14u",
    techData: "/toh/hwdata/asus/asus_rt-n14u_c1",
    routerPath: "asus~asus_rt-n14u_c1"
  },
  {
    brand: "Asus",
    model: "RT-N15",
    page: "/toh/asus/rt-n15",
    techData: "/toh/hwdata/asus/asus_rt-n15",
    routerPath: "asus~asus_rt-n15"
  },
  {
    brand: "Asus",
    model: "RT-N56U",
    page: "/toh/asus/rt-n56u",
    techData: "/toh/hwdata/asus/asus_rt-n56u_a1",
    routerPath: "asus~asus_rt-n56u_a1"
  },
  {
    brand: "Asus",
    model: "RT-N66U",
    page: "/toh/asus/rt-n66u",
    techData: "/toh/hwdata/asus/asus_rt-n66u_b1",
    routerPath: "asus~asus_rt-n66u_b1"
  },
  {
    brand: "Asus",
    model: "RT-AC56U",
    page: "/toh/asus/rt-ac56u",
    techData: "/toh/hwdata/asus/asus_rt-ac56u",
    routerPath: "asus~asus_rt-ac56u"
  },
  {
    brand: "Asus",
    model: "RT-AC68U",
    page: "/toh/asus/rt-ac68u",
    techData: "/toh/hwdata/asus/asus_rt-ac68u",
    routerPath: "asus~asus_rt-ac68u"
  },
  {
    brand: "Asus",
    model: "RT-AC87U",
    page: "/toh/asus/rt-ac87u",
    techData: "/toh/hwdata/asus/asus_rt-ac87u",
    routerPath: "asus~asus_rt-ac87u"
  },
  {
    brand: "Asus",
    model: "RT-N10+",
    page: "/toh/asus/rt-n10plus.d1",
    techData: "/toh/hwdata/asus/asus_rt-n10plus_d1",
    routerPath: "asus~asus_rt-n10plus_d1"
  },
  {
    brand: "Asus",
    model: "RT-AC58U",
    page: "/toh/asus/rt-ac58u",
    techData: "/toh/hwdata/asus/asus_rt-ac58u",
    routerPath: "asus~asus_rt-ac58u"
  },
  {
    brand: "Asus",
    model: "RT-AC57U",
    page: "/toh/asus/asus_rt-ac57u",
    techData: "/toh/hwdata/asus/asus_rt-ac57u",
    routerPath: "asus~asus_rt-ac57u"
  },
  {
    brand: "ASUS",
    model: "RT-AC85P",
    page: "/toh/asus/asus_rt-ac85p",
    techData: "/toh/hwdata/asus/asus_rt-ac85p",
    routerPath: "asus~asus_rt-ac85p"
  },
  {
    brand: "ASUS",
    model: "RT-AC65P",
    page: "/toh/asus/asus_rt-ac85p",
    techData: "/toh/hwdata/asus/asus_rt-ac65p",
    routerPath: "asus~asus_rt-ac65p"
  },
  {
    brand: "ASUS",
    model: "RT-ACRH13",
    page: "/toh/asus/rt-ac58u",
    techData: "/toh/hwdata/asus/asus_rt-acrh13",
    routerPath: "asus~asus_rt-acrh13"
  },
  {
    brand: "ASUS",
    model: "RT-AC1300UHP",
    page: "/toh/asus/rt-ac58u",
    techData: "/toh/hwdata/asus/asus_rt-ac1300uhp",
    routerPath: "asus~asus_rt-ac1300uhp"
  },
  {
    brand: "Atmel",
    model: "ATNGW100",
    page: "/toh/atmel/atngw100",
    techData: "/toh/hwdata/atmel/atmel_atngw100",
    routerPath: "atmel~atmel_atngw100"
  },
  {
    brand: "AudioCodes",
    model: "Mediapack MP-252",
    page: "/toh/audiocodes/mp252",
    techData: "/toh/hwdata/audiocodes/audiocodes_mediapackmp-252",
    routerPath: "audiocodes~audiocodes_mediapackmp-252"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box WLAN 3370",
    page: "/toh/avm/fritz.box.wlan.3370",
    techData: "/toh/hwdata/avm/avm_fritz_box_wlan_3370",
    routerPath: "avm~avm_fritz_box_wlan_3370"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box Fon WLAN 7170",
    page: "/toh/avm/fritz.box.wlan.7170",
    techData: "/toh/hwdata/avm/avm_fritzboxfonwlan7170",
    routerPath: "avm~avm_fritzboxfonwlan7170"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box 7360SL",
    page: "/toh/avm/fritz.box.wlan.7360",
    techData: "/toh/hwdata/avm/avm_fritz_box_7360sl",
    routerPath: "avm~avm_fritz_box_7360sl"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box 4040",
    page: "/toh/avm/avm_fritz_box_4040",
    techData: "/toh/hwdata/avm/avm_fritz_box_4040",
    routerPath: "avm~avm_fritz_box_4040"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box Fon WLAN 7320",
    page: "/toh/avm/avm_fritz_box_7320",
    techData: "/toh/hwdata/avm/avm_fritz_box_fon_wlan_7320",
    routerPath: "avm~avm_fritz_box_fon_wlan_7320"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box 7312",
    page: "/toh/avm/fritz.box.wlan.7312",
    techData: "/toh/hwdata/avm/avm_fritz_box_7312",
    routerPath: "avm~avm_fritz_box_7312"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box 7330",
    page: "/toh/avm/fritz.box.wlan.7330",
    techData: "/toh/hwdata/avm/avm_fritz_box_7330",
    routerPath: "avm~avm_fritz_box_7330"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box 4020",
    page: "/toh/avm/fritz.box.4020",
    techData: "/toh/hwdata/avm/avm_fritz_box_4020",
    routerPath: "avm~avm_fritz_box_4020"
  },
  {
    brand: "AVM",
    model: "FRITZ!WLAN Repeater 450E",
    page: "/toh/avm/avm_fritz_wlan_repeater_450e",
    techData: "/toh/hwdata/avm/avm_fritz_wlan_repeater_450e",
    routerPath: "avm~avm_fritz_wlan_repeater_450e"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box 7362SL",
    page: "/toh/avm/avm_7362_sl",
    techData: "/toh/hwdata/avm/avm_fritz_box_7362sl",
    routerPath: "avm~avm_fritz_box_7362sl"
  },
  {
    brand: "AVM",
    model: "FRITZ!Box 7412",
    page: "/toh/avm/avm_fritz_box_7412",
    techData: "/toh/hwdata/avm/avm_fritz_box_7412",
    routerPath: "avm~avm_fritz_box_7412"
  },
  {
    brand: "AXIMCom",
    model: "MR-102N",
    page: "/toh/aximcom/mr-102n",
    techData: "/toh/hwdata/aximcom/aximcom_mr-102n",
    routerPath: "aximcom~aximcom_mr-102n"
  },
  {
    brand: "Aztech",
    model: "HW550-3G",
    page: "/toh/aztech/hw550-3g",
    techData: "/toh/hwdata/aztech/aztech_hw550-3g",
    routerPath: "aztech~aztech_hw550-3g"
  },
  {
    brand: "Belkin",
    model: "F5D8230-4",
    page: "/toh/belkin/f5d8230-4",
    techData: "/toh/hwdata/belkin/belkin_f5d8230-4_v1002",
    routerPath: "belkin~belkin_f5d8230-4_v1002"
  },
  {
    brand: "Belkin",
    model: "F5D8235-4",
    page: "/toh/belkin/f5d8235-4",
    techData: "/toh/hwdata/belkin/belkin_f5d8235-4_v1",
    routerPath: "belkin~belkin_f5d8235-4_v1"
  },
  {
    brand: "Belkin",
    model: "F5D8235-4",
    page: "/toh/belkin/f5d8235-4",
    techData: "/toh/hwdata/belkin/belkin_f5d8235-4_v2",
    routerPath: "belkin~belkin_f5d8235-4_v2"
  },
  {
    brand: "Belkin",
    model: "F7C027",
    page: "/toh/belkin/f7c027",
    techData: "/toh/hwdata/belkin/belkin_f7c027",
    routerPath: "belkin~belkin_f7c027"
  },
  {
    brand: "Belkin",
    model: "F9K1115 v2 (AC 1750 DB)",
    page: "/toh/belkin/f9k1115v2",
    techData: "/toh/hwdata/belkin/belkin_f9k1115v2_v2",
    routerPath: "belkin~belkin_f9k1115v2_v2"
  },
  {
    brand: "Belkin",
    model: "F9K1109",
    page: "/toh/belkin/belkin_f9k1109_v1",
    techData: "/toh/hwdata/belkin/belkin_f9k1109_v1",
    routerPath: "belkin~belkin_f9k1109_v1"
  },
  {
    brand: "Billion",
    model: "BiPAC 7700N",
    page: "/toh/billion/billion_bipac_7700n",
    techData: "/toh/hwdata/billion/billion_bipac_7700n",
    routerPath: "billion~billion_bipac_7700n"
  },
  {
    brand: "Bitmain",
    model: "Antrouter R1",
    page: "/toh/bitmain/r1",
    techData: "/toh/hwdata/bitmain/bitmain_antrouter_r1",
    routerPath: "bitmain~bitmain_antrouter_r1"
  },
  {
    brand: "Bitmain",
    model: "Antminer S1",
    page: "/toh/bitmain/s1",
    techData: "/toh/hwdata/bitmain/bitmain_antminer_s1",
    routerPath: "bitmain~bitmain_antminer_s1"
  },
  {
    brand: "Bitmain",
    model: "Antminer S3",
    page: "/toh/bitmain/s3",
    techData: "/toh/hwdata/bitmain/bitmain_antminer_s3",
    routerPath: "bitmain~bitmain_antminer_s3"
  },
  {
    brand: "BT",
    model: "Home Hub 3",
    page: "/toh/bt/homehub_v3a",
    techData: "/toh/hwdata/bt/bt_homehub3_1",
    routerPath: "bt~bt_homehub3_1"
  },
  {
    brand: "BT",
    model: "Home Hub 5",
    page: "/toh/bt/homehub_v5a",
    techData: "/toh/hwdata/bt/bt_homehub_5_type_a",
    routerPath: "bt~bt_homehub_5_type_a"
  },
  {
    brand: "BT",
    model: "Home Hub 2",
    page: "/toh/bt/homehub_v2a",
    techData: "/toh/hwdata/bt/bt_homehub_2a",
    routerPath: "bt~bt_homehub_2a"
  },
  {
    brand: "BT",
    model: "Home Hub 2",
    page: "/toh/bt/homehub_v2b",
    techData: "/toh/hwdata/bt/bt_homehub_2b",
    routerPath: "bt~bt_homehub_2b"
  },
  {
    brand: "BT Openreach",
    model: "VG3503J",
    page: "/toh/bt/vg3503j",
    techData: "/toh/hwdata/bt/bt_vg3503j_v1",
    routerPath: "bt~bt_vg3503j_v1"
  },
  {
    brand: "BT Openreach",
    model: "VG3503J",
    page: "/toh/bt/vg3503j",
    techData: "/toh/hwdata/bt/bt_vg3503j_v2",
    routerPath: "bt~bt_vg3503j_v2"
  },
  {
    brand: "Buffalo",
    model: "LS421DE",
    page: "/toh/buffalo/ls421de",
    techData: "/toh/hwdata/buffalo/buffalo_ls421de_v1",
    routerPath: "buffalo~buffalo_ls421de_v1"
  },
  {
    brand: "Buffalo",
    model: "WBMR-G54",
    page: "/toh/buffalo/wbmr-g54",
    techData: "/toh/hwdata/buffalo/buffalo_wbmr-g54",
    routerPath: "buffalo~buffalo_wbmr-g54"
  },
  {
    brand: "Buffalo",
    model: "WBMR-HP-G300H",
    page: "/toh/buffalo/wbmr-hp-g300h",
    techData: "/toh/hwdata/buffalo/buffalo_wbmr-hp-g300h",
    routerPath: "buffalo~buffalo_wbmr-hp-g300h"
  },
  {
    brand: "Buffalo",
    model: "WHR-300HP2",
    page: "/toh/buffalo/whr-300hp2",
    techData: "/toh/hwdata/buffalo/buffalo_whr-300hp2",
    routerPath: "buffalo~buffalo_whr-300hp2"
  },
  {
    brand: "Buffalo",
    model: "WHR-600D",
    page: "/toh/buffalo/whr-600d",
    techData: "/toh/hwdata/buffalo/buffalo_whr-600d",
    routerPath: "buffalo~buffalo_whr-600d"
  },
  {
    brand: "Buffalo",
    model: "WHR-1166D",
    page: "/toh/buffalo/whr-1166d",
    techData: "/toh/hwdata/buffalo/buffalo_whr-1166d",
    routerPath: "buffalo~buffalo_whr-1166d"
  },
  {
    brand: "Buffalo",
    model: "WHR-G54S",
    page: "/toh/buffalo/whr-g54s",
    techData: "/toh/hwdata/buffalo/buffalo_whr-g54s",
    routerPath: "buffalo~buffalo_whr-g54s"
  },
  {
    brand: "Buffalo",
    model: "WHR-G125",
    page: "/toh/buffalo/whr-g125",
    techData: "/toh/hwdata/buffalo/buffalo_whr-g125",
    routerPath: "buffalo~buffalo_whr-g125"
  },
  {
    brand: "Buffalo",
    model: "WHR-G300N",
    page: "/toh/buffalo/whr-g300nv1",
    techData: "/toh/hwdata/buffalo/buffalo_whr-g300n_v1",
    routerPath: "buffalo~buffalo_whr-g300n_v1"
  },
  {
    brand: "Buffalo",
    model: "WHR-G300N",
    page: "/toh/buffalo/whr-g300nv2",
    techData: "/toh/hwdata/buffalo/buffalo_whr-g300n_v2",
    routerPath: "buffalo~buffalo_whr-g300n_v2"
  },
  {
    brand: "Buffalo",
    model: "WHR-G301N",
    page: "/toh/buffalo/whr-g300nv2",
    techData: "/toh/hwdata/buffalo/buffalo_whr-g301n_v1",
    routerPath: "buffalo~buffalo_whr-g301n_v1"
  },
  {
    brand: "Buffalo",
    model: "WHR-HP-G54",
    page: "/toh/buffalo/whr-hp-g54",
    techData: "/toh/hwdata/buffalo/buffalo_whr-hp-g54",
    routerPath: "buffalo~buffalo_whr-hp-g54"
  },
  {
    brand: "Buffalo",
    model: "WHR-HP-G300N",
    page: "/toh/buffalo/whr-hp-g300n",
    techData: "/toh/hwdata/buffalo/buffalo_whr-hp-g300n_v1",
    routerPath: "buffalo~buffalo_whr-hp-g300n_v1"
  },
  {
    brand: "Buffalo",
    model: "WHR-HP-GN",
    page: "/toh/buffalo/whr-hp-g300n",
    techData: "/toh/hwdata/buffalo/buffalo_whr-hp-gn_v1",
    routerPath: "buffalo~buffalo_whr-hp-gn_v1"
  },
  {
    brand: "Buffalo",
    model: "WLI-TX4-AG300N",
    page: "/toh/buffalo/wli-tx4-ag300n",
    techData: "/toh/hwdata/buffalo/buffalo_wli-tx4-ag300n_v1",
    routerPath: "buffalo~buffalo_wli-tx4-ag300n_v1"
  },
  {
    brand: "Buffalo",
    model: "WLI-TX4-G54HP",
    page: "/toh/buffalo/wli-tx4-g54hp",
    techData: "/toh/hwdata/buffalo/buffalo_wli-tx4-g54hp",
    routerPath: "buffalo~buffalo_wli-tx4-g54hp"
  },
  {
    brand: "Buffalo",
    model: "WMR-300",
    page: "/toh/buffalo/wmr-300",
    techData: "/toh/hwdata/buffalo/buffalo_wmr-300",
    routerPath: "buffalo~buffalo_wmr-300"
  },
  {
    brand: "Buffalo",
    model: "WSR-600DHP",
    page: "/toh/buffalo/wsr-600dhp",
    techData: "/toh/hwdata/buffalo/buffalo_wsr-600dhp",
    routerPath: "buffalo~buffalo_wsr-600dhp"
  },
  {
    brand: "Buffalo",
    model: "WSR-1166DHP",
    page: "/toh/buffalo/wsr-1166dhp",
    techData: "/toh/hwdata/buffalo/buffalo_wsr-1166dhp",
    routerPath: "buffalo~buffalo_wsr-1166dhp"
  },
  {
    brand: "Buffalo",
    model: "WZR-300HP",
    page: "/toh/buffalo/wzr-300hp",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-300hp",
    routerPath: "buffalo~buffalo_wzr-300hp"
  },
  {
    brand: "Buffalo",
    model: "WZR-450HP2",
    page: "/toh/buffalo/wzr-450hp2",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-450hp2",
    routerPath: "buffalo~buffalo_wzr-450hp2"
  },
  {
    brand: "Buffalo",
    model: "WZR-600DHP",
    page: "/toh/buffalo/wzr-600dhp",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-600dhp",
    routerPath: "buffalo~buffalo_wzr-600dhp"
  },
  {
    brand: "Buffalo",
    model: "WZR-600DHP2",
    page: "/toh/buffalo/wzr-600dhp2",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-600dhp2",
    routerPath: "buffalo~buffalo_wzr-600dhp2"
  },
  {
    brand: "Buffalo",
    model: "WZR-1750DHP(D)",
    page: "/toh/buffalo/wzr-1750dhp",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-1750dhp",
    routerPath: "buffalo~buffalo_wzr-1750dhp"
  },
  {
    brand: "Buffalo",
    model: "WZR-HP-G300NH2",
    page: "/toh/buffalo/wzr-hp-g300nh2",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-hp-g300nh2_v2",
    routerPath: "buffalo~buffalo_wzr-hp-g300nh2_v2"
  },
  {
    brand: "Buffalo",
    model: "WZR-HP-G300NH",
    page: "/toh/buffalo/wzr-hp-g300h",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-hp-g300nh_v1",
    routerPath: "buffalo~buffalo_wzr-hp-g300nh_v1"
  },
  {
    brand: "Buffalo",
    model: "WZR-HP-G450H",
    page: "/toh/buffalo/wzr-hp-g450h",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-hp-g450h_v1",
    routerPath: "buffalo~buffalo_wzr-hp-g450h_v1"
  },
  {
    brand: "Buffalo",
    model: "WZR-HP-AG300H",
    page: "/toh/buffalo/wzr-hp-ag300h",
    techData: "/toh/hwdata/buffalo/buffalo_wzr-hp-ag300h_v1",
    routerPath: "buffalo~buffalo_wzr-hp-ag300h_v1"
  },
  {
    brand: "Cisco",
    model: "ON100-K9",
    page: "/toh/cisco/cisco_on-100_k9_v01",
    techData: "/toh/hwdata/cisco/cisco_on100-k9_v01",
    routerPath: "cisco~cisco_on100-k9_v01"
  },
  {
    brand: "Cisco",
    model: "VEN401",
    page: "/toh/cisco/ven401",
    techData: "/toh/hwdata/cisco/cisco_ven401",
    routerPath: "cisco~cisco_ven401"
  },
  {
    brand: "Cisco",
    model: "Valet M10",
    page: "/toh/cisco/m10",
    techData: "/toh/hwdata/cisco/cisco_valet_m10_v1",
    routerPath: "cisco~cisco_valet_m10_v1"
  },
  {
    brand: "Cisco",
    model: "Valet M10",
    page: "/toh/cisco/m10",
    techData: "/toh/hwdata/cisco/cisco_valet_m10_v2",
    routerPath: "cisco~cisco_valet_m10_v2"
  },
  {
    brand: "COMFAST",
    model: "CF-WR800N",
    page: "/toh/comfast/cf-wr800n",
    techData: "/toh/hwdata/comfast/comfast_cf-wr800n",
    routerPath: "comfast~comfast_cf-wr800n"
  },
  {
    brand: "COMFAST",
    model: "CF-E380AC",
    page: "/toh/comfast/cf-e380ac",
    techData: "/toh/hwdata/comfast/comfast_cf-e380ac_v1",
    routerPath: "comfast~comfast_cf-e380ac_v1"
  },
  {
    brand: "COMFAST",
    model: "CF-E110N",
    page: "/toh/comfast/cf-e110n",
    techData: "/toh/hwdata/comfast/comfast_cf-e110n",
    routerPath: "comfast~comfast_cf-e110n"
  },
  {
    brand: "COMFAST",
    model: "CF-E120A",
    page: "/toh/comfast/cf-e120a",
    techData: "/toh/hwdata/comfast/comfast_cf-e120a_v3",
    routerPath: "comfast~comfast_cf-e120a_v3"
  },
  {
    brand: "COMFAST",
    model: "CF-E313AC",
    page: "/toh/comfast/comfast_cf-e313ac",
    techData: "/toh/hwdata/comfast/comfast_cf-e313ac",
    routerPath: "comfast~comfast_cf-e313ac"
  },
  {
    brand: "COMFAST",
    model: "CF-EW72",
    page: "/toh/comfast/comfast_cf-ew72",
    techData: "/toh/hwdata/comfast/comfast_cf-ew72",
    routerPath: "comfast~comfast_cf-ew72"
  },
  {
    brand: "COMFAST",
    model: "CF-E560AC",
    page: "/toh/comfast/comfast_cf-e560ac",
    techData: "/toh/hwdata/comfast/comfast_e560ac",
    routerPath: "comfast~comfast_e560ac"
  },
  {
    brand: "Compex",
    model: "MMC543AHV",
    page: "/toh/compex/mmc543ahv",
    techData: "/toh/hwdata/compex/compex_mmc543ahv_6e03",
    routerPath: "compex~compex_mmc543ahv_6e03"
  },
  {
    brand: "Compex",
    model: "MMC543HV",
    page: "/toh/compex/mmc543hv",
    techData: "/toh/hwdata/compex/compex_mmc543hv_6e03",
    routerPath: "compex~compex_mmc543hv_6e03"
  },
  {
    brand: "Compex",
    model: "WP54",
    page: "/toh/compex/wp54",
    techData: "/toh/hwdata/compex/compex_wp54_6e",
    routerPath: "compex~compex_wp54_6e"
  },
  {
    brand: "Compex",
    model: "WP54g",
    page: "/toh/compex/wp54",
    techData: "/toh/hwdata/compex/compex_wp54g_6e",
    routerPath: "compex~compex_wp54g_6e"
  },
  {
    brand: "Compex",
    model: "WP543",
    page: "/toh/compex/wp543",
    techData: "/toh/hwdata/compex/compex_wp543_6e03",
    routerPath: "compex~compex_wp543_6e03"
  },
  {
    brand: "Compex",
    model: "WP543AHV",
    page: "/toh/compex/wp543ahv",
    techData: "/toh/hwdata/compex/compex_wp543ahv_6e03",
    routerPath: "compex~compex_wp543ahv_6e03"
  },
  {
    brand: "Compex",
    model: "WP546HV",
    page: "/toh/compex/wp546hv",
    techData: "/toh/hwdata/compex/compex_wp546hv_6b08",
    routerPath: "compex~compex_wp546hv_6b08"
  },
  {
    brand: "Compex",
    model: "WPE72",
    page: "/toh/compex/wpe72",
    techData: "/toh/hwdata/compex/compex_wpe72_7a07",
    routerPath: "compex~compex_wpe72_7a07"
  },
  {
    brand: "Compex",
    model: "WPE72NX",
    page: "/toh/compex/wpe72nx",
    techData: "/toh/hwdata/compex/compex_wpe72nx_7a07",
    routerPath: "compex~compex_wpe72nx_7a07"
  },
  {
    brand: "Compex",
    model: "WPJ72Z",
    page: "/toh/compex/wpj72z",
    techData: "/toh/hwdata/compex/compex_wpj72z_7b06",
    routerPath: "compex~compex_wpj72z_7b06"
  },
  {
    brand: "Compex",
    model: "WPJ342",
    page: "/toh/compex/wpj342",
    techData: "/toh/hwdata/compex/compex_wpj342_7a05",
    routerPath: "compex~compex_wpj342_7a05"
  },
  {
    brand: "Compex",
    model: "WPJ344",
    page: "/toh/compex/wpj344",
    techData: "/toh/hwdata/compex/compex_wpj344_6a06",
    routerPath: "compex~compex_wpj344_6a06"
  },
  {
    brand: "Compex",
    model: "WPJ531",
    page: "/toh/compex/wpj531",
    techData: "/toh/hwdata/compex/compex_wpj531_7a03",
    routerPath: "compex~compex_wpj531_7a03"
  },
  {
    brand: "Compex",
    model: "WPJ558",
    page: "/toh/compex/wpj558",
    techData: "/toh/hwdata/compex/compex_wpj558_6a06",
    routerPath: "compex~compex_wpj558_6a06"
  },
  {
    brand: "Comtrend",
    model: "AR-5381u",
    page: "/toh/comtrend/ar5381u",
    techData: "/toh/hwdata/comtrend/comtrend_ar-5381u",
    routerPath: "comtrend~comtrend_ar-5381u"
  },
  {
    brand: "Comtrend",
    model: "AR-5387un",
    page: "/toh/comtrend/ar5387un",
    techData: "/toh/hwdata/comtrend/comtrend_ar-5387un",
    routerPath: "comtrend~comtrend_ar-5387un"
  },
  {
    brand: "Comtrend",
    model: "CT-536+",
    page: "/toh/comtrend/ct536plus",
    techData: "/toh/hwdata/comtrend/comtrend_ct-536",
    routerPath: "comtrend~comtrend_ct-536"
  },
  {
    brand: "Comtrend",
    model: "CT-5361",
    page: "/toh/comtrend/ct5361",
    techData: "/toh/hwdata/comtrend/comtrend_ct-5361",
    routerPath: "comtrend~comtrend_ct-5361"
  },
  {
    brand: "Comtrend",
    model: "CT-5365",
    page: "/toh/comtrend/ct5365",
    techData: "/toh/hwdata/comtrend/comtrend_ct-5365",
    routerPath: "comtrend~comtrend_ct-5365"
  },
  {
    brand: "Comtrend",
    model: "CT-5621",
    page: "/toh/comtrend/ct5621",
    techData: "/toh/hwdata/comtrend/comtrend_ct-5621",
    routerPath: "comtrend~comtrend_ct-5621"
  },
  {
    brand: "Comtrend",
    model: "CT-6373",
    page: "/toh/comtrend/ct6373",
    techData: "/toh/hwdata/comtrend/comtrend_ct-6373",
    routerPath: "comtrend~comtrend_ct-6373"
  },
  {
    brand: "Comtrend",
    model: "HG-536+",
    page: "/toh/comtrend/ct536plus",
    techData: "/toh/hwdata/comtrend/comtrend_hg-536",
    routerPath: "comtrend~comtrend_hg-536"
  },
  {
    brand: "Comtrend",
    model: "VR-3025u",
    page: "/toh/comtrend/vr3025u",
    techData: "/toh/hwdata/comtrend/comtrend_vr-3025u",
    routerPath: "comtrend~comtrend_vr-3025u"
  },
  {
    brand: "Comtrend",
    model: "VR-3025un",
    page: "/toh/comtrend/vr3025un",
    techData: "/toh/hwdata/comtrend/comtrend_vr-3025un",
    routerPath: "comtrend~comtrend_vr-3025un"
  },
  {
    brand: "Comtrend",
    model: "VR-3026e",
    page: "/toh/comtrend/vr3026e",
    techData: "/toh/hwdata/comtrend/comtrend_vr-3026e_v1",
    routerPath: "comtrend~comtrend_vr-3026e_v1"
  },
  {
    brand: "Comtrend",
    model: "WAP-5813n",
    page: "/toh/comtrend/wap-5813n",
    techData: "/toh/hwdata/comtrend/comtrend_wap-5813n",
    routerPath: "comtrend~comtrend_wap-5813n"
  },
  {
    brand: "Comtrend",
    model: "AR-5315u",
    page: "/toh/comtrend/ar5315u",
    techData: "/toh/hwdata/comtrend/comtrend_ar-5315u",
    routerPath: "comtrend~comtrend_ar-5315u"
  },
  {
    brand: "Creator",
    model: "Ci40 (Marduk)",
    page: "/toh/creator/creator_ci40_marduk_02kz_5",
    techData: "/toh/hwdata/creator/creator_ci40_marduk_02kz_5",
    routerPath: "creator~creator_ci40_marduk_02kz_5"
  },
  {
    brand: "Cubitech",
    model: "Cubieboard (CubieBoard1)",
    page: "/toh/cubietech/cubieboard",
    techData: "/toh/hwdata/cubitech/cubitech_cubieboard",
    routerPath: "cubitech~cubitech_cubieboard"
  },
  {
    brand: "Cubitech",
    model: "Cubieboard2",
    page: "/toh/cubietech/cubieboard2",
    techData: "/toh/hwdata/cubitech/cubitech_cubieboard2",
    routerPath: "cubitech~cubitech_cubieboard2"
  },
  {
    brand: "Cubitech",
    model: "Cubietruck (CubieBoard3)",
    page: "/toh/cubietech/cubietruck",
    techData: "/toh/hwdata/cubitech/cubitech_cubietruck",
    routerPath: "cubitech~cubitech_cubietruck"
  },
  {
    brand: "D-Link",
    model: "DAP-2695",
    page: "/toh/d-link/dap-2695",
    techData: "/toh/hwdata/d-link/d-link_dap-2695_a1",
    routerPath: "d-link~d-link_dap-2695_a1"
  },
  {
    brand: "D-Link",
    model: "DAP-1350",
    page: "/toh/d-link/dap-1350",
    techData: "/toh/hwdata/d-link/d-link_dap1350",
    routerPath: "d-link~d-link_dap1350"
  },
  {
    brand: "D-Link",
    model: "DCS-930L/DCS-932L",
    page: "/toh/d-link/dcs-930l",
    techData: "/toh/hwdata/d-link/d-link_dcs-93x",
    routerPath: "d-link~d-link_dcs-93x"
  },
  {
    brand: "D-Link",
    model: "DIR-300",
    page: "/toh/d-link/dir-300",
    techData: "/toh/hwdata/d-link/d-link_dir-300_a1",
    routerPath: "d-link~d-link_dir-300_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-300",
    page: "/toh/d-link/dir-300revb",
    techData: "/toh/hwdata/d-link/d-link_dir-300_b1",
    routerPath: "d-link~d-link_dir-300_b1"
  },
  {
    brand: "D-Link",
    model: "DIR-300",
    page: "/toh/d-link/dir-300revb",
    techData: "/toh/hwdata/d-link/d-link_dir-300_b5_b6_b7",
    routerPath: "d-link~d-link_dir-300_b5_b6_b7"
  },
  {
    brand: "D-Link",
    model: "DIR-320",
    page: "/toh/d-link/dir-320",
    techData: "/toh/hwdata/d-link/d-link_dir-320_a1",
    routerPath: "d-link~d-link_dir-320_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-320",
    page: "/toh/d-link/dir-320",
    techData: "/toh/hwdata/d-link/d-link_dir-320_a2",
    routerPath: "d-link~d-link_dir-320_a2"
  },
  {
    brand: "D-Link",
    model: "DIR-320",
    page: "/toh/d-link/dir-320_revb1",
    techData: "/toh/hwdata/d-link/d-link_dir-320_b1",
    routerPath: "d-link~d-link_dir-320_b1"
  },
  {
    brand: "D-Link",
    model: "DIR-330",
    page: "/toh/d-link/dir-330",
    techData: "/toh/hwdata/d-link/d-link_dir-330_a1",
    routerPath: "d-link~d-link_dir-330_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-505",
    page: "/toh/d-link/dir-505",
    techData: "/toh/hwdata/d-link/d-link_dir-505",
    routerPath: "d-link~d-link_dir-505"
  },
  {
    brand: "D-Link",
    model: "DIR-510L",
    page: "/toh/d-link/d-link_dir-510l_a1",
    techData: "/toh/hwdata/d-link/d-link_dir-510l",
    routerPath: "d-link~d-link_dir-510l"
  },
  {
    brand: "D-Link",
    model: "DIR-600",
    page: "/toh/d-link/dir-600",
    techData: "/toh/hwdata/d-link/d-link_dir-600_a1",
    routerPath: "d-link~d-link_dir-600_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-600",
    page: "/toh/d-link/dir-300revb",
    techData: "/toh/hwdata/d-link/d-link_dir-600_b1",
    routerPath: "d-link~d-link_dir-600_b1"
  },
  {
    brand: "D-Link",
    model: "DIR-600",
    page: "/toh/d-link/dir-300revb",
    techData: "/toh/hwdata/d-link/d-link_dir-600_b2",
    routerPath: "d-link~d-link_dir-600_b2"
  },
  {
    brand: "D-Link",
    model: "DIR-601",
    page: "/toh/d-link/dir-600",
    techData: "/toh/hwdata/d-link/d-link_dir-601_a1",
    routerPath: "d-link~d-link_dir-601_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_d1",
    routerPath: "d-link~d-link_dir-615_d1"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_e1",
    routerPath: "d-link~d-link_dir-615_e1"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_e4",
    routerPath: "d-link~d-link_dir-615_e4"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_h1",
    routerPath: "d-link~d-link_dir-615_h1"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_i1",
    routerPath: "d-link~d-link_dir-615_i1"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_i3",
    routerPath: "d-link~d-link_dir-615_i3"
  },
  {
    brand: "D-Link",
    model: "DIR-620",
    page: "/toh/d-link/dir-620",
    techData: "/toh/hwdata/d-link/d-link_dir-620_a1",
    routerPath: "d-link~d-link_dir-620_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-620",
    page: "/toh/d-link/dir-620",
    techData: "/toh/hwdata/d-link/d-link_dir-620_d1",
    routerPath: "d-link~d-link_dir-620_d1"
  },
  {
    brand: "D-Link",
    model: "DIR-645",
    page: "/toh/d-link/dir-645",
    techData: "/toh/hwdata/d-link/d-link_dir-645_a1",
    routerPath: "d-link~d-link_dir-645_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-810L",
    page: "/toh/d-link/dir-810l",
    techData: "/toh/hwdata/d-link/d-link_dir-810l_b1",
    routerPath: "d-link~d-link_dir-810l_b1"
  },
  {
    brand: "D-Link",
    model: "DIR-825",
    page: "/toh/d-link/dir-825",
    techData: "/toh/hwdata/d-link/d-link_dir-825",
    routerPath: "d-link~d-link_dir-825"
  },
  {
    brand: "D-Link",
    model: "DIR-825",
    page: "/toh/d-link/dir-825",
    techData: "/toh/hwdata/d-link/d-link_dir-825_c1",
    routerPath: "d-link~d-link_dir-825_c1"
  },
  {
    brand: "D-Link",
    model: "DIR-835",
    page: "/toh/d-link/dir-835",
    techData: "/toh/hwdata/d-link/d-link_dir-835_a1",
    routerPath: "d-link~d-link_dir-835_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-860L",
    page: "/toh/d-link/dir-860l",
    techData: "/toh/hwdata/d-link/d-link_dir-860l_b1",
    routerPath: "d-link~d-link_dir-860l_b1"
  },
  {
    brand: "D-Link",
    model: "DIR-885L",
    page: "/toh/d-link/d-link_dir-885l",
    techData: "/toh/hwdata/d-link/d-link_dir-885l_a1",
    routerPath: "d-link~d-link_dir-885l_a1"
  },
  {
    brand: "D-Link",
    model: "DNS-120",
    page: "/toh/d-link/dns-120",
    techData: "/toh/hwdata/d-link/d-link_dns-120_a2",
    routerPath: "d-link~d-link_dns-120_a2"
  },
  {
    brand: "D-Link",
    model: "DSL-504T",
    page: "/toh/d-link/dsl-504t",
    techData: "/toh/hwdata/d-link/d-link_dsl-504t_c3",
    routerPath: "d-link~d-link_dsl-504t_c3"
  },
  {
    brand: "D-Link",
    model: "DSL-524T",
    page: "/toh/d-link/dsl-524t",
    techData: "/toh/hwdata/d-link/d-link_dsl-524t_esl524teu.a1g",
    routerPath: "d-link~d-link_dsl-524t_esl524teu.a1g"
  },
  {
    brand: "D-Link",
    model: "DSL-584T",
    page: "/toh/d-link/dsl-584t",
    techData: "/toh/hwdata/d-link/d-link_dsl-584t",
    routerPath: "d-link~d-link_dsl-584t"
  },
  {
    brand: "D-Link",
    model: "DSL-2650U/BRU/D",
    page: "/toh/d-link/dsl-2650u",
    techData: "/toh/hwdata/d-link/d-link_dsl-2650ubrud",
    routerPath: "d-link~d-link_dsl-2650ubrud"
  },
  {
    brand: "D-Link",
    model: "DSL-2740B/1B",
    page: "/toh/d-link/dsl-2740b",
    techData: "/toh/hwdata/d-link/d-link_dsl-2740b1b_c2",
    routerPath: "d-link~d-link_dsl-2740b1b_c2"
  },
  {
    brand: "D-Link",
    model: "DSL-2740B/1B",
    page: "/toh/d-link/dsl-2740b",
    techData: "/toh/hwdata/d-link/d-link_dsl-2740b1b_c3",
    routerPath: "d-link~d-link_dsl-2740b1b_c3"
  },
  {
    brand: "D-Link",
    model: "DSL-2740B/1B",
    page: "/toh/d-link/dsl-2740b",
    techData: "/toh/hwdata/d-link/d-link_dsl-2740b1b_f1",
    routerPath: "d-link~d-link_dsl-2740b1b_f1"
  },
  {
    brand: "D-Link",
    model: "DSL-2740U",
    page: "/toh/d-link/dsl-2740b",
    techData: "/toh/hwdata/d-link/d-link_dsl-2740u_c2",
    routerPath: "d-link~d-link_dsl-2740u_c2"
  },
  {
    brand: "D-Link",
    model: "DSL-G624T",
    page: "/toh/d-link/dsl-624t",
    techData: "/toh/hwdata/d-link/d-link_dsl-g624t",
    routerPath: "d-link~d-link_dsl-g624t"
  },
  {
    brand: "D-Link",
    model: "DSL-G684T",
    page: "/toh/d-link/dsl-524t",
    techData: "/toh/hwdata/d-link/d-link_dsl-g684t",
    routerPath: "d-link~d-link_dsl-g684t"
  },
  {
    brand: "D-Link",
    model: "DVA-G3810BN/TL",
    page: "/toh/d-link/dva-g3810bn-tl",
    techData: "/toh/hwdata/d-link/d-link_dva-g3810bntl_a1",
    routerPath: "d-link~d-link_dva-g3810bntl_a1"
  },
  {
    brand: "D-Link",
    model: "DWL-2100AP",
    page: "/toh/d-link/dwl-2100ap",
    techData: "/toh/hwdata/d-link/d-link_dwl-2100ap_a2",
    routerPath: "d-link~d-link_dwl-2100ap_a2"
  },
  {
    brand: "D-Link",
    model: "DWL-2100AP",
    page: "/toh/d-link/dwl-2100ap",
    techData: "/toh/hwdata/d-link/d-link_dwl-2100ap_a3",
    routerPath: "d-link~d-link_dwl-2100ap_a3"
  },
  {
    brand: "D-Link",
    model: "DWR-116",
    page: "/toh/d-link/d-link_dwr-116",
    techData: "/toh/hwdata/d-link/d-link_dwr-116_a1",
    routerPath: "d-link~d-link_dwr-116_a1"
  },
  {
    brand: "D-Link",
    model: "DHP-1565",
    page: "/toh/d-link/dhp-1565",
    techData: "/toh/hwdata/d-link/d-link_dhp-1565",
    routerPath: "d-link~d-link_dhp-1565"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_c1",
    routerPath: "d-link~d-link_dir-615_c1"
  },
  {
    brand: "D-Link",
    model: "DCH-M225",
    page: "/toh/d-link/d-link_dch-m225_a1",
    techData: "/toh/hwdata/d-link/d-link_dch-m225_a1",
    routerPath: "d-link~d-link_dch-m225_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-610",
    page: "/toh/d-link/d-link_dir-610_a1",
    techData: "/toh/hwdata/d-link/d-link_dir-610_a1",
    routerPath: "d-link~d-link_dir-610_a1"
  },
  {
    brand: "D-Link",
    model: "DWR-512",
    page: "/toh/d-link/d-link_dwr-512_b",
    techData: "/toh/hwdata/d-link/d-link_dwr-512_b",
    routerPath: "d-link~d-link_dwr-512_b"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_d3_d4",
    routerPath: "d-link~d-link_dir-615_d3_d4"
  },
  {
    brand: "D-Link",
    model: "DIR-615",
    page: "/toh/d-link/dir-615",
    techData: "/toh/hwdata/d-link/d-link_dir-615_e2_e3",
    routerPath: "d-link~d-link_dir-615_e2_e3"
  },
  {
    brand: "D-Link",
    model: "DGL-5500",
    page: "/toh/d-link/dgl-5500",
    techData: "/toh/hwdata/d-link/d-link_dgl-5500_a1",
    routerPath: "d-link~d-link_dgl-5500_a1"
  },
  {
    brand: "D-Link",
    model: "DIR-600",
    page: "/toh/d-link/dir-300revb",
    techData: "/toh/hwdata/d-link/d-link_dir-600_b6_b6e",
    routerPath: "d-link~d-link_dir-600_b6_b6e"
  },
  {
    brand: "D-Link",
    model: "DIR-600",
    page: "/toh/d-link/dir-300revb",
    techData: "/toh/hwdata/d-link/d-link_dir-600_b5e",
    routerPath: "d-link~d-link_dir-600_b5e"
  },
  {
    brand: "D-Link",
    model: "DWR-921",
    page: "/toh/d-link/d-link_dwr-921",
    techData: "/toh/hwdata/d-link/d-link_dwr-921_c3",
    routerPath: "d-link~d-link_dwr-921_c3"
  },
  {
    brand: "D-Link",
    model: "DWR-921",
    page: "/toh/d-link/d-link_dwr-921",
    techData: "/toh/hwdata/d-link/d-link_dwr-921_c1",
    routerPath: "d-link~d-link_dwr-921_c1"
  },
  {
    brand: "D-Link",
    model: "DNS-313",
    page: "/toh/d-link/dns-313",
    techData: "/toh/hwdata/d-link/d-link_dns-313",
    routerPath: "d-link~d-link_dns-313"
  },
  {
    brand: "D-Link",
    model: "DIR-685",
    page: "/toh/d-link/dir-685",
    techData: "/toh/hwdata/d-link/d-link_dir-685",
    routerPath: "d-link~d-link_dir-685"
  },
  {
    brand: "D-Link",
    model: "DCS-930L/DCS-932L",
    page: "/toh/d-link/dcs-930l",
    techData: "/toh/hwdata/d-link/d-link_dcs-930l_dcs-932l_a1",
    routerPath: "d-link~d-link_dcs-930l_dcs-932l_a1"
  },
  {
    brand: "D-Link",
    model: "DWR-118",
    page: "/toh/d-link/d-link_dwr-118",
    techData: "/toh/hwdata/d-link/d-link_dwr-118_a2",
    routerPath: "d-link~d-link_dwr-118_a2"
  },
  {
    brand: "D-Link",
    model: "DWR-118",
    page: "/toh/d-link/d-link_dwr-118",
    techData: "/toh/hwdata/d-link/d-link_dwr-118_a1",
    routerPath: "d-link~d-link_dwr-118_a1"
  },
  {
    brand: "D-Link",
    model: "DWR-922",
    page: "/toh/d-link/d-link_dwr-922",
    techData: "/toh/hwdata/d-link/d-link_dwr-922_e2",
    routerPath: "d-link~d-link_dwr-922_e2"
  },
  {
    brand: "D-Link",
    model: "DIR-842",
    page: "/toh/d-link/d-link_dir-842",
    techData: "/toh/hwdata/d-link/d-link_dir-842_c2",
    routerPath: "d-link~d-link_dir-842_c2"
  },
  {
    brand: "D-Link",
    model: "DIR-842",
    page: "/toh/d-link/d-link_dir-842",
    techData: "/toh/hwdata/d-link/d-link_dir-842_c1",
    routerPath: "d-link~d-link_dir-842_c1"
  },
  {
    brand: "D-Link",
    model: "DIR-842",
    page: "/toh/d-link/d-link_dir-842",
    techData: "/toh/hwdata/d-link/d-link_dir-842_c3",
    routerPath: "d-link~d-link_dir-842_c3"
  },
  {
    brand: "D-Link",
    model: "DAP-2610",
    page: "/toh/d-link/d-link_dap-2610",
    techData: "/toh/hwdata/d-link/d-link_dap-2610",
    routerPath: "d-link~d-link_dap-2610"
  },
  {
    brand: "D-Link",
    model: "DWR-960",
    page: "/toh/d-link/d-link_dwr-960",
    techData: "/toh/hwdata/d-link/d-link_dwr-960",
    routerPath: "d-link~d-link_dwr-960"
  },
  {
    brand: "Davolink",
    model: "DV-201AMR",
    page: "/toh/davolink/dv-201amr",
    techData: "/toh/hwdata/davolink/davolink_dv-201amr",
    routerPath: "davolink~davolink_dv-201amr"
  },
  {
    brand: "Devolo",
    model: "dlan pro wireless 500 plus",
    page: "/toh/devolo/devolo_dlan_pro_wireless_500_plus",
    techData: "/toh/hwdata/devolo/devolo_dlan_pro_wireless_500_plus",
    routerPath: "devolo~devolo_dlan_pro_wireless_500_plus"
  },
  {
    brand: "Devolo",
    model: "dLAN pro 1200+ WiFi ac",
    page: "/toh/devolo/devolo_dlan_pro_1200_wifi_ac",
    techData: "/toh/hwdata/devolo/devolo_dlan_pro_1200_wifi_ac",
    routerPath: "devolo~devolo_dlan_pro_1200_wifi_ac"
  },
  {
    brand: "Devolo",
    model: "WiFi pro 1750e",
    page: "/toh/devolo/devolo_wifi_pro_1750e",
    techData: "/toh/hwdata/devolo/devolo_wifi_pro_1750e",
    routerPath: "devolo~devolo_wifi_pro_1750e"
  },
  {
    brand: "Dovado",
    model: "Tiny AC",
    page: "/toh/dovado/dovado_tiny_ac",
    techData: "/toh/hwdata/dovado/dovado_tiny_ac",
    routerPath: "dovado~dovado_tiny_ac"
  },
  {
    brand: "Dragino",
    model: "MS12",
    page: "/toh/dragino/ms12",
    techData: "/toh/hwdata/dragino/dragino_ms12",
    routerPath: "dragino~dragino_ms12"
  },
  {
    brand: "Dragino",
    model: "MS14",
    page: "/toh/dragino/ms14",
    techData: "/toh/hwdata/dragino/dragino_ms14",
    routerPath: "dragino~dragino_ms14"
  },
  {
    brand: "EasyAcc",
    model: "Wi-Stor Wizard 8800",
    page: "/toh/easyacc/wizard8800",
    techData: "/toh/hwdata/easyacc/easyacc_wi-stor_wizard_8800",
    routerPath: "easyacc~easyacc_wi-stor_wizard_8800"
  },
  {
    brand: "EasyLink",
    model: "M-150",
    page: "/toh/easylink/el-m150",
    techData: "/toh/hwdata/easylink/easylink_m-150",
    routerPath: "easylink~easylink_m-150"
  },
  {
    brand: "EasyLink",
    model: "M-mini",
    page: "/toh/easylink/el-m-mini",
    techData: "/toh/hwdata/easylink/easylink_m-mini",
    routerPath: "easylink~easylink_m-mini"
  },
  {
    brand: "Edimax",
    model: "3g-6200n",
    page: "/toh/edimax/3g-6200n",
    techData: "/toh/hwdata/edimax/edimax_3g-6200n",
    routerPath: "edimax~edimax_3g-6200n"
  },
  {
    brand: "Edimax",
    model: "BR-6475nD",
    page: "/toh/edimax/br-6475nd",
    techData: "/toh/hwdata/edimax/edimax_br-6475nd_a",
    routerPath: "edimax~edimax_br-6475nd_a"
  },
  {
    brand: "Edimax",
    model: "BR-6478AC",
    page: "/toh/edimax/edimax_br-6478ac_v2",
    techData: "/toh/hwdata/edimax/edimax_br-6478ac_v2",
    routerPath: "edimax~edimax_br-6478ac_v2"
  },
  {
    brand: "Edimax",
    model: "EW-7476RPC",
    page: "/toh/edimax/edimax_ew-7476rpc",
    techData: "/toh/hwdata/edimax/edimax_ew-7476rpc",
    routerPath: "edimax~edimax_ew-7476rpc"
  },
  {
    brand: "Edimax",
    model: "EW-7478APC",
    page: "/toh/edimax/edimax_ew-7478apc",
    techData: "/toh/hwdata/edimax/edimax_ew-7478apc",
    routerPath: "edimax~edimax_ew-7478apc"
  },
  {
    brand: "Edimax",
    model: "Gemini RG21S",
    page: "/toh/edimax/edimax_gemini_rg21s",
    techData: "/toh/hwdata/edimax/edimax_gemini_rg21s",
    routerPath: "edimax~edimax_gemini_rg21s"
  },
  {
    brand: "Embedded Wireless",
    model: "Dorin Platform",
    page: "/toh/ew/dorin",
    techData: "/toh/hwdata/embeddedwireless/embeddedwireless_dorinplatform",
    routerPath: "embeddedwireless~embeddedwireless_dorinplatform"
  },
  {
    brand: "EnGenius",
    model: "ENS202EXT",
    page: "/toh/engenius/engenius_ens202ext_1.0.0",
    techData: "/toh/hwdata/engenius/engenius_ens202ext_1.0.0",
    routerPath: "engenius~engenius_ens202ext_1.0.0"
  },
  {
    brand: "EnGenius",
    model: "EPG5000",
    page: "/toh/engenius/epg5000",
    techData: "/toh/hwdata/engenius/engenius_epg5000_v100",
    routerPath: "engenius~engenius_epg5000_v100"
  },
  {
    brand: "EnGenius",
    model: "ESR6650",
    page: "/toh/engenius/esr-6650",
    techData: "/toh/hwdata/engenius/engenius_esr-6650",
    routerPath: "engenius~engenius_esr-6650"
  },
  {
    brand: "EnGenius",
    model: "ESR-9753",
    page: "/toh/engenius/engenius_esr9753",
    techData: "/toh/hwdata/engenius/engenius_esr-9753",
    routerPath: "engenius~engenius_esr-9753"
  },
  {
    brand: "EnGenius",
    model: "ESR900",
    page: "/toh/engenius/esr900",
    techData: "/toh/hwdata/engenius/engenius_esr900_v100",
    routerPath: "engenius~engenius_esr900_v100"
  },
  {
    brand: "EnGenius",
    model: "ESR1750",
    page: "/toh/engenius/esr1750",
    techData: "/toh/hwdata/engenius/engenius_esr1750_v110",
    routerPath: "engenius~engenius_esr1750_v110"
  },
  {
    brand: "EnGenius",
    model: "ESR9850",
    page: "/toh/engenius/esr9850",
    techData: "/toh/hwdata/engenius/engenius_esr9850",
    routerPath: "engenius~engenius_esr9850"
  },
  {
    brand: "EnGenius",
    model: "EAP300",
    page: "/toh/engenius/engenius_eap300_v2",
    techData: "/toh/hwdata/engenius/engenius_eap300_v2",
    routerPath: "engenius~engenius_eap300_v2"
  },
  {
    brand: "EnGenius",
    model: "EAP1300",
    page: "/toh/engenius/engenius_eap1300",
    techData: "/toh/hwdata/engenius/engenius_eap1300",
    routerPath: "engenius~engenius_eap1300"
  },
  {
    brand: "EnGenius",
    model: "EWS511AP",
    page: "/toh/engenius/engenius_ews511ap",
    techData: "/toh/hwdata/engenius/engenius_ews511ap",
    routerPath: "engenius~engenius_ews511ap"
  },
  {
    brand: "EnGenius",
    model: "ENS620EXT",
    page: "/toh/engenius/engenius_ens620ext",
    techData: "/toh/hwdata/engenius/engenius_ens620ext",
    routerPath: "engenius~engenius_ens620ext"
  },
  {
    brand: "EnGenius",
    model: "ECB1750",
    page: "/toh/engenius/engenius_ecb1750",
    techData: "/toh/hwdata/engenius/engenius_ecb1750",
    routerPath: "engenius~engenius_ecb1750"
  },
  {
    brand: "Evaluation boards / unbranded boards",
    model: "A5-V11",
    page: "/toh/unbranded/a5-v11",
    techData: "/toh/hwdata/other/other_a5-v11",
    routerPath: "other~other_a5-v11"
  },
  {
    brand: "Evaluation boards / unbranded boards",
    model: "Ralink V11ST-FE",
    page: "/toh/ralink/v11st-fe",
    techData: "/toh/hwdata/other/other_ralinkv11st-fe",
    routerPath: "other~other_ralinkv11st-fe"
  },
  {
    brand: "Evaluation boards / unbranded boards",
    model: "XDX-RN502J",
    page: "/toh/widemac/sl-r7205",
    techData: "/toh/hwdata/other/other_xdx-rn502j",
    routerPath: "other~other_xdx-rn502j"
  },
  {
    brand: "EZVIZ",
    model: "CS-W3-WD1200G EUP",
    page: "/toh/ezviz/ezviz_cs-w3-wd1200g_eup",
    techData: "/toh/hwdata/ezviz/ezviz_cs-w3-wd1200g_eup",
    routerPath: "ezviz~ezviz_cs-w3-wd1200g_eup"
  },
  {
    brand: "Firefly",
    model: "FireWRT",
    page: "/toh/firefly/firewrt",
    techData: "/toh/hwdata/firefly/firefly_firewrt",
    routerPath: "firefly~firefly_firewrt"
  },
  {
    brand: "Fon",
    model: "Fonera 2.0g",
    page: "/toh/fon/fonera2",
    techData: "/toh/hwdata/fon/fon_fonera2_fon2202",
    routerPath: "fon~fon_fonera2_fon2202"
  },
  {
    brand: "Fon",
    model: "Fonera 2.0n",
    page: "/toh/fon/fonera2.0n",
    techData: "/toh/hwdata/fon/fon_fonera20n_fon2303a",
    routerPath: "fon~fon_fonera20n_fon2303a"
  },
  {
    brand: "Fon",
    model: "Fonera+",
    page: "/toh/fon/fonera2",
    techData: "/toh/hwdata/fon/fon_fonera_fon2201",
    routerPath: "fon~fon_fonera_fon2201"
  },
  {
    brand: "Fon",
    model: "La Fonera",
    page: "/toh/fon/fonera",
    techData: "/toh/hwdata/fon/fon_lafonera_fon2100",
    routerPath: "fon~fon_lafonera_fon2100"
  },
  {
    brand: "Freecom",
    model: "FSG-3",
    page: "/toh/freecom/fsg-3",
    techData: "/toh/hwdata/freecom/freecom_fsg-3",
    routerPath: "freecom~freecom_fsg-3"
  },
  {
    brand: "FriendlyARM",
    model: "NanoPi NEO Plus2",
    page: "/toh/friendlyarm/friendlyarm_nanopi_neo_plus2",
    techData: "/toh/hwdata/friendlyarm/friendlyarm_nanopi_neo_plus2",
    routerPath: "friendlyarm~friendlyarm_nanopi_neo_plus2"
  },
  {
    brand: "FriendlyARM",
    model: "NanoPi NEO Core2",
    page: "/toh/friendlyarm/friendlyarm_nanopi_neo_core2",
    techData: "/toh/hwdata/friendlyarm/friendlyarm_nanopi_neo_core2",
    routerPath: "friendlyarm~friendlyarm_nanopi_neo_core2"
  },
  {
    brand: "Frys",
    model: "FR-54RTR",
    page: "/toh/d-link/dir-600",
    techData: "/toh/hwdata/frys/frys_fr-54rtr",
    routerPath: "frys~frys_fr-54rtr"
  },
  {
    brand: "Gainstrong",
    model: "Oolite",
    page: "/toh/gainstrong/oolitev1",
    techData: "/toh/hwdata/gainstrong/gainstrong_oolite_v1",
    routerPath: "gainstrong~gainstrong_oolite_v1"
  },
  {
    brand: "Gateway",
    model: "7001",
    page: "/toh/gateway/7001",
    techData: "/toh/hwdata/gateway/gateway_7001",
    routerPath: "gateway~gateway_7001"
  },
  {
    brand: "Gateway",
    model: "7001",
    page: "/toh/gateway/7001",
    techData: "/toh/hwdata/gateway/gateway_7001_g",
    routerPath: "gateway~gateway_7001_g"
  },
  {
    brand: "Gateworks",
    model: "Laguna GW2382",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_lagunagw2382",
    routerPath: "gateworks~gateworks_lagunagw2382"
  },
  {
    brand: "Gateworks",
    model: "Avila GW2342",
    page: "/toh/gateworks/gateworks_avila_gw2342",
    techData: "/toh/hwdata/gateworks/gateworks_avila_gw2342",
    routerPath: "gateworks~gateworks_avila_gw2342"
  },
  {
    brand: "Gateworks",
    model: "Cambria GW2350",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_cambriagw2350",
    routerPath: "gateworks~gateworks_cambriagw2350"
  },
  {
    brand: "Gateworks",
    model: "Cambria GW2358-4",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_cambriagw2358-4",
    routerPath: "gateworks~gateworks_cambriagw2358-4"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5530",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_gw5530",
    routerPath: "gateworks~gateworks_gw5530"
  },
  {
    brand: "Gateworks",
    model: "Laguna GW2380",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_lagunagw2380",
    routerPath: "gateworks~gateworks_lagunagw2380"
  },
  {
    brand: "Gateworks",
    model: "Laguna GW2387",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_lagunagw2387",
    routerPath: "gateworks~gateworks_lagunagw2387"
  },
  {
    brand: "Gateworks",
    model: "Laguna GW2388",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_lagunagw2388",
    routerPath: "gateworks~gateworks_lagunagw2388"
  },
  {
    brand: "Gateworks",
    model: "Laguna GW2391",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_lagunagw2391",
    routerPath: "gateworks~gateworks_lagunagw2391"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5100",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5100",
    routerPath: "gateworks~gateworks_ventanagw5100"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5200",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5200",
    routerPath: "gateworks~gateworks_ventanagw5200"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5220",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5220",
    routerPath: "gateworks~gateworks_ventanagw5220"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5300",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5300",
    routerPath: "gateworks~gateworks_ventanagw5300"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5310",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5310",
    routerPath: "gateworks~gateworks_ventanagw5310"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5400",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5400",
    routerPath: "gateworks~gateworks_ventanagw5400"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5410",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5410",
    routerPath: "gateworks~gateworks_ventanagw5410"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5510",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5510",
    routerPath: "gateworks~gateworks_ventanagw5510"
  },
  {
    brand: "Gateworks",
    model: "Ventana GW5520",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_ventanagw5520",
    routerPath: "gateworks~gateworks_ventanagw5520"
  },
  {
    brand: "Gateworks",
    model: "Newport GW6100",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_newport_gw6100",
    routerPath: "gateworks~gateworks_newport_gw6100"
  },
  {
    brand: "Gateworks",
    model: "Newport GW6200",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_newport_gw6200",
    routerPath: "gateworks~gateworks_newport_gw6200"
  },
  {
    brand: "Gateworks",
    model: "Newport GW6300",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_newport_gw6300",
    routerPath: "gateworks~gateworks_newport_gw6300"
  },
  {
    brand: "Gateworks",
    model: "Newport GW6400",
    page: "/toh/gateworks/start",
    techData: "/toh/hwdata/gateworks/gateworks_newport_gw6400",
    routerPath: "gateworks~gateworks_newport_gw6400"
  },
  {
    brand: "Gigaset",
    model: "SX762",
    page: "/toh/gigaset/sx76x",
    techData: "/toh/hwdata/gigaset/gigaset_sx762",
    routerPath: "gigaset~gigaset_sx762"
  },
  {
    brand: "Gigaset",
    model: "SX763",
    page: "/toh/gigaset/sx76x",
    techData: "/toh/hwdata/gigaset/gigaset_sx763",
    routerPath: "gigaset~gigaset_sx763"
  },
  {
    brand: "GL.iNet",
    model: "GL-MT300A",
    page: "/toh/gl.inet/gl-mt300a",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-mt300a",
    routerPath: "gl.inet~gl.inet_gl-mt300a"
  },
  {
    brand: "GL.iNet",
    model: "GL-MT300N",
    page: "/toh/gl.inet/gl.inet_gl-mt300n_v1",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-mt300n_v1",
    routerPath: "gl.inet~gl.inet_gl-mt300n_v1"
  },
  {
    brand: "GL.iNet",
    model: "GL-MT750",
    page: "/toh/gl.inet/gl-mt750",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-mt750",
    routerPath: "gl.inet~gl.inet_gl-mt750"
  },
  {
    brand: "GL.iNet",
    model: "GL-AR300M",
    page: "/toh/gl.inet/gl.inet_gl-ar300m",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-ar300m",
    routerPath: "gl.inet~gl.inet_gl-ar300m"
  },
  {
    brand: "GL.iNet",
    model: "6408A",
    page: "/toh/gl.inet/gl-inet_64xx",
    techData: "/toh/hwdata/gl.inet/gl.inet_6408a",
    routerPath: "gl.inet~gl.inet_6408a"
  },
  {
    brand: "GL.iNet",
    model: "6416A",
    page: "/toh/gl.inet/gl-inet_64xx",
    techData: "/toh/hwdata/gl.inet/gl.inet_6416a",
    routerPath: "gl.inet~gl.inet_6416a"
  },
  {
    brand: "GL.iNet",
    model: "GL-AR150",
    page: "/toh/gl.inet/gl-ar150",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-ar150",
    routerPath: "gl.inet~gl.inet_gl-ar150"
  },
  {
    brand: "GL.iNet",
    model: "GL-AR300",
    page: "/toh/gl.inet/gl-ar300",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-ar300",
    routerPath: "gl.inet~gl.inet_gl-ar300"
  },
  {
    brand: "GL.iNet",
    model: "GL-MT300N",
    page: "/toh/gl.inet/gl.inet_gl-mt300n_v2",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-mt300n_v2",
    routerPath: "gl.inet~gl.inet_gl-mt300n_v2"
  },
  {
    brand: "GL.iNet",
    model: "GL-USB150",
    page: "/toh/gl.inet/gl.inet_gl-usb150",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-usb150",
    routerPath: "gl.inet~gl.inet_gl-usb150"
  },
  {
    brand: "GL.iNet",
    model: "GL-AR750",
    page: "/toh/gl.inet/gl-ar750",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-ar750",
    routerPath: "gl.inet~gl.inet_gl-ar750"
  },
  {
    brand: "GL.iNet",
    model: "GL-B1300",
    page: "/toh/gl.inet/gl.inet_gl-b1300",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-b1300",
    routerPath: "gl.inet~gl.inet_gl-b1300"
  },
  {
    brand: "GL.iNet",
    model: "GL-AR750S",
    page: "/toh/gl.inet/gl-ar750s",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-ar750s",
    routerPath: "gl.inet~gl.inet_gl-ar750s"
  },
  {
    brand: "GL.iNet",
    model: "GL-AR300M-Lite",
    page: "/toh/gl.inet/gl.inet_gl-ar300m-lite",
    techData: "/toh/hwdata/gl.inet/gl.inet_gl-ar300m-lite",
    routerPath: "gl.inet~gl.inet_gl-ar300m-lite"
  },
  {
    brand: "HAME",
    model: "MPR-A1",
    page: "/toh/hame/mpr-a1",
    techData: "/toh/hwdata/hame/hame_mpr-a1",
    routerPath: "hame~hame_mpr-a1"
  },
  {
    brand: "HAME",
    model: "MPR-A2",
    page: "/toh/hame/mpr-a2",
    techData: "/toh/hwdata/hame/hame_mpr-a2",
    routerPath: "hame~hame_mpr-a2"
  },
  {
    brand: "Hi-Link",
    model: "HLK-RM04",
    page: "/toh/hilink/hlk-rm04",
    techData: "/toh/hwdata/hi-link/hi-link_hlk-rm04_v1",
    routerPath: "hi-link~hi-link_hlk-rm04_v1"
  },
  {
    brand: "Hi-Link",
    model: "HLK-RM04",
    page: "/toh/hilink/hlk-rm04",
    techData: "/toh/hwdata/hi-link/hi-link_hlk-rm04_v2",
    routerPath: "hi-link~hi-link_hlk-rm04_v2"
  },
  {
    brand: "HiWiFi/Gee",
    model: "HC5661",
    page: "/toh/hiwifi/hiwifi_gee_hc5661",
    techData: "/toh/hwdata/hiwifi_gee/hiwifi_gee_hc5661",
    routerPath: "hiwifi_gee~hiwifi_gee_hc5661"
  },
  {
    brand: "HiWiFi/Gee",
    model: "HC5761",
    page: "/toh/hiwifi/hiwifi_gee_hc5661",
    techData: "/toh/hwdata/hiwifi_gee/hiwifi_gee_hc5761",
    routerPath: "hiwifi_gee~hiwifi_gee_hc5761"
  },
  {
    brand: "HiWiFi/Gee",
    model: "HC5861",
    page: "/toh/hiwifi/hiwifi_gee_hc5661",
    techData: "/toh/hwdata/hiwifi_gee/hiwifi_gee_hc5861",
    routerPath: "hiwifi_gee~hiwifi_gee_hc5861"
  },
  {
    brand: "HiWiFi/Gee",
    model: "HC6361",
    page: "/toh/hiwifi/hc6361",
    techData: "/toh/hwdata/hiwifi_gee/hiwifi_gee_hc6361",
    routerPath: "hiwifi_gee~hiwifi_gee_hc6361"
  },
  {
    brand: "HooToo",
    model: "HT-TM01 (TripMate)",
    page: "/toh/hootoo/tripmate-nano",
    techData: "/toh/hwdata/hootoo/hootoo_ht-tm01",
    routerPath: "hootoo~hootoo_ht-tm01"
  },
  {
    brand: "HooToo",
    model: "HT-TM04 (TripMate Elite)",
    page: "/toh/hootoo/tripmate-nano",
    techData: "/toh/hwdata/hootoo/hootoo_tripmate_elite_ht-tm04",
    routerPath: "hootoo~hootoo_tripmate_elite_ht-tm04"
  },
  {
    brand: "HooToo",
    model: "HT-TM02 (TripMate Nano)",
    page: "/toh/hootoo/tripmate-nano",
    techData: "/toh/hwdata/hootoo/hootoo_tripmatenano_v15",
    routerPath: "hootoo~hootoo_tripmatenano_v15"
  },
  {
    brand: "Huawei",
    model: "E970",
    page: "/toh/huawei/e970",
    techData: "/toh/hwdata/huawei/huawei_e970",
    routerPath: "huawei~huawei_e970"
  },
  {
    brand: "Huawei",
    model: "HG255D",
    page: "/toh/huawei/hg255d",
    techData: "/toh/hwdata/huawei/huawei_hg255d",
    routerPath: "huawei~huawei_hg255d"
  },
  {
    brand: "Huawei",
    model: "HG520v",
    page: "/toh/huawei/hg520v",
    techData: "/toh/hwdata/huawei/huawei_hg520v",
    routerPath: "huawei~huawei_hg520v"
  },
  {
    brand: "Huawei",
    model: "HG553",
    page: "/toh/huawei/hg553",
    techData: "/toh/hwdata/huawei/huawei_hg553",
    routerPath: "huawei~huawei_hg553"
  },
  {
    brand: "Huawei",
    model: "HG556a",
    page: "/toh/huawei/hg556a",
    techData: "/toh/hwdata/huawei/huawei_hg556a_c",
    routerPath: "huawei~huawei_hg556a_c"
  },
  {
    brand: "Huawei",
    model: "HG622",
    page: "/toh/huawei/hg622",
    techData: "/toh/hwdata/huawei/huawei_hg622",
    routerPath: "huawei~huawei_hg622"
  },
  {
    brand: "Huawei",
    model: "HG655b",
    page: "/toh/huawei/hg655d",
    techData: "/toh/hwdata/huawei/huawei_hg655b",
    routerPath: "huawei~huawei_hg655b"
  },
  {
    brand: "Huawei",
    model: "HG655d",
    page: "/toh/huawei/hg655d",
    techData: "/toh/hwdata/huawei/huawei_hg655d",
    routerPath: "huawei~huawei_hg655d"
  },
  {
    brand: "Huawei",
    model: "HG556a",
    page: "/toh/huawei/hg556a",
    techData: "/toh/hwdata/huawei/huawei_hg556a_b",
    routerPath: "huawei~huawei_hg556a_b"
  },
  {
    brand: "Huawei",
    model: "HG556a",
    page: "/toh/huawei/hg556a",
    techData: "/toh/hwdata/huawei/huawei_hg556a_a",
    routerPath: "huawei~huawei_hg556a_a"
  },
  {
    brand: "Huawei",
    model: "HG622u",
    page: "/toh/huawei/hg622u",
    techData: "/toh/hwdata/huawei/huawei_hg622u",
    routerPath: "huawei~huawei_hg622u"
  },
  {
    brand: "I-O Data",
    model: "WN-AC1600DGR2",
    page: "/toh/i-o_data/i-o_data_wn-ac1600dgr",
    techData: "/toh/hwdata/i-o_data/i-o_data_wn-ac1600dgr2",
    routerPath: "i-o_data~i-o_data_wn-ac1600dgr2"
  },
  {
    brand: "I-O Data",
    model: "WN-AC1600DGR",
    page: "/toh/i-o_data/i-o_data_wn-ac1600dgr",
    techData: "/toh/hwdata/i-o_data/i-o_data_wn-ac1600dgr",
    routerPath: "i-o_data~i-o_data_wn-ac1600dgr"
  },
  {
    brand: "I-O Data",
    model: "WN-AC1600DGR3",
    page: "/toh/i-o_data/i-o_data_wn-ac1600dgr",
    techData: "/toh/hwdata/i-o_data/i-o_data_wn-ac1600dgr3",
    routerPath: "i-o_data~i-o_data_wn-ac1600dgr3"
  },
  {
    brand: "I-O Data",
    model: "WN-AX2033GR",
    page: "/toh/i-o_data/i-o_data_wn-ax2033gr",
    techData: "/toh/hwdata/i-o_data/i-o_data_wn-ax2033gr",
    routerPath: "i-o_data~i-o_data_wn-ax2033gr"
  },
  {
    brand: "I2SE",
    model: "Duckbill",
    page: "/toh/i2se/duckbill",
    techData: "/toh/hwdata/i2se/i2se_duckbill",
    routerPath: "i2se~i2se_duckbill"
  },
  {
    brand: "Intellidesign",
    model: "Hyrax PCP-100",
    page: "/toh/intellidesign/hyrax",
    techData: "/toh/hwdata/intellidesign/intellidesign_hyraxpcp-100",
    routerPath: "intellidesign~intellidesign_hyraxpcp-100"
  },
  {
    brand: "Intenso",
    model: "Memory 2 Move",
    page: "/toh/intenso/m2m",
    techData: "/toh/hwdata/intenso/intenso_memory2move",
    routerPath: "intenso~intenso_memory2move"
  },
  {
    brand: "Inventel",
    model: "Livebox 1",
    page: "/toh/inventel/dv4210",
    techData: "/toh/hwdata/inventel/inventel_livebox1_dv4210",
    routerPath: "inventel~inventel_livebox1_dv4210"
  },
  {
    brand: "Iomega",
    model: "iConnect",
    page: "/toh/iomega/iconnect",
    techData: "/toh/hwdata/iomega/iomega_iconnect",
    routerPath: "iomega~iomega_iconnect"
  },
  {
    brand: "ITian",
    model: "Square One SQ201",
    page: "/toh/itian/sq201",
    techData: "/toh/hwdata/itian/itian_square_one_sq201",
    routerPath: "itian~itian_square_one_sq201"
  },
  {
    brand: "JCG",
    model: "JHR-N805R",
    page: "/toh/jcg/jhr-n805r",
    techData: "/toh/hwdata/jcg/jcg_jhr-n805r",
    routerPath: "jcg~jcg_jhr-n805r"
  },
  {
    brand: "JCG",
    model: "JHR-N825R",
    page: "/toh/jcg/jhr-n825r",
    techData: "/toh/hwdata/jcg/jcg_jhr-n825r",
    routerPath: "jcg~jcg_jhr-n825r"
  },
  {
    brand: "JCG",
    model: "JHR-N926R",
    page: "/toh/jcg/jhr-n926r",
    techData: "/toh/hwdata/jcg/jcg_jhr-n926r",
    routerPath: "jcg~jcg_jhr-n926r"
  },
  {
    brand: "jjPlus",
    model: "JA76PF2",
    page: "/toh/jjplus/ja76pf2",
    techData: "/toh/hwdata/jjplus/jjplus_ja76pf2",
    routerPath: "jjplus~jjplus_ja76pf2"
  },
  {
    brand: "Kingston",
    model: "MLW221",
    page: "/toh/kingston/mlw221",
    techData: "/toh/hwdata/kingston/kingston_mlw221",
    routerPath: "kingston~kingston_mlw221"
  },
  {
    brand: "Kingston",
    model: "MLWG2",
    page: "/toh/kingston/mlwg2",
    techData: "/toh/hwdata/kingston/kingston_mlwg2",
    routerPath: "kingston~kingston_mlwg2"
  },
  {
    brand: "Lamobo",
    model: "BananaPi R1",
    page: "/toh/lamobo/bananapi_r1",
    techData: "/toh/hwdata/lamobo/lamobo_bananapi_r1",
    routerPath: "lamobo~lamobo_bananapi_r1"
  },
  {
    brand: "Lemaker",
    model: "Banana Pro",
    page: "/toh/lemaker/bananapro",
    techData: "/toh/hwdata/lemaker/lemaker_banana_pro",
    routerPath: "lemaker~lemaker_banana_pro"
  },
  {
    brand: "Lemaker",
    model: "BananaPi",
    page: "/toh/lemaker/bananapi",
    techData: "/toh/hwdata/lemaker/lemaker_bananapi",
    routerPath: "lemaker~lemaker_bananapi"
  },
  {
    brand: "Lenovo",
    model: "Newifi D1",
    page: "/toh/lenovo/lenovo_newifi_d1",
    techData: "/toh/hwdata/lenovo/lenovo_newifi_d1",
    routerPath: "lenovo~lenovo_newifi_d1"
  },
  {
    brand: "Lenovo",
    model: "Newifi mini Y1",
    page: "/toh/lenovo/lenovo_y1_v1",
    techData: "/toh/hwdata/lenovo/lenovo_newifi_mini_y1",
    routerPath: "lenovo~lenovo_newifi_mini_y1"
  },
  {
    brand: "Linksprite",
    model: "pcDuino3",
    page: "/toh/pcduino/pcduino3",
    techData: "/toh/hwdata/linksprite/linksprite_pcduino3",
    routerPath: "linksprite~linksprite_pcduino3"
  },
  {
    brand: "Linksprite",
    model: "pcDuino1",
    page: "/toh/pcduino/pcduino",
    techData: "/toh/hwdata/linksprite/linksprite_pcduino1",
    routerPath: "linksprite~linksprite_pcduino1"
  },
  {
    brand: "Linksys",
    model: "AG241",
    page: "/toh/linksys/ag241",
    techData: "/toh/hwdata/linksys/linksys_ag241_v1",
    routerPath: "linksys~linksys_ag241_v1"
  },
  {
    brand: "Linksys",
    model: "AG241",
    page: "/toh/linksys/ag241",
    techData: "/toh/hwdata/linksys/linksys_ag241_v2",
    routerPath: "linksys~linksys_ag241_v2"
  },
  {
    brand: "Linksys",
    model: "AG241",
    page: "/toh/linksys/ag241",
    techData: "/toh/hwdata/linksys/linksys_ag241_v2b",
    routerPath: "linksys~linksys_ag241_v2b"
  },
  {
    brand: "Linksys",
    model: "E1700",
    page: "/toh/linksys/e1700",
    techData: "/toh/hwdata/linksys/linksys_e1700",
    routerPath: "linksys~linksys_e1700"
  },
  {
    brand: "Linksys",
    model: "E4200",
    page: "/toh/linksys/ea4500",
    techData: "/toh/hwdata/linksys/linksys_e4200_v2",
    routerPath: "linksys~linksys_e4200_v2"
  },
  {
    brand: "Linksys",
    model: "EA3500",
    page: "/toh/linksys/ea3500",
    techData: "/toh/hwdata/linksys/linksys_ea3500",
    routerPath: "linksys~linksys_ea3500"
  },
  {
    brand: "Linksys",
    model: "EA8500",
    page: "/toh/linksys/linksys_ea8500",
    techData: "/toh/hwdata/linksys/linksys_ea8500",
    routerPath: "linksys~linksys_ea8500"
  },
  {
    brand: "Linksys",
    model: "NSLU2",
    page: "/toh/linksys/nslu2",
    techData: "/toh/hwdata/linksys/linksys_nslu2",
    routerPath: "linksys~linksys_nslu2"
  },
  {
    brand: "Linksys",
    model: "RE6500",
    page: "/toh/linksys/re6500",
    techData: "/toh/hwdata/linksys/linksys_re6500",
    routerPath: "linksys~linksys_re6500"
  },
  {
    brand: "Linksys",
    model: "WAG54G",
    page: "/toh/linksys/wag54g",
    techData: "/toh/hwdata/linksys/linksys_wag54g_v2",
    routerPath: "linksys~linksys_wag54g_v2"
  },
  {
    brand: "Linksys",
    model: "WAG54G",
    page: "/toh/linksys/wag54g",
    techData: "/toh/hwdata/linksys/linksys_wag54g_v3",
    routerPath: "linksys~linksys_wag54g_v3"
  },
  {
    brand: "Linksys",
    model: "WAG54G",
    page: "/toh/linksys/wag54g",
    techData: "/toh/hwdata/linksys/linksys_wag54g_v11",
    routerPath: "linksys~linksys_wag54g_v11"
  },
  {
    brand: "Linksys",
    model: "WAG354G",
    page: "/toh/linksys/wag354g",
    techData: "/toh/hwdata/linksys/linksys_wag354g",
    routerPath: "linksys~linksys_wag354g"
  },
  {
    brand: "Linksys",
    model: "WAG354G",
    page: "/toh/linksys/wag354g",
    techData: "/toh/hwdata/linksys/linksys_wag354g_2",
    routerPath: "linksys~linksys_wag354g_2"
  },
  {
    brand: "Linksys",
    model: "WRT54G-TM",
    page: "/toh/linksys/wrt54g-tm",
    techData: "/toh/hwdata/linksys/linksys_wrt54g-tm_10",
    routerPath: "linksys~linksys_wrt54g-tm_10"
  },
  {
    brand: "Linksys",
    model: "WRT54G",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54g",
    routerPath: "linksys~linksys_wrt54g"
  },
  {
    brand: "Linksys",
    model: "WRT54G3GV2(-VF)",
    page: "/toh/linksys/wrt54g3gv2-vf",
    techData: "/toh/hwdata/linksys/linksys_wrt54g3gv2_10",
    routerPath: "linksys~linksys_wrt54g3gv2_10"
  },
  {
    brand: "Linksys",
    model: "WRT54G",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54g_10",
    routerPath: "linksys~linksys_wrt54g_10"
  },
  {
    brand: "Linksys",
    model: "WRT54G",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54g_11",
    routerPath: "linksys~linksys_wrt54g_11"
  },
  {
    brand: "Linksys",
    model: "WRT54G",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54g_31",
    routerPath: "linksys~linksys_wrt54g_31"
  },
  {
    brand: "Linksys",
    model: "WRT54G",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54g_40",
    routerPath: "linksys~linksys_wrt54g_40"
  },
  {
    brand: "Linksys",
    model: "WRT54GL",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54gl",
    routerPath: "linksys~linksys_wrt54gl"
  },
  {
    brand: "Linksys",
    model: "WRT54GS",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54gs",
    routerPath: "linksys~linksys_wrt54gs"
  },
  {
    brand: "Linksys",
    model: "WRT150N",
    page: "/toh/linksys/wrt150n",
    techData: "/toh/hwdata/linksys/linksys_wrt150n_v1",
    routerPath: "linksys~linksys_wrt150n_v1"
  },
  {
    brand: "Linksys",
    model: "WRT160N",
    page: "/toh/linksys/wrt160n",
    techData: "/toh/hwdata/linksys/linksys_wrt160n_v3",
    routerPath: "linksys~linksys_wrt160n_v3"
  },
  {
    brand: "Linksys",
    model: "WRT160NL",
    page: "/toh/linksys/wrt160nl",
    techData: "/toh/hwdata/linksys/linksys_wrt160nl",
    routerPath: "linksys~linksys_wrt160nl"
  },
  {
    brand: "Linksys",
    model: "WRT310N",
    page: "/toh/linksys/wrt310n_v2",
    techData: "/toh/hwdata/linksys/linksys_wrt310n_v2",
    routerPath: "linksys~linksys_wrt310n_v2"
  },
  {
    brand: "Linksys",
    model: "WRT320N",
    page: "/toh/linksys/wrt320n",
    techData: "/toh/hwdata/linksys/linksys_wrt320n_v1",
    routerPath: "linksys~linksys_wrt320n_v1"
  },
  {
    brand: "Linksys",
    model: "WRT600N",
    page: "/toh/linksys/linksys_wrt600n_1.1",
    techData: "/toh/hwdata/linksys/linksys_wrt600n_1.1",
    routerPath: "linksys~linksys_wrt600n_1.1"
  },
  {
    brand: "Linksys",
    model: "WRT1200AC",
    page: "/toh/linksys/linksys_wrt1200ac",
    techData: "/toh/hwdata/linksys/linksys_wrt1200ac",
    routerPath: "linksys~linksys_wrt1200ac"
  },
  {
    brand: "Linksys",
    model: "WRT1900AC",
    page: "/toh/linksys/linksys_wrt1900ac",
    techData: "/toh/hwdata/linksys/linksys_wrt1900ac_v1",
    routerPath: "linksys~linksys_wrt1900ac_v1"
  },
  {
    brand: "Linksys",
    model: "WRT1900AC",
    page: "/toh/linksys/linksys_wrt1900ac",
    techData: "/toh/hwdata/linksys/linksys_wrt1900ac_v2",
    routerPath: "linksys~linksys_wrt1900ac_v2"
  },
  {
    brand: "Linksys",
    model: "WRT1900ACS",
    page: "/toh/linksys/linksys_wrt1900acs",
    techData: "/toh/hwdata/linksys/linksys_wrt1900acs_v1",
    routerPath: "linksys~linksys_wrt1900acs_v1"
  },
  {
    brand: "Linksys",
    model: "WRT3200ACM",
    page: "/toh/linksys/linksys_wrt3200acm",
    techData: "/toh/hwdata/linksys/linksys_wrt3200acm",
    routerPath: "linksys~linksys_wrt3200acm"
  },
  {
    brand: "Linksys",
    model: "WRTSL54GS",
    page: "/toh/linksys/wrtsl54gs",
    techData: "/toh/hwdata/linksys/linksys_wrtsl54gs",
    routerPath: "linksys~linksys_wrtsl54gs"
  },
  {
    brand: "Linksys",
    model: "WRT310N",
    page: "/toh/linksys/wrt310n_v1",
    techData: "/toh/hwdata/linksys/linksys_wrt310n_v1",
    routerPath: "linksys~linksys_wrt310n_v1"
  },
  {
    brand: "Linksys",
    model: "E1200",
    page: "/toh/linksys/linksys_e1200_2",
    techData: "/toh/hwdata/linksys/linksys_e1200_v2",
    routerPath: "linksys~linksys_e1200_v2"
  },
  {
    brand: "Linksys",
    model: "E2500",
    page: "/toh/linksys/linksys_e2500_v1",
    techData: "/toh/hwdata/linksys/linksys_e2500_v1",
    routerPath: "linksys~linksys_e2500_v1"
  },
  {
    brand: "Linksys",
    model: "E3000",
    page: "/toh/linksys/e3000",
    techData: "/toh/hwdata/linksys/linksys_e3000_v1",
    routerPath: "linksys~linksys_e3000_v1"
  },
  {
    brand: "Linksys",
    model: "WRT300N",
    page: "/toh/linksys/wrt300n",
    techData: "/toh/hwdata/linksys/linksys_wrt300n_v1.1",
    routerPath: "linksys~linksys_wrt300n_v1.1"
  },
  {
    brand: "Linksys",
    model: "WRT350N",
    page: "/toh/linksys/wrt350nv1",
    techData: "/toh/hwdata/linksys/linksys_wrt350n_v1",
    routerPath: "linksys~linksys_wrt350n_v1"
  },
  {
    brand: "Linksys",
    model: "WRT350N v2",
    page: "/toh/linksys/wrt350nv2",
    techData: "/toh/hwdata/linksys/linksys_wrt350n_v2",
    routerPath: "linksys~linksys_wrt350n_v2"
  },
  {
    brand: "Linksys",
    model: "WRT610N",
    page: "/toh/linksys/wrt610n",
    techData: "/toh/hwdata/linksys/linksys_wrt610n_v1",
    routerPath: "linksys~linksys_wrt610n_v1"
  },
  {
    brand: "Linksys",
    model: "WRT610N",
    page: "/toh/linksys/wrt610n",
    techData: "/toh/hwdata/linksys/linksys_wrt610n_v2",
    routerPath: "linksys~linksys_wrt610n_v2"
  },
  {
    brand: "Linksys",
    model: "E1000",
    page: "/toh/linksys/e1000",
    techData: "/toh/hwdata/linksys/linksys_e1000_v1",
    routerPath: "linksys~linksys_e1000_v1"
  },
  {
    brand: "Linksys",
    model: "E1000",
    page: "/toh/linksys/e1000",
    techData: "/toh/hwdata/linksys/linksys_e1000_v2",
    routerPath: "linksys~linksys_e1000_v2"
  },
  {
    brand: "Linksys",
    model: "E2000",
    page: "/toh/linksys/e2000",
    techData: "/toh/hwdata/linksys/linksys_e2000_v1",
    routerPath: "linksys~linksys_e2000_v1"
  },
  {
    brand: "Linksys",
    model: "E3200",
    page: "/toh/linksys/e3200",
    techData: "/toh/hwdata/linksys/linksys_e3200_v1",
    routerPath: "linksys~linksys_e3200_v1"
  },
  {
    brand: "Linksys",
    model: "E900",
    page: "/toh/linksys/e900",
    techData: "/toh/hwdata/linksys/linksys_e900_v1",
    routerPath: "linksys~linksys_e900_v1"
  },
  {
    brand: "Linksys",
    model: "EA4500",
    page: "/toh/linksys/ea4500",
    techData: "/toh/hwdata/linksys/linksys_ea4500_v1",
    routerPath: "linksys~linksys_ea4500_v1"
  },
  {
    brand: "Linksys",
    model: "EA4500",
    page: "/toh/linksys/ea4500",
    techData: "/toh/hwdata/linksys/linksys_ea4500_v2",
    routerPath: "linksys~linksys_ea4500_v2"
  },
  {
    brand: "Linksys",
    model: "WRT32X",
    page: "/toh/linksys/linksys_wrt32x",
    techData: "/toh/hwdata/linksys/linksys_wrt32x_v1_venom",
    routerPath: "linksys~linksys_wrt32x_v1_venom"
  },
  {
    brand: "Linksys",
    model: "WRT54GS",
    page: "/toh/linksys/wrt54g",
    techData: "/toh/hwdata/linksys/linksys_wrt54gs_v3",
    routerPath: "linksys~linksys_wrt54gs_v3"
  },
  {
    brand: "Linksys",
    model: "EA6350",
    page: "/toh/linksys/linksys_ea6350_v3",
    techData: "/toh/hwdata/linksys/linksys_ea6350_v3",
    routerPath: "linksys~linksys_ea6350_v3"
  },
  {
    brand: "Linksys",
    model: "EA8300",
    page: "/toh/linksys/linksys_ea8300",
    techData: "/toh/hwdata/linksys/linksys_ea8300",
    routerPath: "linksys~linksys_ea8300"
  },
  {
    brand: "MediaTek",
    model: "LinkIt Smart 7688",
    page: "/toh/seeed/linkit7688",
    techData: "/toh/hwdata/mediatek/mediatek_linkit_smart_7688",
    routerPath: "mediatek~mediatek_linkit_smart_7688"
  },
  {
    brand: "Medion",
    model: "MD86587",
    page: "/toh/medion/md86587",
    techData: "/toh/hwdata/medion/medion_md86587_v1",
    routerPath: "medion~medion_md86587_v1"
  },
  {
    brand: "Meraki",
    model: "Mini",
    page: "/toh/meraki/mini",
    techData: "/toh/hwdata/meraki/meraki_mini",
    routerPath: "meraki~meraki_mini"
  },
  {
    brand: "Meraki",
    model: "MR12",
    page: "/toh/meraki/mr12",
    techData: "/toh/hwdata/meraki/meraki_mr12",
    routerPath: "meraki~meraki_mr12"
  },
  {
    brand: "Meraki",
    model: "MR16",
    page: "/toh/meraki/mr16",
    techData: "/toh/hwdata/meraki/meraki_mr16",
    routerPath: "meraki~meraki_mr16"
  },
  {
    brand: "Meraki",
    model: "MR18",
    page: "/toh/meraki/mr18",
    techData: "/toh/hwdata/meraki/meraki_mr18",
    routerPath: "meraki~meraki_mr18"
  },
  {
    brand: "Meraki",
    model: "MR24",
    page: "/toh/meraki/mr24",
    techData: "/toh/hwdata/meraki/meraki_mr24",
    routerPath: "meraki~meraki_mr24"
  },
  {
    brand: "Meraki",
    model: "MX60",
    page: "/toh/meraki/mx60",
    techData: "/toh/hwdata/meraki/meraki_mx60",
    routerPath: "meraki~meraki_mx60"
  },
  {
    brand: "Meraki",
    model: "MX60W",
    page: "/toh/meraki/mx60w",
    techData: "/toh/hwdata/meraki/meraki_mx60w",
    routerPath: "meraki~meraki_mx60w"
  },
  {
    brand: "Meraki",
    model: "Z1",
    page: "/toh/meraki/z1",
    techData: "/toh/hwdata/meraki/meraki_z1",
    routerPath: "meraki~meraki_z1"
  },
  {
    brand: "Meraki",
    model: "MR33",
    page: "/toh/meraki/mr33",
    techData: "/toh/hwdata/meraki/meraki_mr33",
    routerPath: "meraki~meraki_mr33"
  },
  {
    brand: "Mercury",
    model: "M301",
    page: "/toh/mercury/m301",
    techData: "/toh/hwdata/mercury/mercury_m301_1",
    routerPath: "mercury~mercury_m301_1"
  },
  {
    brand: "Mercury",
    model: "MAC1200R",
    page: "/toh/mercury/mac1200r",
    techData: "/toh/hwdata/mercury/mercury_mac1200r_v11",
    routerPath: "mercury~mercury_mac1200r_v11"
  },
  {
    brand: "Mercury",
    model: "MW150R",
    page: "/toh/mercury/mw150r",
    techData: "/toh/hwdata/mercury/mercury_mw150r_v8",
    routerPath: "mercury~mercury_mw150r_v8"
  },
  {
    brand: "Mercury",
    model: "MW4530R",
    page: "/toh/mercury/mw4530r",
    techData: "/toh/hwdata/mercury/mercury_mw4530r_v1",
    routerPath: "mercury~mercury_mw4530r_v1"
  },
  {
    brand: "Microduino",
    model: "MicroWRT Core",
    page: "/toh/microduino/microwrt",
    techData: "/toh/hwdata/microduino/microduino_microwrt_core",
    routerPath: "microduino~microduino_microwrt_core"
  },
  {
    brand: "MikroTik",
    model: "RB133",
    page: "/toh/mikrotik/rb133",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb133",
    routerPath: "mikrotik~mikrotik_rb133"
  },
  {
    brand: "MikroTik",
    model: "RB411",
    page: "/toh/mikrotik/rb411",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb411",
    routerPath: "mikrotik~mikrotik_rb411"
  },
  {
    brand: "MikroTik",
    model: "RB411AH",
    page: "/toh/mikrotik/rb411",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb411ah",
    routerPath: "mikrotik~mikrotik_rb411ah"
  },
  {
    brand: "MikroTik",
    model: "RB411U",
    page: "/toh/mikrotik/rb411",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb411u",
    routerPath: "mikrotik~mikrotik_rb411u"
  },
  {
    brand: "MikroTik",
    model: "RB411UAHR",
    page: "/toh/mikrotik/rb411",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb411uahr",
    routerPath: "mikrotik~mikrotik_rb411uahr"
  },
  {
    brand: "MikroTik",
    model: "RB433",
    page: "/toh/mikrotik/rb433",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb433",
    routerPath: "mikrotik~mikrotik_rb433"
  },
  {
    brand: "MikroTik",
    model: "RB433AH",
    page: "/toh/mikrotik/rb433",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb433ah",
    routerPath: "mikrotik~mikrotik_rb433ah"
  },
  {
    brand: "MikroTik",
    model: "RB433UAH",
    page: "/toh/mikrotik/rb433",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb433uah",
    routerPath: "mikrotik~mikrotik_rb433uah"
  },
  {
    brand: "MikroTik",
    model: "RB450",
    page: "/toh/mikrotik/rb450g",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb450",
    routerPath: "mikrotik~mikrotik_rb450"
  },
  {
    brand: "MikroTik",
    model: "RB450G",
    page: "/toh/mikrotik/rb450g",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb450g",
    routerPath: "mikrotik~mikrotik_rb450g"
  },
  {
    brand: "MikroTik",
    model: "RB493",
    page: "/toh/mikrotik/mikrotik_routerboard_rb493",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb493",
    routerPath: "mikrotik~mikrotik_rb493"
  },
  {
    brand: "MikroTik",
    model: "RB493G",
    page: "/toh/mikrotik/rb493g",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb493g",
    routerPath: "mikrotik~mikrotik_rb493g"
  },
  {
    brand: "MikroTik",
    model: "RB532",
    page: "/toh/mikrotik/rb532",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb532",
    routerPath: "mikrotik~mikrotik_rb532"
  },
  {
    brand: "MikroTik",
    model: "RB532A",
    page: "/toh/mikrotik/rb532",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb532a",
    routerPath: "mikrotik~mikrotik_rb532a"
  },
  {
    brand: "MikroTik",
    model: "RB750",
    page: "/toh/mikrotik/rb750",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb750",
    routerPath: "mikrotik~mikrotik_rb750"
  },
  {
    brand: "MikroTik",
    model: "RB750r2 (hEX lite)",
    page: "/toh/mikrotik/rb750_r2",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb750_r2",
    routerPath: "mikrotik~mikrotik_rb750_r2"
  },
  {
    brand: "MikroTik",
    model: "RB750GL",
    page: "/toh/mikrotik/rb750gl",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb750gl",
    routerPath: "mikrotik~mikrotik_rb750gl"
  },
  {
    brand: "MikroTik",
    model: "RB750Gr3",
    page: "/toh/mikrotik/mikrotik_rb750gr3",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb750gr3",
    routerPath: "mikrotik~mikrotik_rb750gr3"
  },
  {
    brand: "MikroTik",
    model: "RB750UP",
    page: "/toh/mikrotik/rb750up",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb750up",
    routerPath: "mikrotik~mikrotik_rb750up"
  },
  {
    brand: "MikroTik",
    model: "RB951G-2HnD",
    page: "/toh/mikrotik/rb951g_2hnd",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb951g-2hnd",
    routerPath: "mikrotik~mikrotik_rb951g-2hnd"
  },
  {
    brand: "MikroTik",
    model: "RB951Ui-2HnD",
    page: "/toh/mikrotik/rb951ui",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb951ui-2hnd",
    routerPath: "mikrotik~mikrotik_rb951ui-2hnd"
  },
  {
    brand: "MikroTik",
    model: "RB2011",
    page: "/toh/mikrotik/rb2011",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb2011",
    routerPath: "mikrotik~mikrotik_rb2011"
  },
  {
    brand: "MikroTik",
    model: "RB2011UiAS-2HnD-IN",
    page: "/toh/mikrotik/rb2011uias",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb2011uias-2hnd-in",
    routerPath: "mikrotik~mikrotik_rb2011uias-2hnd-in"
  },
  {
    brand: "MikroTik",
    model: "RB941-2nD (hAP lite)",
    page: "/toh/mikrotik/rb941_2nd",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb941-2nd",
    routerPath: "mikrotik~mikrotik_rb941-2nd"
  },
  {
    brand: "MikroTik",
    model: "RB951Ui-2nD (hAP)",
    page: "/toh/mikrotik/rb951ui-2nd",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb951ui-2nd",
    routerPath: "mikrotik~mikrotik_rb951ui-2nd"
  },
  {
    brand: "MikroTik",
    model: "RB435G",
    page: "/toh/mikrotik/rb435",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb435g",
    routerPath: "mikrotik~mikrotik_rb435g"
  },
  {
    brand: "MikroTik",
    model: "RB750P-PBr2 (PowerBox)",
    page: "/toh/mikrotik/mikrotik_rb750p-pbr2_powerbox",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb750p-pbr2_powerbox",
    routerPath: "mikrotik~mikrotik_rb750p-pbr2_powerbox"
  },
  {
    brand: "MikroTik",
    model: "RBSXT2nDr3 (SXT Lite 2)",
    page: "/toh/mikrotik/mikrotik_rbsxt2ndr3",
    techData: "/toh/hwdata/mikrotik/mikrotik_rbsxt2ndr3_sxt_lite_2",
    routerPath: "mikrotik~mikrotik_rbsxt2ndr3_sxt_lite_2"
  },
  {
    brand: "MikroTik",
    model: "RB912UAG-5HPnd",
    page: "/toh/mikrotik/rb91xg_5hpnd",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb912uag-5hpnd",
    routerPath: "mikrotik~mikrotik_rb912uag-5hpnd"
  },
  {
    brand: "MikroTik",
    model: "RBM33G",
    page: "/toh/mikrotik/mikrotik_rbm33g",
    techData: "/toh/hwdata/mikrotik/mikrotik_rbm33g",
    routerPath: "mikrotik~mikrotik_rbm33g"
  },
  {
    brand: "MikroTik",
    model: "RB450Gx4",
    page: "/inbox/toh/mikrotik/mikrotik_rb450gx4",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb450gx4",
    routerPath: "mikrotik~mikrotik_rb450gx4"
  },
  {
    brand: "MikroTik",
    model: "RB941-2nD-TC (hAP lite)",
    page: "/toh/mikrotik/rb941_2nd",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb941-2nd-tc",
    routerPath: "mikrotik~mikrotik_rb941-2nd-tc"
  },
  {
    brand: "MikroTik",
    model: "RB2011UiAS-2HnD-IN",
    page: "/toh/mikrotik/rb2011uias",
    techData: "/toh/hwdata/mikrotik/mikrotik_rb2011uias-2hnd-in_r2",
    routerPath: "mikrotik~mikrotik_rb2011uias-2hnd-in_r2"
  },
  {
    brand: "MikroTik",
    model: "RBcAPGi-5acD2nD (cAP ac)",
    page: "/inbox/toh/mikrotik/mikrotik_rbcapgi-5acd2nd_cap_ac",
    techData: "/toh/hwdata/mikrotik/mikrotik_rbcapgi-5acd2nd_cap_ac",
    routerPath: "mikrotik~mikrotik_rbcapgi-5acd2nd_cap_ac"
  },
  {
    brand: "MQMaker",
    model: "WiTi Board",
    page: "/toh/mqmaker/witi",
    techData: "/toh/hwdata/mqmaker/mqmaker_witi_board_256mb",
    routerPath: "mqmaker~mqmaker_witi_board_256mb"
  },
  {
    brand: "MQMaker",
    model: "WiTi Board",
    page: "/toh/mqmaker/witi",
    techData: "/toh/hwdata/mqmaker/mqmaker_witi_board_512mb",
    routerPath: "mqmaker~mqmaker_witi_board_512mb"
  },
  {
    brand: "Multilaser",
    model: "RE027",
    page: "/toh/multilaser/re027",
    techData: "/toh/hwdata/multilaser/multilaser_re027_v1",
    routerPath: "multilaser~multilaser_re027_v1"
  },
  {
    brand: "NetComm",
    model: "NB6PLUS4W",
    page: "/toh/netcomm/nb6plus4w",
    techData: "/toh/hwdata/netcomm/netcomm_nb6plus4w",
    routerPath: "netcomm~netcomm_nb6plus4w"
  },
  {
    brand: "Netgear",
    model: "D7800",
    page: "/toh/netgear/netgear_d7800",
    techData: "/toh/hwdata/netgear/netgear_d7800",
    routerPath: "netgear~netgear_d7800"
  },
  {
    brand: "Netgear",
    model: "DG834G",
    page: "/toh/netgear/dg834g",
    techData: "/toh/hwdata/netgear/netgear_dg834g",
    routerPath: "netgear~netgear_dg834g"
  },
  {
    brand: "Netgear",
    model: "DG834G",
    page: "/toh/netgear/dg834g.v3",
    techData: "/toh/hwdata/netgear/netgear_dg834g_v3",
    routerPath: "netgear~netgear_dg834g_v3"
  },
  {
    brand: "Netgear",
    model: "DG834G",
    page: "/toh/netgear/dg834g.v4",
    techData: "/toh/hwdata/netgear/netgear_dg834g_v4",
    routerPath: "netgear~netgear_dg834g_v4"
  },
  {
    brand: "Netgear",
    model: "DG834GT",
    page: "/toh/netgear/dg834gt",
    techData: "/toh/hwdata/netgear/netgear_dg834gt",
    routerPath: "netgear~netgear_dg834gt"
  },
  {
    brand: "Netgear",
    model: "DGN2200",
    page: "/toh/netgear/dgn2200",
    techData: "/toh/hwdata/netgear/netgear_dgn2200_v1",
    routerPath: "netgear~netgear_dgn2200_v1"
  },
  {
    brand: "Netgear",
    model: "DGN3500",
    page: "/toh/netgear/dgn3500b",
    techData: "/toh/hwdata/netgear/netgear_dgn3500",
    routerPath: "netgear~netgear_dgn3500"
  },
  {
    brand: "Netgear",
    model: "DGN3500B",
    page: "/toh/netgear/dgn3500b",
    techData: "/toh/hwdata/netgear/netgear_dgn3500b",
    routerPath: "netgear~netgear_dgn3500b"
  },
  {
    brand: "Netgear",
    model: "DGND3700",
    page: "/toh/netgear/dgnd3700",
    techData: "/toh/hwdata/netgear/netgear_dgnd3700_v1",
    routerPath: "netgear~netgear_dgnd3700_v1"
  },
  {
    brand: "Netgear",
    model: "DGND3800B",
    page: "/toh/netgear/dgnd3700",
    techData: "/toh/hwdata/netgear/netgear_dgnd3800b",
    routerPath: "netgear~netgear_dgnd3800b"
  },
  {
    brand: "Netgear",
    model: "EVG2000",
    page: "/toh/netgear/evg2000",
    techData: "/toh/hwdata/netgear/netgear_evg2000",
    routerPath: "netgear~netgear_evg2000"
  },
  {
    brand: "Netgear",
    model: "EX2700",
    page: "/toh/netgear/netgear_ex2700",
    techData: "/toh/hwdata/netgear/netgear_ex2700",
    routerPath: "netgear~netgear_ex2700"
  },
  {
    brand: "Netgear",
    model: "R6100",
    page: "/toh/netgear/netgear_r6100",
    techData: "/toh/hwdata/netgear/netgear_r6100",
    routerPath: "netgear~netgear_r6100"
  },
  {
    brand: "Netgear",
    model: "R6250",
    page: "/toh/netgear/r6250",
    techData: "/toh/hwdata/netgear/netgear_r6250",
    routerPath: "netgear~netgear_r6250"
  },
  {
    brand: "Netgear",
    model: "R6300",
    page: "/toh/netgear/netgear_r6300_v2",
    techData: "/toh/hwdata/netgear/netgear_r6300_v2",
    routerPath: "netgear~netgear_r6300_v2"
  },
  {
    brand: "Netgear",
    model: "R7000",
    page: "/toh/netgear/r7000",
    techData: "/toh/hwdata/netgear/netgear_r7000",
    routerPath: "netgear~netgear_r7000"
  },
  {
    brand: "Netgear",
    model: "R8000",
    page: "/toh/netgear/r8000",
    techData: "/toh/hwdata/netgear/netgear_r8000",
    routerPath: "netgear~netgear_r8000"
  },
  {
    brand: "Netgear",
    model: "WAG302",
    page: "/toh/netgear/wg302",
    techData: "/toh/hwdata/netgear/netgear_wag302_v1",
    routerPath: "netgear~netgear_wag302_v1"
  },
  {
    brand: "Netgear",
    model: "WAG302",
    page: "/toh/netgear/wg302",
    techData: "/toh/hwdata/netgear/netgear_wag302_v2",
    routerPath: "netgear~netgear_wag302_v2"
  },
  {
    brand: "Netgear",
    model: "WG302",
    page: "/toh/netgear/wg302",
    techData: "/toh/hwdata/netgear/netgear_wg302_v1",
    routerPath: "netgear~netgear_wg302_v1"
  },
  {
    brand: "Netgear",
    model: "WG302",
    page: "/toh/netgear/wg302",
    techData: "/toh/hwdata/netgear/netgear_wg302_v2",
    routerPath: "netgear~netgear_wg302_v2"
  },
  {
    brand: "Netgear",
    model: "WGT634U",
    page: "/toh/netgear/wgt634u",
    techData: "/toh/hwdata/netgear/netgear_wgt634u",
    routerPath: "netgear~netgear_wgt634u"
  },
  {
    brand: "Netgear",
    model: "WNDR37AV",
    page: "/toh/netgear/wndr37av",
    techData: "/toh/hwdata/netgear/netgear_wndr37av_v1",
    routerPath: "netgear~netgear_wndr37av_v1"
  },
  {
    brand: "Netgear",
    model: "WNDR3400",
    page: "/toh/netgear/wndr3400",
    techData: "/toh/hwdata/netgear/netgear_wndr3400_v1",
    routerPath: "netgear~netgear_wndr3400_v1"
  },
  {
    brand: "Netgear",
    model: "WNDR3400",
    page: "/toh/netgear/wndr3400",
    techData: "/toh/hwdata/netgear/netgear_wndr3400_v2",
    routerPath: "netgear~netgear_wndr3400_v2"
  },
  {
    brand: "Netgear",
    model: "WNDR3400",
    page: "/toh/netgear/wndr3400",
    techData: "/toh/hwdata/netgear/netgear_wndr3400_v3",
    routerPath: "netgear~netgear_wndr3400_v3"
  },
  {
    brand: "Netgear",
    model: "WNDR3700",
    page: "/toh/netgear/wndr3700",
    techData: "/toh/hwdata/netgear/netgear_wndr3700_v1",
    routerPath: "netgear~netgear_wndr3700_v1"
  },
  {
    brand: "Netgear",
    model: "WNDR3700",
    page: "/toh/netgear/wndr3700",
    techData: "/toh/hwdata/netgear/netgear_wndr3700_v2",
    routerPath: "netgear~netgear_wndr3700_v2"
  },
  {
    brand: "Netgear",
    model: "WNDR3700",
    page: "/toh/netgear/wndr3700",
    techData: "/toh/hwdata/netgear/netgear_wndr3700_v3",
    routerPath: "netgear~netgear_wndr3700_v3"
  },
  {
    brand: "Netgear",
    model: "WNDR3700",
    page: "/toh/netgear/wndr3700",
    techData: "/toh/hwdata/netgear/netgear_wndr3700_v4",
    routerPath: "netgear~netgear_wndr3700_v4"
  },
  {
    brand: "Netgear",
    model: "WNDR3800",
    page: "/toh/netgear/wndr3800",
    techData: "/toh/hwdata/netgear/netgear_wndr3800",
    routerPath: "netgear~netgear_wndr3800"
  },
  {
    brand: "Netgear",
    model: "WNDR3800",
    page: "/toh/netgear/wndr3800",
    techData: "/toh/hwdata/netgear/netgear_wndr3800_ch",
    routerPath: "netgear~netgear_wndr3800_ch"
  },
  {
    brand: "Netgear",
    model: "WNDR4000",
    page: "/toh/netgear/wndr4000",
    techData: "/toh/hwdata/netgear/netgear_wndr4000",
    routerPath: "netgear~netgear_wndr4000"
  },
  {
    brand: "Netgear",
    model: "WNDR4300",
    page: "/toh/netgear/wndr4300",
    techData: "/toh/hwdata/netgear/netgear_wndr4300_v1",
    routerPath: "netgear~netgear_wndr4300_v1"
  },
  {
    brand: "Netgear",
    model: "WNDR4700",
    page: "/toh/netgear/wndr4700",
    techData: "/toh/hwdata/netgear/netgear_wndr4700_v1",
    routerPath: "netgear~netgear_wndr4700_v1"
  },
  {
    brand: "Netgear",
    model: "WNR612",
    page: "/toh/netgear/wnr612v2",
    techData: "/toh/hwdata/netgear/netgear_wnr612v2",
    routerPath: "netgear~netgear_wnr612v2"
  },
  {
    brand: "Netgear",
    model: "WNR834B",
    page: "/toh/netgear/wnr834b",
    techData: "/toh/hwdata/netgear/netgear_wnr834b_v2",
    routerPath: "netgear~netgear_wnr834b_v2"
  },
  {
    brand: "Netgear",
    model: "WNR854T",
    page: "/toh/netgear/wnr854t",
    techData: "/toh/hwdata/netgear/netgear_wnr854t",
    routerPath: "netgear~netgear_wnr854t"
  },
  {
    brand: "Netgear",
    model: "WNR1000",
    page: "/toh/netgear/wnr1000_v2",
    techData: "/toh/hwdata/netgear/netgear_wnr1000_v2",
    routerPath: "netgear~netgear_wnr1000_v2"
  },
  {
    brand: "Netgear",
    model: "WNR2000",
    page: "/toh/netgear/wnr2000",
    techData: "/toh/hwdata/netgear/netgear_wnr2000_v1",
    routerPath: "netgear~netgear_wnr2000_v1"
  },
  {
    brand: "Netgear",
    model: "WNR2000",
    page: "/toh/netgear/wnr2000",
    techData: "/toh/hwdata/netgear/netgear_wnr2000_v2",
    routerPath: "netgear~netgear_wnr2000_v2"
  },
  {
    brand: "Netgear",
    model: "WNR2000",
    page: "/toh/netgear/wnr2000",
    techData: "/toh/hwdata/netgear/netgear_wnr2000_v3",
    routerPath: "netgear~netgear_wnr2000_v3"
  },
  {
    brand: "Netgear",
    model: "WNR2000",
    page: "/toh/netgear/wnr2000",
    techData: "/toh/hwdata/netgear/netgear_wnr2000_v4",
    routerPath: "netgear~netgear_wnr2000_v4"
  },
  {
    brand: "Netgear",
    model: "WNR3500L",
    page: "/toh/netgear/wnr3500l",
    techData: "/toh/hwdata/netgear/netgear_wnr3500l_v1",
    routerPath: "netgear~netgear_wnr3500l_v1"
  },
  {
    brand: "Netgear",
    model: "WNR3500L",
    page: "/toh/netgear/wnr3500l",
    techData: "/toh/hwdata/netgear/netgear_wnr3500l_v2",
    routerPath: "netgear~netgear_wnr3500l_v2"
  },
  {
    brand: "Netgear",
    model: "WPN824N",
    page: "/toh/netgear/wpn824n",
    techData: "/toh/hwdata/netgear/netgear_wpn824n",
    routerPath: "netgear~netgear_wpn824n"
  },
  {
    brand: "Netgear",
    model: "WPN824N NA",
    page: "/toh/netgear/wpn824n",
    techData: "/toh/hwdata/netgear/netgear_wpn824n_na",
    routerPath: "netgear~netgear_wpn824n_na"
  },
  {
    brand: "Netgear",
    model: "WNR3500",
    page: "/toh/netgear/wnr3500_v2",
    techData: "/toh/hwdata/netgear/netgear_wnr3500_v2",
    routerPath: "netgear~netgear_wnr3500_v2"
  },
  {
    brand: "Netgear",
    model: "R7500",
    page: "/toh/netgear/netgear_r7500",
    techData: "/toh/hwdata/netgear/netgear_r7500_v2",
    routerPath: "netgear~netgear_r7500_v2"
  },
  {
    brand: "Netgear",
    model: "WNR1000",
    page: "/toh/netgear/wnr1000_v3",
    techData: "/toh/hwdata/netgear/netgear_wnr1000_v3",
    routerPath: "netgear~netgear_wnr1000_v3"
  },
  {
    brand: "Netgear",
    model: "R7500",
    page: "/toh/netgear/netgear_r7500",
    techData: "/toh/hwdata/netgear/netgear_r7500_v1",
    routerPath: "netgear~netgear_r7500_v1"
  },
  {
    brand: "Netgear",
    model: "WNCE2001",
    page: "/toh/netgear/wnce2001",
    techData: "/toh/hwdata/netgear/netgear_wnce2001",
    routerPath: "netgear~netgear_wnce2001"
  },
  {
    brand: "Netgear",
    model: "R7800",
    page: "/toh/netgear/r7800",
    techData: "/toh/hwdata/netgear/netgear_r7800",
    routerPath: "netgear~netgear_r7800"
  },
  {
    brand: "Netgear",
    model: "WNDR3700",
    page: "/toh/netgear/wndr3700",
    techData: "/toh/hwdata/netgear/netgear_wndr3700_v5",
    routerPath: "netgear~netgear_wndr3700_v5"
  },
  {
    brand: "Netgear",
    model: "WN3000RP",
    page: "/toh/netgear/netgear_wn3000rp_v1",
    techData: "/toh/hwdata/netgear/netgear_wn3000rp_v1",
    routerPath: "netgear~netgear_wn3000rp_v1"
  },
  {
    brand: "Netgear",
    model: "R6220",
    page: "/toh/netgear/netgear_r6220",
    techData: "/toh/hwdata/netgear/netgear_r6220",
    routerPath: "netgear~netgear_r6220"
  },
  {
    brand: "Netgear",
    model: "EX3700",
    page: "/toh/netgear/ex3700_ex3800",
    techData: "/toh/hwdata/netgear/netgear_ex3700",
    routerPath: "netgear~netgear_ex3700"
  },
  {
    brand: "Netgear",
    model: "WNDR3300",
    page: "/toh/netgear/wndr3300",
    techData: "/toh/hwdata/netgear/netgear_wndr3300_v1",
    routerPath: "netgear~netgear_wndr3300_v1"
  },
  {
    brand: "Netgear",
    model: "DM200",
    page: "/toh/netgear/dm200",
    techData: "/toh/hwdata/netgear/netgear_dm200",
    routerPath: "netgear~netgear_dm200"
  },
  {
    brand: "Netgear",
    model: "EX3800",
    page: "/toh/netgear/ex3700_ex3800",
    techData: "/toh/hwdata/netgear/netgear_ex3800",
    routerPath: "netgear~netgear_ex3800"
  },
  {
    brand: "Netgear",
    model: "WNR1000",
    page: "/toh/netgear/wnr1000_v2",
    techData: "/toh/hwdata/netgear/netgear_wnr1000_v2-vc",
    routerPath: "netgear~netgear_wnr1000_v2-vc"
  },
  {
    brand: "Netgear",
    model: "WNDAP360",
    page: "/toh/netgear/wndap360",
    techData: "/toh/hwdata/netgear/netgear_wndap360",
    routerPath: "netgear~netgear_wndap360"
  },
  {
    brand: "Netgear",
    model: "R6300",
    page: "/toh/netgear/netgear_r6300_v1",
    techData: "/toh/hwdata/netgear/netgear_r6300_v1",
    routerPath: "netgear~netgear_r6300_v1"
  },
  {
    brand: "Netgear",
    model: "R6120",
    page: "/toh/netgear/netgear_r6120",
    techData: "/toh/hwdata/netgear/netgear_r6120",
    routerPath: "netgear~netgear_r6120"
  },
  {
    brand: "Netgear",
    model: "R6350",
    page: "/toh/netgear/netgear_r6350",
    techData: "/toh/hwdata/netgear/netgear_r6350",
    routerPath: "netgear~netgear_r6350"
  },
  {
    brand: "Netgear",
    model: "WNDRMAC",
    page: "/toh/netgear/wndr3700",
    techData: "/toh/hwdata/netgear/netgear_wndrmac_v1",
    routerPath: "netgear~netgear_wndrmac_v1"
  },
  {
    brand: "Netgear",
    model: "WNDRMAC",
    page: "/toh/netgear/wndr3800",
    techData: "/toh/hwdata/netgear/netgear_wndrmac_v2",
    routerPath: "netgear~netgear_wndrmac_v2"
  },
  {
    brand: "Netgear",
    model: "R6230",
    page: "/toh/netgear/netgear_r6220",
    techData: "/toh/hwdata/netgear/netgear_r6230",
    routerPath: "netgear~netgear_r6230"
  },
  {
    brand: "Netgear",
    model: "R6260",
    page: "/toh/netgear/netgear_r6260",
    techData: "/toh/hwdata/netgear/netgear_r6260",
    routerPath: "netgear~netgear_r6260"
  },
  {
    brand: "Netgear",
    model: "WNR2200",
    page: "/toh/netgear/wnr2200",
    techData: "/toh/hwdata/netgear/netgear_wnr2200_16mb_ru",
    routerPath: "netgear~netgear_wnr2200_16mb_ru"
  },
  {
    brand: "Netgear",
    model: "WNR2200",
    page: "/toh/netgear/wnr2200",
    techData: "/toh/hwdata/netgear/netgear_wnr2200_8mb_eu",
    routerPath: "netgear~netgear_wnr2200_8mb_eu"
  },
  {
    brand: "NETGEAR",
    model: "WN2500RP",
    page: "/toh/netgear/wn2500rp_v1",
    techData: "/toh/hwdata/netgear/netgear_wn2500rp_v1",
    routerPath: "netgear~netgear_wn2500rp_v1"
  },
  {
    brand: "NETGEAR",
    model: "R6800",
    page: "/toh/netgear/netgear_r6800",
    techData: "/toh/hwdata/netgear/netgear_r6800_1.0",
    routerPath: "netgear~netgear_r6800_1.0"
  },
  {
    brand: "Netis",
    model: "M1200AC",
    page: "/toh/netis/wf-2881",
    techData: "/toh/hwdata/netis/netis_m1200ac",
    routerPath: "netis~netis_m1200ac"
  },
  {
    brand: "Netis",
    model: "WF-2881",
    page: "/toh/netis/wf-2881",
    techData: "/toh/hwdata/netis/netis_wf-2881",
    routerPath: "netis~netis_wf-2881"
  },
  {
    brand: "Nexx",
    model: "WT3020A",
    page: "/toh/nexx/wt3020",
    techData: "/toh/hwdata/nexx/nexx_wt3020a",
    routerPath: "nexx~nexx_wt3020a"
  },
  {
    brand: "Nexx",
    model: "WT3020AD",
    page: "/toh/nexx/wt3020",
    techData: "/toh/hwdata/nexx/nexx_wt3020ad",
    routerPath: "nexx~nexx_wt3020ad"
  },
  {
    brand: "Nexx",
    model: "WT3020F",
    page: "/toh/nexx/wt3020",
    techData: "/toh/hwdata/nexx/nexx_wt3020f",
    routerPath: "nexx~nexx_wt3020f"
  },
  {
    brand: "Nexx",
    model: "WT3020H",
    page: "/toh/nexx/wt3020",
    techData: "/toh/hwdata/nexx/nexx_wt3020h",
    routerPath: "nexx~nexx_wt3020h"
  },
  {
    brand: "Nexx",
    model: "WT1520F",
    page: "/toh/nexx/wt1520",
    techData: "/toh/hwdata/nexx/nexx_wt1520f",
    routerPath: "nexx~nexx_wt1520f"
  },
  {
    brand: "Nexx",
    model: "WT1520H",
    page: "/toh/nexx/wt1520",
    techData: "/toh/hwdata/nexx/nexx_wt1520h",
    routerPath: "nexx~nexx_wt1520h"
  },
  {
    brand: "NuCom",
    model: "R5010UN",
    page: "/toh/nucom/r5010unv2",
    techData: "/toh/hwdata/nucom/nucom_r5010un",
    routerPath: "nucom~nucom_r5010un"
  },
  {
    brand: "Observa",
    model: "VH4032N",
    page: "/toh/observatelecom/vh4032n",
    techData: "/toh/hwdata/observa/observa_vh4032n",
    routerPath: "observa~observa_vh4032n"
  },
  {
    brand: "Olimex",
    model: "A20-OLinuXino-LIME",
    page: "/toh/olimex/a20-olinuxino-lime",
    techData: "/toh/hwdata/olimex/olimex_a20-olinuxino-lime",
    routerPath: "olimex~olimex_a20-olinuxino-lime"
  },
  {
    brand: "Olimex",
    model: "iMX233-OLinuXino-MAXI",
    page: "/toh/olimex/imx233-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_imx233-olinuxino-maxi",
    routerPath: "olimex~olimex_imx233-olinuxino-maxi"
  },
  {
    brand: "Olimex",
    model: "iMX233-OLinuXino-MICRO",
    page: "/toh/olimex/imx233-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_imx233-olinuxino-micro",
    routerPath: "olimex~olimex_imx233-olinuxino-micro"
  },
  {
    brand: "Olimex",
    model: "iMX233-OLinuXino-MINI-WiFi",
    page: "/toh/olimex/imx233-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_imx233-olinuxino-mini-wifi",
    routerPath: "olimex~olimex_imx233-olinuxino-mini-wifi"
  },
  {
    brand: "Olimex",
    model: "iMX233-OLinuXino-MINI",
    page: "/toh/olimex/imx233-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_imx233-olinuxino-mini",
    routerPath: "olimex~olimex_imx233-olinuxino-mini"
  },
  {
    brand: "Olimex",
    model: "iMX233-OLinuXino-NANO",
    page: "/toh/olimex/imx233-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_imx233-olinuxino-nano",
    routerPath: "olimex~olimex_imx233-olinuxino-nano"
  },
  {
    brand: "Olimex",
    model: "RT5350F-OLinuXino-EVB",
    page: "/toh/olimex/rt5350f-olinuxino-evb",
    techData: "/toh/hwdata/olimex/olimex_rt5350f-olinuxino-evb",
    routerPath: "olimex~olimex_rt5350f-olinuxino-evb"
  },
  {
    brand: "Olimex",
    model: "A20-OLinuXino-MICRO",
    page: "/toh/olimex/a20-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_a20-olinuxino-micro",
    routerPath: "olimex~olimex_a20-olinuxino-micro"
  },
  {
    brand: "Olimex",
    model: "A10-OLinuXino-LIME",
    page: "/toh/olimex/a10-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_a10-olinuxino-lime",
    routerPath: "olimex~olimex_a10-olinuxino-lime"
  },
  {
    brand: "Olimex",
    model: "A13-OLinuXino-WIFI",
    page: "/toh/olimex/a13-olinuxino",
    techData: "/toh/hwdata/olimex/olimex_a13-olinuxino-wifi",
    routerPath: "olimex~olimex_a13-olinuxino-wifi"
  },
  {
    brand: "Olimex",
    model: "A13-SOM-512",
    page: "/toh/olimex/a13-olinuxino-som",
    techData: "/toh/hwdata/olimex/olimex_a13-som-512",
    routerPath: "olimex~olimex_a13-som-512"
  },
  {
    brand: "Olimex",
    model: "A20-OLinuXino-LIME2",
    page: "/toh/olimex/a20-olinuxino-lime2",
    techData: "/toh/hwdata/olimex/olimex_a20-olinuxino-lime2",
    routerPath: "olimex~olimex_a20-olinuxino-lime2"
  },
  {
    brand: "OMYlink",
    model: "OMY-G1",
    page: "/toh/omylink/omy-g1",
    techData: "/toh/hwdata/omylink/omylink_omy-g1",
    routerPath: "omylink~omylink_omy-g1"
  },
  {
    brand: "OMYlink",
    model: "OMY-X1",
    page: "/toh/omylink/omy-x1",
    techData: "/toh/hwdata/omylink/omylink_omy-x1",
    routerPath: "omylink~omylink_omy-x1"
  },
  {
    brand: "On Networks",
    model: "N150R",
    page: "/toh/netgear/wnr612v2",
    techData: "/toh/hwdata/onnetworks/onnetworks_n150r",
    routerPath: "onnetworks~onnetworks_n150r"
  },
  {
    brand: "Open-Mesh",
    model: "OM2P",
    page: "/toh/openmesh/om2p",
    techData: "/toh/hwdata/open-mesh/open-mesh_om2p",
    routerPath: "open-mesh~open-mesh_om2p"
  },
  {
    brand: "OpenEmbed",
    model: "SOM9331",
    page: "/toh/openembed/openembed_som9331_v1",
    techData: "/toh/hwdata/openembed/openembed_som9331_v1",
    routerPath: "openembed~openembed_som9331_v1"
  },
  {
    brand: "Pacific Networks",
    model: "RT150M",
    page: "/toh/pacificnetworks/pn-rt150m",
    techData: "/toh/hwdata/pacificnetworks/pacificnetworks_rt150m_v1",
    routerPath: "pacificnetworks~pacificnetworks_rt150m_v1"
  },
  {
    brand: "PC Engines",
    model: "ALIX 1c",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix1c",
    routerPath: "pcengines~pcengines_alix1c"
  },
  {
    brand: "PC Engines",
    model: "ALIX 1d",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix1d",
    routerPath: "pcengines~pcengines_alix1d"
  },
  {
    brand: "PC Engines",
    model: "ALIX 2d0",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix2d0",
    routerPath: "pcengines~pcengines_alix2d0"
  },
  {
    brand: "PC Engines",
    model: "ALIX 2d1",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix2d1",
    routerPath: "pcengines~pcengines_alix2d1"
  },
  {
    brand: "PC Engines",
    model: "ALIX 2d2",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix2d2",
    routerPath: "pcengines~pcengines_alix2d2"
  },
  {
    brand: "PC Engines",
    model: "ALIX 2d3",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix2d3",
    routerPath: "pcengines~pcengines_alix2d3"
  },
  {
    brand: "PC Engines",
    model: "ALIX 2d4",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix2d4",
    routerPath: "pcengines~pcengines_alix2d4"
  },
  {
    brand: "PC Engines",
    model: "ALIX 2d5",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix2d5",
    routerPath: "pcengines~pcengines_alix2d5"
  },
  {
    brand: "PC Engines",
    model: "ALIX 2d13",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix2d13",
    routerPath: "pcengines~pcengines_alix2d13"
  },
  {
    brand: "PC Engines",
    model: "ALIX 3d1",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix3d1",
    routerPath: "pcengines~pcengines_alix3d1"
  },
  {
    brand: "PC Engines",
    model: "ALIX 3d2",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix3d2",
    routerPath: "pcengines~pcengines_alix3d2"
  },
  {
    brand: "PC Engines",
    model: "ALIX 3d3",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix3d3",
    routerPath: "pcengines~pcengines_alix3d3"
  },
  {
    brand: "PC Engines",
    model: "ALIX 6f2",
    page: "/toh/pcengines/alix",
    techData: "/toh/hwdata/pcengines/pcengines_alix6f2",
    routerPath: "pcengines~pcengines_alix6f2"
  },
  {
    brand: "PC Engines",
    model: "APU1C",
    page: "/toh/pcengines/apu",
    techData: "/toh/hwdata/pcengines/pcengines_apu1c",
    routerPath: "pcengines~pcengines_apu1c"
  },
  {
    brand: "PC Engines",
    model: "APU1C4",
    page: "/toh/pcengines/apu",
    techData: "/toh/hwdata/pcengines/pcengines_apu1c4",
    routerPath: "pcengines~pcengines_apu1c4"
  },
  {
    brand: "PC Engines",
    model: "WRAP",
    page: "/toh/pcengines/wrap",
    techData: "/toh/hwdata/pcengines/pcengines_wrap",
    routerPath: "pcengines~pcengines_wrap"
  },
  {
    brand: "PC Engines",
    model: "APU3C4",
    page: "/toh/pcengines/apu3",
    techData: "/toh/hwdata/pcengines/pcengines_apu3c4",
    routerPath: "pcengines~pcengines_apu3c4"
  },
  {
    brand: "PC Engines",
    model: "APU3C2",
    page: "/toh/pcengines/apu3",
    techData: "/toh/hwdata/pcengines/pcengines_apu3c2",
    routerPath: "pcengines~pcengines_apu3c2"
  },
  {
    brand: "PC Engines",
    model: "APU3A2",
    page: "/toh/pcengines/apu3",
    techData: "/toh/hwdata/pcengines/pcengines_apu3a2",
    routerPath: "pcengines~pcengines_apu3a2"
  },
  {
    brand: "PC Engines",
    model: "APU2C4",
    page: "/toh/pcengines/apu2",
    techData: "/toh/hwdata/pcengines/pcengines_apu2c4",
    routerPath: "pcengines~pcengines_apu2c4"
  },
  {
    brand: "PC Engines",
    model: "APU2C2",
    page: "/toh/pcengines/apu2",
    techData: "/toh/hwdata/pcengines/pcengines_apu2c2",
    routerPath: "pcengines~pcengines_apu2c2"
  },
  {
    brand: "PC Engines",
    model: "APU2C0",
    page: "/toh/pcengines/apu2",
    techData: "/toh/hwdata/pcengines/pcengines_apu2c0",
    routerPath: "pcengines~pcengines_apu2c0"
  },
  {
    brand: "PC Engines",
    model: "APU1D",
    page: "/toh/pcengines/apu",
    techData: "/toh/hwdata/pcengines/pcengines_apu1d",
    routerPath: "pcengines~pcengines_apu1d"
  },
  {
    brand: "Petatel",
    model: "PSR-680W",
    page: "/toh/petatel/psr-680w",
    techData: "/toh/hwdata/petatel/petatel_psr-680w",
    routerPath: "petatel~petatel_psr-680w"
  },
  {
    brand: "PHICOMM",
    model: "PSG1208",
    page: "/toh/phicomm/psg1208",
    techData: "/toh/hwdata/phicomm/phicomm_psg1208",
    routerPath: "phicomm~phicomm_psg1208"
  },
  {
    brand: "PHICOMM",
    model: "K2P",
    page: "/toh/phicomm/phicomm_k2p_ke2p",
    techData: "/toh/hwdata/phicomm/phicomm_k2p",
    routerPath: "phicomm~phicomm_k2p"
  },
  {
    brand: "PHICOMM",
    model: "K3",
    page: "/toh/phicomm/phicomm_k3_a1",
    techData: "/toh/hwdata/phicomm/phicomm_k3_a1",
    routerPath: "phicomm~phicomm_k3_a1"
  },
  {
    brand: "PHICOMM",
    model: "KE2P",
    page: "/toh/phicomm/phicomm_k2p_ke2p",
    techData: "/toh/hwdata/phicomm/phicomm_ke2p",
    routerPath: "phicomm~phicomm_ke2p"
  },
  {
    brand: "Pirelli",
    model: "AliceGate AGPF",
    page: "/toh/pirelli/agpf",
    techData: "/toh/hwdata/pirelli/pirelli_alicegateagpf",
    routerPath: "pirelli~pirelli_alicegateagpf"
  },
  {
    brand: "Pirelli",
    model: "FastWeb DRG A226M",
    page: "/toh/pirelli/drg_a226m",
    techData: "/toh/hwdata/pirelli/pirelli_fastweb_drga226m",
    routerPath: "pirelli~pirelli_fastweb_drga226m"
  },
  {
    brand: "Planex",
    model: "MZK-W04NU",
    page: "/toh/planex/mzk-w04nu",
    techData: "/toh/hwdata/planex/planex_mzk-w04nu",
    routerPath: "planex~planex_mzk-w04nu"
  },
  {
    brand: "Planex",
    model: "MZK-W300NH",
    page: "/toh/planex/mzk-w300nh",
    techData: "/toh/hwdata/planex/planex_mzk-w300nh",
    routerPath: "planex~planex_mzk-w300nh"
  },
  {
    brand: "Pogoplug",
    model: "Pogoplug V2",
    page: "/toh/cloudengines/pogoplug",
    techData: "/toh/hwdata/pogoplug/pogoplug_pogoplug_e02",
    routerPath: "pogoplug~pogoplug_pogoplug_e02"
  },
  {
    brand: "Pogoplug",
    model: "Pogoplug V4",
    page: "/toh/cloudengines/pogo-v4",
    techData: "/toh/hwdata/pogoplug/pogoplug_pogoplug_v4",
    routerPath: "pogoplug~pogoplug_pogoplug_v4"
  },
  {
    brand: "Pogoplug",
    model: "Pogoplug Mobile",
    page: "/toh/cloudengines/pogo-v4",
    techData: "/toh/hwdata/pogoplug/pogoplug_pogoplugmobilev4",
    routerPath: "pogoplug~pogoplug_pogoplugmobilev4"
  },
  {
    brand: "Pogoplug",
    model: "Pogoplug V3",
    page: "/toh/cloudengines/pogoplugpro",
    techData: "/toh/hwdata/pogoplug/pogoplug_pogoplug_v3",
    routerPath: "pogoplug~pogoplug_pogoplug_v3"
  },
  {
    brand: "Pogoplug",
    model: "Pogoplug V3 Pro",
    page: "/toh/cloudengines/pogoplugpro",
    techData: "/toh/hwdata/pogoplug/pogoplug_pogoplug_v3_pro",
    routerPath: "pogoplug~pogoplug_pogoplug_v3_pro"
  },
  {
    brand: "Poray",
    model: "IP2202",
    page: "/toh/poray/ip2202",
    techData: "/toh/hwdata/poray/poray_porayip2202",
    routerPath: "poray~poray_porayip2202"
  },
  {
    brand: "Poray",
    model: "Poray M3",
    page: "/toh/poray/m3",
    techData: "/toh/hwdata/poray/poray_poraym3",
    routerPath: "poray~poray_poraym3"
  },
  {
    brand: "Poray",
    model: "Poray M4 4MiB/8MiB",
    page: "/toh/poray/m4",
    techData: "/toh/hwdata/poray/poray_poraym4",
    routerPath: "poray~poray_poraym4"
  },
  {
    brand: "Poray",
    model: "Poray X8",
    page: "/toh/poray/x8",
    techData: "/toh/hwdata/poray/poray_porayx8",
    routerPath: "poray~poray_porayx8"
  },
  {
    brand: "Poray",
    model: "PRP-X5/X6",
    page: "/toh/poray/prp-x5",
    techData: "/toh/hwdata/poray/poray_prp-x5-x6",
    routerPath: "poray~poray_prp-x5-x6"
  },
  {
    brand: "PowerCloud Systems",
    model: "CR5000",
    page: "/toh/powercloud_systems/powercloud_systems_cr5000",
    techData: "/toh/hwdata/powercloud_systems/powercloud_systems_cr5000",
    routerPath: "powercloud_systems~powercloud_systems_cr5000"
  },
  {
    brand: "PowerCloud Systems",
    model: "CR3000",
    page: "/toh/powercloud_systems/powercloud_systems_cr3000",
    techData: "/toh/hwdata/powercloud_systems/powercloud_systems_cr3000",
    routerPath: "powercloud_systems~powercloud_systems_cr3000"
  },
  {
    brand: "PowerCloud Systems",
    model: "CAP324",
    page: "/toh/powercloud_systems/powercloud_systems_cap324",
    techData: "/toh/hwdata/powercloud_systems/powercloud_systems_cap324",
    routerPath: "powercloud_systems~powercloud_systems_cap324"
  },
  {
    brand: "Prolink",
    model: "PWH2004",
    page: "/toh/prolink/pwh2004",
    techData: "/toh/hwdata/prolink/prolink_pwh2004",
    routerPath: "prolink~prolink_pwh2004"
  },
  {
    brand: "QEMU (armvirt)",
    model: "qemu-system-arm",
    page: "/toh/qemu/qemu",
    techData: "/toh/hwdata/qemu/qemu_arm",
    routerPath: "qemu~qemu_arm"
  },
  {
    brand: "QEMU (i386)",
    model: "qemu-system-i386",
    page: "/toh/qemu/qemu",
    techData: "/toh/hwdata/qemu/qemu_i386",
    routerPath: "qemu~qemu_i386"
  },
  {
    brand: "QEMU (malta)",
    model: "qemu-system-mips(el)",
    page: "/toh/qemu/qemu",
    techData: "/toh/hwdata/qemu/qemu_mips-el",
    routerPath: "qemu~qemu_mips-el"
  },
  {
    brand: "QEMU (x86_64)",
    model: "qemu-system-x86_64",
    page: "/toh/qemu/qemu",
    techData: "/toh/hwdata/qemu/qemu_x86_64",
    routerPath: "qemu~qemu_x86_64"
  },
  {
    brand: "Qihoo hardware",
    model: "C301",
    page: "/toh/qihoo/c301",
    techData: "/toh/hwdata/qihoohardware/qihoohardware_c301",
    routerPath: "qihoohardware~qihoohardware_c301"
  },
  {
    brand: "Qxwlan",
    model: "E1700AC",
    page: "/toh/qxwlan/e1700ac",
    techData: "/toh/hwdata/qxwlan/qxwlan_e1700ac_v2",
    routerPath: "qxwlan~qxwlan_e1700ac_v2"
  },
  {
    brand: "Qxwlan",
    model: "E600GAC",
    page: "/toh/qxwlan/e600gac",
    techData: "/toh/hwdata/qxwlan/qxwlan_e600gac_v2",
    routerPath: "qxwlan~qxwlan_e600gac_v2"
  },
  {
    brand: "Qxwlan",
    model: "E600G",
    page: "/toh/qxwlan/e600g",
    techData: "/toh/hwdata/qxwlan/qxwlan_e600g_v2",
    routerPath: "qxwlan~qxwlan_e600g_v2"
  },
  {
    brand: "Qxwlan",
    model: "E750A",
    page: "/toh/qxwlan/e750a",
    techData: "/toh/hwdata/qxwlan/qxwlan_e750a_v4",
    routerPath: "qxwlan~qxwlan_e750a_v4"
  },
  {
    brand: "Qxwlan",
    model: "E750G",
    page: "/toh/qxwlan/e750g",
    techData: "/toh/hwdata/qxwlan/qxwlan_e750g_v8",
    routerPath: "qxwlan~qxwlan_e750g_v8"
  },
  {
    brand: "Qxwlan",
    model: "E558",
    page: "/toh/qxwlan/e558",
    techData: "/toh/hwdata/qxwlan/qxwlan_e558_v2",
    routerPath: "qxwlan~qxwlan_e558_v2"
  },
  {
    brand: "Qxwlan",
    model: "E2600AC",
    page: "/toh/qxwlan/e2600ac",
    techData: "/toh/hwdata/qxwlan/qxwlan_e2600ac_c2",
    routerPath: "qxwlan~qxwlan_e2600ac_c2"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi 2",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_2_b",
    routerPath: "raspberry_pi_foundation~raspberry_pi_2_b"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi 3",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_3_b",
    routerPath: "raspberry_pi_foundation~raspberry_pi_3_b"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi 3",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_3_bplus",
    routerPath: "raspberry_pi_foundation~raspberry_pi_3_bplus"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_a",
    routerPath: "raspberry_pi_foundation~raspberry_pi_a"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_b",
    routerPath: "raspberry_pi_foundation~raspberry_pi_b"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_bplus",
    routerPath: "raspberry_pi_foundation~raspberry_pi_bplus"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi Zero W",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_zero_w",
    routerPath: "raspberry_pi_foundation~raspberry_pi_zero_w"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi 4",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData:
      "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_foundation_raspberry_pi_4_b",
    routerPath:
      "raspberry_pi_foundation~raspberry_pi_foundation_raspberry_pi_4_b"
  },
  {
    brand: "Raspberry Pi Foundation",
    model: "Raspberry Pi 2",
    page: "/toh/raspberry_pi_foundation/raspberry_pi",
    techData: "/toh/hwdata/raspberry_pi_foundation/raspberry_pi_2_b_bcm2837",
    routerPath: "raspberry_pi_foundation~raspberry_pi_2_b_bcm2837"
  },
  {
    brand: "RavPower",
    model: "RP-WD02",
    page: "/toh/ravpower/rp-wd02",
    techData: "/toh/hwdata/ravpower/ravpower_rp-wd02",
    routerPath: "ravpower~ravpower_rp-wd02"
  },
  {
    brand: "Redwave",
    model: "RW2458N / AeroMAX 5",
    page: "/toh/redwave/rw2458n",
    techData: "/toh/hwdata/redwave/redwave_rw2458naeromax5",
    routerPath: "redwave~redwave_rw2458naeromax5"
  },
  {
    brand: "Rosewill",
    model: "RNX-N150RT",
    page: "/toh/rosewill/rnx-n150rt",
    techData: "/toh/hwdata/rosewill/rosewill_rnx-n150rt",
    routerPath: "rosewill~rosewill_rnx-n150rt"
  },
  {
    brand: "Rosewill",
    model: "RNX-N300RT",
    page: "/toh/rosewill/rnx-n300rt",
    techData: "/toh/hwdata/rosewill/rosewill_rnx-n300rt",
    routerPath: "rosewill~rosewill_rnx-n300rt"
  },
  {
    brand: "Rosewill",
    model: "RNX-N360RT",
    page: "/toh/rosewill/rnx-n360rt",
    techData: "/toh/hwdata/rosewill/rosewill_rnx-n360rt",
    routerPath: "rosewill~rosewill_rnx-n360rt"
  },
  {
    brand: "Sagem",
    model: "F@ST2404",
    page: "/toh/sagem/fast2404",
    techData: "/toh/hwdata/sagem/sagem_fast2404",
    routerPath: "sagem~sagem_fast2404"
  },
  {
    brand: "Sagem",
    model: "F@ST2504n",
    page: "/toh/sagem/sagem_fast2504n_v.6",
    techData: "/toh/hwdata/sagem/sagem_fast2504n_v.6",
    routerPath: "sagem~sagem_fast2504n_v.6"
  },
  {
    brand: "Sagem",
    model: "F@ST2604",
    page: "/toh/sagem/fast2604",
    techData: "/toh/hwdata/sagem/sagem_fast2604",
    routerPath: "sagem~sagem_fast2604"
  },
  {
    brand: "Sagem",
    model: "F@ST2704N",
    page: "/toh/plusnet/fast2704nv1",
    techData: "/toh/hwdata/sagem/sagem_fast2704n_v1",
    routerPath: "sagem~sagem_fast2704n_v1"
  },
  {
    brand: "Sagem",
    model: "F@ST2704",
    page: "/toh/sagem/fast2704",
    techData: "/toh/hwdata/sagem/sagem_fast2704_v2",
    routerPath: "sagem~sagem_fast2704_v2"
  },
  {
    brand: "SamKnows",
    model: "SK-WB8",
    page: "/toh/samknows/sk-wb8",
    techData: "/toh/hwdata/samknows/samknows_sk-wb8",
    routerPath: "samknows~samknows_sk-wb8"
  },
  {
    brand: "Seagate",
    model: "DockStar",
    page: "/toh/seagate/dockstar",
    techData: "/toh/hwdata/seagate/seagate_dockstar",
    routerPath: "seagate~seagate_dockstar"
  },
  {
    brand: "Seagate",
    model: "GoFlexHome",
    page: "/toh/seagate/goflexhome",
    techData: "/toh/hwdata/seagate/seagate_goflexhome",
    routerPath: "seagate~seagate_goflexhome"
  },
  {
    brand: "Seagate",
    model: "GoFlexNet",
    page: "/toh/seagate/goflexnet",
    techData: "/toh/hwdata/seagate/seagate_goflexnet",
    routerPath: "seagate~seagate_goflexnet"
  },
  {
    brand: "Sercomm",
    model: "IP1006RRv2",
    page: "/toh/sitecom/wl-341",
    techData: "/toh/hwdata/sercomm/sercomm_ip1006rrv2",
    routerPath: "sercomm~sercomm_ip1006rrv2"
  },
  {
    brand: "Sercomm",
    model: "AD1018",
    page: "/toh/sercomm/ad1018",
    techData: "/toh/hwdata/sercomm/sercomm_ad1018",
    routerPath: "sercomm~sercomm_ad1018"
  },
  {
    brand: "SFR (Société Française de Radiotéléphonie)",
    model: "Neufbox4 (NB4)",
    page: "/toh/sfr/nb4",
    techData: "/toh/hwdata/sfr/sfr_neufbox4",
    routerPath: "sfr~sfr_neufbox4"
  },
  {
    brand: "SimpleTech",
    model: "SimpleShare",
    page: "/toh/simpletech/simpleshare",
    techData: "/toh/hwdata/simpletech/simpletech_simpleshare",
    routerPath: "simpletech~simpletech_simpleshare"
  },
  {
    brand: "Sinovoip",
    model: "Banana Pi R2",
    page: "/toh/sinovoip/sinovoip_banana_pi_r2",
    techData: "/toh/hwdata/sinovoip/sinovoip_banana_pi_r2",
    routerPath: "sinovoip~sinovoip_banana_pi_r2"
  },
  {
    brand: "Sinovoip",
    model: "Banana Pi M2 Plus",
    page: "/toh/sinovoip/sinovoip_banana_pi_m2_plus",
    techData: "/toh/hwdata/sinovoip/sinovoip_banana_pi_m2_plus_m2plus-h3",
    routerPath: "sinovoip~sinovoip_banana_pi_m2_plus_m2plus-h3"
  },
  {
    brand: "Sitecom",
    model: "WL-341",
    page: "/toh/sitecom/wl-341",
    techData: "/toh/hwdata/sitecom/sitecom_wl-341",
    routerPath: "sitecom~sitecom_wl-341"
  },
  {
    brand: "Sitecom",
    model: "WL-351",
    page: "/toh/sitecom/wl-351",
    techData: "/toh/hwdata/sitecom/sitecom_wl-351",
    routerPath: "sitecom~sitecom_wl-351"
  },
  {
    brand: "Sitecom",
    model: "WLR-7100",
    page: "/toh/sitecom/wlr-7100",
    techData: "/toh/hwdata/sitecom/sitecom_wlr-7100",
    routerPath: "sitecom~sitecom_wlr-7100"
  },
  {
    brand: "Sitecom",
    model: "WLR-8100",
    page: "/toh/sitecom/wlr-8100",
    techData: "/toh/hwdata/sitecom/sitecom_wlr-8100",
    routerPath: "sitecom~sitecom_wlr-8100"
  },
  {
    brand: "Sitecom",
    model: "WLR-6000",
    page: "/toh/sitecom/wlr-6000",
    techData: "/toh/hwdata/sitecom/sitecom_wlr-6000",
    routerPath: "sitecom~sitecom_wlr-6000"
  },
  {
    brand: "Sky",
    model: "SR102",
    page: "/toh/sky/sr102",
    techData: "/toh/hwdata/sky/sky_sr102",
    routerPath: "sky~sky_sr102"
  },
  {
    brand: "Skyline",
    model: "SL-R7205",
    page: "/toh/widemac/sl-r7205",
    techData: "/toh/hwdata/skyline/skyline_sl-r7205",
    routerPath: "skyline~skyline_sl-r7205"
  },
  {
    brand: "SMC",
    model: "SMC7908ISP-A",
    page: "/toh/arcadyan/arv4518pw",
    techData: "/toh/hwdata/smc/smc_smc7908isp-a",
    routerPath: "smc~smc_smc7908isp-a"
  },
  {
    brand: "SolidRun",
    model: "ClearFog Pro",
    page: "/toh/solidrun/clearfog",
    techData: "/toh/hwdata/solidrun/solidrun_clearfog_pro",
    routerPath: "solidrun~solidrun_clearfog_pro"
  },
  {
    brand: "SolidRun",
    model: "ClearFog Base",
    page: "/toh/solidrun/clearfog",
    techData: "/toh/hwdata/solidrun/solidrun_clearfog_base",
    routerPath: "solidrun~solidrun_clearfog_base"
  },
  {
    brand: "Sparklan",
    model: "WCR-150GN",
    page: "/toh/sparklan/wcr-150gn",
    techData: "/toh/hwdata/sparklan/sparklan_wcr-150gn",
    routerPath: "sparklan~sparklan_wcr-150gn"
  },
  {
    brand: "STORYLiNK",
    model: "SAP-G3200U3",
    page: "/toh/storylink/sap-g3200u3",
    techData: "/toh/hwdata/storylink/storylink_sap-g3200u3",
    routerPath: "storylink~storylink_sap-g3200u3"
  },
  {
    brand: "T-Com / Telekom",
    model: "Speedport W 303V Typ B",
    page: "/toh/t-com/spw303v-b",
    techData: "/toh/hwdata/t-comtelekom/t-comtelekom_speedportw303vtypb",
    routerPath: "t-comtelekom~t-comtelekom_speedportw303vtypb"
  },
  {
    brand: "T-Com / Telekom",
    model: "Speedport W 500V",
    page: "/toh/t-com/spw500v",
    techData: "/toh/hwdata/t-comtelekom/t-comtelekom_speedportw500v",
    routerPath: "t-comtelekom~t-comtelekom_speedportw500v"
  },
  {
    brand: "T-Com / Telekom",
    model: "Speedport W 504V",
    page: "/toh/t-com/spw504v",
    techData: "/toh/hwdata/t-comtelekom/t-comtelekom_speedportw504v",
    routerPath: "t-comtelekom~t-comtelekom_speedportw504v"
  },
  {
    brand: "T-Com / Telekom",
    model: "Speedport W 701V",
    page: "/toh/t-com/spw701v",
    techData: "/toh/hwdata/t-comtelekom/t-comtelekom_speedportw701v",
    routerPath: "t-comtelekom~t-comtelekom_speedportw701v"
  },
  {
    brand: "Technicolor",
    model: "TG582n",
    page: "/toh/thomson/tg582n",
    techData: "/toh/hwdata/technicolor/technicolor_tg582n",
    routerPath: "technicolor~technicolor_tg582n"
  },
  {
    brand: "Technicolor",
    model: "TG582n",
    page: "/toh/thomson/tg582n",
    techData: "/toh/hwdata/technicolor/technicolor_tg582n_dant-t",
    routerPath: "technicolor~technicolor_tg582n_dant-t"
  },
  {
    brand: "Tecom",
    model: "GW6000",
    page: "/toh/tecom/gw6000",
    techData: "/toh/hwdata/tecom/tecom_gw6000",
    routerPath: "tecom~tecom_gw6000"
  },
  {
    brand: "Telsey",
    model: "CPA-ZNTE60T",
    page: "/toh/telsey/cpa-znte60t",
    techData: "/toh/hwdata/telsey/telsey_cpa-znte60t",
    routerPath: "telsey~telsey_cpa-znte60t"
  },
  {
    brand: "Telsey",
    model: "CPVA502+",
    page: "/toh/telsey/cpva502.w",
    techData: "/toh/hwdata/telsey/telsey_cpva502",
    routerPath: "telsey~telsey_cpva502"
  },
  {
    brand: "Telsey",
    model: "CPVA502+W",
    page: "/toh/telsey/cpva502.w",
    techData: "/toh/hwdata/telsey/telsey_cpva502w",
    routerPath: "telsey~telsey_cpva502w"
  },
  {
    brand: "Teltonika",
    model: "RUT5XX",
    page: "/toh/teltonika/rut5xx",
    techData: "/toh/hwdata/teltonika/teltonika_rut5xx",
    routerPath: "teltonika~teltonika_rut5xx"
  },
  {
    brand: "Teltonika",
    model: "RUT500",
    page: "/toh/teltonika/rut5xx",
    techData: "/toh/hwdata/teltonika/teltonika_rut500",
    routerPath: "teltonika~teltonika_rut500"
  },
  {
    brand: "Tenda",
    model: "W306R",
    page: "/toh/tenda/w306r",
    techData: "/toh/hwdata/tenda/tenda_w306r_v20",
    routerPath: "tenda~tenda_w306r_v20"
  },
  {
    brand: "Texas Instruments",
    model: "BeagleBoard",
    page: "/toh/texas.instruments/beagleboard",
    techData: "/toh/hwdata/texasinstruments/texasinstruments_beagleboard",
    routerPath: "texasinstruments~texasinstruments_beagleboard"
  },
  {
    brand: "TP-Link",
    model: "Archer C50",
    page: "/toh/tp-link/archer-c50",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c50_v1",
    routerPath: "tp-link~tp-link_archer_c50_v1"
  },
  {
    brand: "TP-Link",
    model: "Archer C2600",
    page: "/toh/tp-link/tp-link_archer_c2600_v1",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c2600",
    routerPath: "tp-link~tp-link_archer_c2600"
  },
  {
    brand: "TP-Link",
    model: "RE450",
    page: "/toh/tp-link/tp-link_re450",
    techData: "/toh/hwdata/tp-link/tp-link_re450",
    routerPath: "tp-link~tp-link_re450"
  },
  {
    brand: "TP-Link",
    model: "TD-W8960N",
    page: "/toh/tp-link/td-w8960n",
    techData: "/toh/hwdata/tp-link/tp-link_td-w8960n",
    routerPath: "tp-link~tp-link_td-w8960n"
  },
  {
    brand: "TP-Link",
    model: "TD-W8968",
    page: "/toh/tp-link/tp-link_td-w8960n_v5",
    techData: "/toh/hwdata/tp-link/tp-link_td-w8968_v3",
    routerPath: "tp-link~tp-link_td-w8968_v3"
  },
  {
    brand: "TP-Link",
    model: "TD-W8980",
    page: "/toh/tp-link/td-w8980",
    techData: "/toh/hwdata/tp-link/tp-link_td-w8980",
    routerPath: "tp-link~tp-link_td-w8980"
  },
  {
    brand: "TP-Link",
    model: "TD-W9980",
    page: "/toh/tp-link/td-w9980",
    techData: "/toh/hwdata/tp-link/tp-link_td-w9980",
    routerPath: "tp-link~tp-link_td-w9980"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3040",
    page: "/toh/tp-link/tl-mr3040",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3040_v2",
    routerPath: "tp-link~tp-link_tl-mr3040_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WA730RE",
    page: "/toh/tp-link/tl-wa730re",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa730re_v1",
    routerPath: "tp-link~tp-link_tl-wa730re_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA801ND",
    page: "/toh/tp-link/tl-wa801nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa801nd_v2",
    routerPath: "tp-link~tp-link_tl-wa801nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WA801ND",
    page: "/toh/tp-link/tl-wa801nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa801nd_v3",
    routerPath: "tp-link~tp-link_tl-wa801nd_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WA830RE",
    page: "/toh/tp-link/tl-wa830re",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa830re_v1",
    routerPath: "tp-link~tp-link_tl-wa830re_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA830RE",
    page: "/toh/tp-link/tl-wa830re",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa830re_v2",
    routerPath: "tp-link~tp-link_tl-wa830re_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WA850RE",
    page: "/toh/tp-link/tl-wa850re",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa850re_v1",
    routerPath: "tp-link~tp-link_tl-wa850re_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA901ND",
    page: "/toh/tp-link/tl-wa901nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa901nd_v4",
    routerPath: "tp-link~tp-link_tl-wa901nd_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-WA5210G",
    page: "/toh/tp-link/tl-wa5210g",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa5210g_1",
    routerPath: "tp-link~tp-link_tl-wa5210g_1"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR4300",
    page: "/toh/tp-link/tl-wdr4300",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr4300",
    routerPath: "tp-link~tp-link_tl-wdr4300"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR6500",
    page: "/toh/tp-link/tp-link_tl-wdr6500_v2",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr6500_v2",
    routerPath: "tp-link~tp-link_tl-wdr6500_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR710N",
    page: "/toh/tp-link/tl-wr710n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr710n_v1.2_eu",
    routerPath: "tp-link~tp-link_tl-wr710n_v1.2_eu"
  },
  {
    brand: "TP-Link",
    model: "TL-WR710N",
    page: "/toh/tp-link/tl-wr710n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr710n_v2.0",
    routerPath: "tp-link~tp-link_tl-wr710n_v2.0"
  },
  {
    brand: "TP-Link",
    model: "TL-WR710N",
    page: "/toh/tp-link/tl-wr710n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr710n_v2.1",
    routerPath: "tp-link~tp-link_tl-wr710n_v2.1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR720N",
    page: "/toh/tp-link/tl-wr720n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr720n_v3",
    routerPath: "tp-link~tp-link_tl-wr720n_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WR720N",
    page: "/toh/tp-link/tl-wr720n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr720n_v4",
    routerPath: "tp-link~tp-link_tl-wr720n_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-WR740N",
    page: "/toh/tp-link/tl-wr740n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr740n_v4.20",
    routerPath: "tp-link~tp-link_tl-wr740n_v4.20"
  },
  {
    brand: "TP-Link",
    model: "TL-WR740N",
    page: "/toh/tp-link/tl-wr740n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr740n_v5_eu",
    routerPath: "tp-link~tp-link_tl-wr740n_v5_eu"
  },
  {
    brand: "TP-Link",
    model: "TL-WR740N",
    page: "/toh/tp-link/tl-wr740n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr740n_v6",
    routerPath: "tp-link~tp-link_tl-wr740n_v6"
  },
  {
    brand: "TP-Link",
    model: "TL-WR741ND",
    page: "/toh/tp-link/tl-wr741nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr741nd_1",
    routerPath: "tp-link~tp-link_tl-wr741nd_1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR741ND",
    page: "/toh/tp-link/tl-wr741nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr741nd_2",
    routerPath: "tp-link~tp-link_tl-wr741nd_2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR741ND",
    page: "/toh/tp-link/tl-wr741nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr741nd_v5",
    routerPath: "tp-link~tp-link_tl-wr741nd_v5"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v10",
    routerPath: "tp-link~tp-link_tl-wr841n_v10"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v11",
    routerPath: "tp-link~tp-link_tl-wr841n_v11"
  },
  {
    brand: "TP-Link",
    model: "TL-WR941ND",
    page: "/toh/tp-link/tl-wr941nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr941nd_v5",
    routerPath: "tp-link~tp-link_tl-wr941nd_v5"
  },
  {
    brand: "TP-Link",
    model: "TL-WR941ND",
    page: "/toh/tp-link/tl-wr941nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr941nd_v6",
    routerPath: "tp-link~tp-link_tl-wr941nd_v6"
  },
  {
    brand: "TP-Link",
    model: "TL-WR1041N",
    page: "/toh/tp-link/tl-wr1041n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr1041n_v2",
    routerPath: "tp-link~tp-link_tl-wr1041n_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR1041ND",
    page: "/toh/tp-link/tl-wr1041nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr1041nd_v2",
    routerPath: "tp-link~tp-link_tl-wr1041nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR1043ND",
    page: "/toh/tp-link/tl-wr1043nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr1043nd_v2",
    routerPath: "tp-link~tp-link_tl-wr1043nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR1043ND",
    page: "/toh/tp-link/tl-wr1043nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr1043nd_v3",
    routerPath: "tp-link~tp-link_tl-wr1043nd_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WR1043ND",
    page: "/toh/tp-link/tl-wr1043nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr1043nd_v4",
    routerPath: "tp-link~tp-link_tl-wr1043nd_v4"
  },
  {
    brand: "TP-Link",
    model: "Archer C2 AC750",
    page: "/toh/tp-link/archer_c2_ac750",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c2_ac750",
    routerPath: "tp-link~tp-link_archer_c2_ac750"
  },
  {
    brand: "TP-Link",
    model: "Archer C5 AC1200",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c5_ac1200_v2.0",
    routerPath: "tp-link~tp-link_archer_c5_ac1200_v2.0"
  },
  {
    brand: "TP-Link",
    model: "Archer C7 AC1750",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c7_ac1750_v2.0",
    routerPath: "tp-link~tp-link_archer_c7_ac1750_v2.0"
  },
  {
    brand: "TP-Link",
    model: "Archer C20i AC750",
    page: "/toh/tp-link/archer-c20i",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c20i_ac750_v1",
    routerPath: "tp-link~tp-link_archer_c20i_ac750_v1"
  },
  {
    brand: "TP-Link",
    model: "Archer MR200",
    page: "/toh/tp-link/archer-mr200",
    techData: "/toh/hwdata/tp-link/tp-link_archer_mr200v1",
    routerPath: "tp-link~tp-link_archer_mr200v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR710N",
    page: "/toh/tp-link/tl-wr710n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr710n_v1.0_us",
    routerPath: "tp-link~tp-link_tl-wr710n_v1.0_us"
  },
  {
    brand: "TP-Link",
    model: "TL-WR740N",
    page: "/toh/tp-link/tl-wr740n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr740n_v1",
    routerPath: "tp-link~tp-link_tl-wr740n_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR740N",
    page: "/toh/tp-link/tl-wr740n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr740n_v3",
    routerPath: "tp-link~tp-link_tl-wr740n_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WR740N",
    page: "/toh/tp-link/tl-wr740n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr740n_v4.23",
    routerPath: "tp-link~tp-link_tl-wr740n_v4.23"
  },
  {
    brand: "TP-Link",
    model: "TL-WR842ND",
    page: "/toh/tp-link/tl-wr842nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr842nd_v1",
    routerPath: "tp-link~tp-link_tl-wr842nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR842ND",
    page: "/toh/tp-link/tl-wr842nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr842nd_v2",
    routerPath: "tp-link~tp-link_tl-wr842nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR941ND",
    page: "/toh/tp-link/tl-wr941nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr941nd_v2",
    routerPath: "tp-link~tp-link_tl-wr941nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR941ND",
    page: "/toh/tp-link/tl-wr941nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr941nd_v3.2",
    routerPath: "tp-link~tp-link_tl-wr941nd_v3.2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR941ND",
    page: "/toh/tp-link/tl-wr941nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr941nd_v3",
    routerPath: "tp-link~tp-link_tl-wr941nd_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WR941ND",
    page: "/toh/tp-link/tl-wr941nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr941nd_v4",
    routerPath: "tp-link~tp-link_tl-wr941nd_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-MR11U",
    page: "/toh/tp-link/tl-mr11u",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr11u_v2",
    routerPath: "tp-link~tp-link_tl-mr11u_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WA801ND",
    page: "/toh/tp-link/tl-wa801nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa801nd_v1",
    routerPath: "tp-link~tp-link_tl-wa801nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR741ND",
    page: "/toh/tp-link/tl-wr741nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr741nd_v2",
    routerPath: "tp-link~tp-link_tl-wr741nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR843ND",
    page: "/toh/tp-link/tl-wr843nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr843nd_v1",
    routerPath: "tp-link~tp-link_tl-wr843nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR940N",
    page: "/toh/tp-link/tl-wr940n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr940n_v4",
    routerPath: "tp-link~tp-link_tl-wr940n_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-WR842N",
    page: "/toh/tp-link/tl-wr842nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr842n_v3",
    routerPath: "tp-link~tp-link_tl-wr842n_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR4900",
    page: "/toh/tp-link/tl-wdr4900",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr4900_v1",
    routerPath: "tp-link~tp-link_tl-wdr4900_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR4900",
    page: "/toh/tp-link/tl-wdr4900",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr4900_v2",
    routerPath: "tp-link~tp-link_tl-wdr4900_v2"
  },
  {
    brand: "TP-Link",
    model: "Archer C7R WDR7500",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c7r_wdr7500_v3_cn",
    routerPath: "tp-link~tp-link_archer_c7r_wdr7500_v3_cn"
  },
  {
    brand: "TP-Link",
    model: "Archer C7 AC1750",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c7_ac1750_v1",
    routerPath: "tp-link~tp-link_archer_c7_ac1750_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR10U",
    page: "/toh/tp-link/tl-mr10u",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr10u_v1",
    routerPath: "tp-link~tp-link_tl-mr10u_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR11U",
    page: "/toh/tp-link/tl-mr11u",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr11u_v1",
    routerPath: "tp-link~tp-link_tl-mr11u_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR12U",
    page: "/toh/tp-link/tl-mr12u",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr12u_v1",
    routerPath: "tp-link~tp-link_tl-mr12u_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR13U",
    page: "/toh/tp-link/tl-mr13u",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr13u_v1",
    routerPath: "tp-link~tp-link_tl-mr13u_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3020",
    page: "/toh/tp-link/tl-mr3020",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3020_v1",
    routerPath: "tp-link~tp-link_tl-mr3020_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3040",
    page: "/toh/tp-link/tl-mr3040",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3040_v1",
    routerPath: "tp-link~tp-link_tl-mr3040_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3220",
    page: "/toh/tp-link/tl-mr3420",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3220_v1",
    routerPath: "tp-link~tp-link_tl-mr3220_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3220",
    page: "/toh/tp-link/tl-mr3220",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3220_v2",
    routerPath: "tp-link~tp-link_tl-mr3220_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3420",
    page: "/toh/tp-link/tl-mr3420",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3420_v1",
    routerPath: "tp-link~tp-link_tl-mr3420_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3420",
    page: "/toh/tp-link/tl-mr3420",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3420_v2",
    routerPath: "tp-link~tp-link_tl-mr3420_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WA701ND",
    page: "/toh/tp-link/tl-wa701nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa701nd_v1",
    routerPath: "tp-link~tp-link_tl-wa701nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA701ND",
    page: "/toh/tp-link/tl-wa701nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa701nd_v2",
    routerPath: "tp-link~tp-link_tl-wa701nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WA750RE",
    page: "/toh/tp-link/tl-wa850re",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa750re_v1",
    routerPath: "tp-link~tp-link_tl-wa750re_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA860RE",
    page: "/toh/tp-link/tl-wa850re",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa860re_v1",
    routerPath: "tp-link~tp-link_tl-wa860re_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA901ND",
    page: "/toh/tp-link/tl-wa901nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa901nd_v1",
    routerPath: "tp-link~tp-link_tl-wa901nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA901ND",
    page: "/toh/tp-link/tl-wa901nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa901nd_v2",
    routerPath: "tp-link~tp-link_tl-wa901nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WA901ND",
    page: "/toh/tp-link/tl-wa901nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa901nd_v3",
    routerPath: "tp-link~tp-link_tl-wa901nd_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR3320",
    page: "/toh/tp-link/tl-wdr3320",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr3320_v2",
    routerPath: "tp-link~tp-link_tl-wdr3320_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR3500",
    page: "/toh/tp-link/tl-wdr3500",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr3500_v1",
    routerPath: "tp-link~tp-link_tl-wdr3500_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR3600",
    page: "/toh/tp-link/tl-wdr3600",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr3600_v1",
    routerPath: "tp-link~tp-link_tl-wdr3600_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WDR4310",
    page: "/toh/tp-link/tl-wdr4310",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wdr4310_v1",
    routerPath: "tp-link~tp-link_tl-wdr4310_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR1043ND",
    page: "/toh/tp-link/tl-wr1043nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr1043nd_v1",
    routerPath: "tp-link~tp-link_tl-wr1043nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR2543ND",
    page: "/toh/tp-link/tl-wr2543nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr2543nd_v1",
    routerPath: "tp-link~tp-link_tl-wr2543nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR703N",
    page: "/toh/tp-link/tl-wr703n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr703n_v1",
    routerPath: "tp-link~tp-link_tl-wr703n_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR710N",
    page: "/toh/tp-link/tl-wr710n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr710n_v1.1_eu",
    routerPath: "tp-link~tp-link_tl-wr710n_v1.1_eu"
  },
  {
    brand: "TP-Link",
    model: "TL-WR741ND",
    page: "/toh/tp-link/tl-wr741nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr741nd_v4",
    routerPath: "tp-link~tp-link_tl-wr741nd_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-WR743ND",
    page: "/toh/tp-link/tl-wr743nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr743nd_v2",
    routerPath: "tp-link~tp-link_tl-wr743nd_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR743ND",
    page: "/toh/tp-link/tl-wr743nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr743nd_v1",
    routerPath: "tp-link~tp-link_tl-wr743nd_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v1.5",
    routerPath: "tp-link~tp-link_tl-wr841n_v1.5"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841nd_v3",
    routerPath: "tp-link~tp-link_tl-wr841nd_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v7",
    routerPath: "tp-link~tp-link_tl-wr841n_v7"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v8",
    routerPath: "tp-link~tp-link_tl-wr841n_v8"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v9",
    routerPath: "tp-link~tp-link_tl-wr841n_v9"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841nd_v5",
    routerPath: "tp-link~tp-link_tl-wr841nd_v5"
  },
  {
    brand: "TP-Link",
    model: "TL-WA7510N",
    page: "/toh/tp-link/tl-wa7510n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa7510n_v1",
    routerPath: "tp-link~tp-link_tl-wa7510n_v1"
  },
  {
    brand: "TP-Link",
    model: "VR200v",
    page: "/toh/tp-link/vr200v",
    techData: "/toh/hwdata/tp-link/tp-link_vr200v",
    routerPath: "tp-link~tp-link_vr200v"
  },
  {
    brand: "TP-Link",
    model: "Archer C59",
    page: "/toh/tp-link/tp-link_archer_c59",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c59_v1",
    routerPath: "tp-link~tp-link_archer_c59_v1"
  },
  {
    brand: "TP-Link",
    model: "Archer C60",
    page: "/toh/tp-link/tp-link_archer_c60_v2",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c60_v1",
    routerPath: "tp-link~tp-link_archer_c60_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WA850RE",
    page: "/toh/tp-link/tl-wa850re",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa850re_v2",
    routerPath: "tp-link~tp-link_tl-wa850re_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N(D)",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v12",
    routerPath: "tp-link~tp-link_tl-wr841n_v12"
  },
  {
    brand: "TP-Link",
    model: "Archer C9",
    page: "/toh/tp-link/archer-c9",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c9_v1",
    routerPath: "tp-link~tp-link_archer_c9_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR6400",
    page: "/toh/tp-link/tp-link_tl-mr6400_v1.0",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr6400_v1",
    routerPath: "tp-link~tp-link_tl-mr6400_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR840N",
    page: "/toh/tp-link/tl-wr840n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr840n_v4",
    routerPath: "tp-link~tp-link_tl-wr840n_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v13",
    routerPath: "tp-link~tp-link_tl-wr841n_v13"
  },
  {
    brand: "TP-Link",
    model: "TL-WR902AC",
    page: "/toh/tp-link/tl-wr902ac_v1",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr902ac_v1",
    routerPath: "tp-link~tp-link_tl-wr902ac_v1"
  },
  {
    brand: "TP-Link",
    model: "RE350",
    page: "/toh/tp-link/tp-link_re350_v1",
    techData: "/toh/hwdata/tp-link/tp-link_re350_v1",
    routerPath: "tp-link~tp-link_re350_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR940N",
    page: "/toh/tp-link/tl-wr940n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr940n_v1",
    routerPath: "tp-link~tp-link_tl-wr940n_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR940N",
    page: "/toh/tp-link/tl-wr940n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr940n_v2",
    routerPath: "tp-link~tp-link_tl-wr940n_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR940N",
    page: "/toh/tp-link/tl-wr940n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr940n_v3",
    routerPath: "tp-link~tp-link_tl-wr940n_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WR847N",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr847n_v8",
    routerPath: "tp-link~tp-link_tl-wr847n_v8"
  },
  {
    brand: "TP-Link",
    model: "Archer C7 AC1750",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c7_v4",
    routerPath: "tp-link~tp-link_archer_c7_v4"
  },
  {
    brand: "TP-Link",
    model: "Archer C20 AC750",
    page: "/toh/tp-link/tp-link_archer_c20_v1",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c20_ac750_v1",
    routerPath: "tp-link~tp-link_archer_c20_ac750_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR940N",
    page: "/toh/tp-link/tl-wr940n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr940n_v5",
    routerPath: "tp-link~tp-link_tl-wr940n_v5"
  },
  {
    brand: "TP-Link",
    model: "TL-WA901ND",
    page: "/toh/tp-link/tl-wa901nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa901nd_v5",
    routerPath: "tp-link~tp-link_tl-wa901nd_v5"
  },
  {
    brand: "TP-Link",
    model: "TL-WR1043N",
    page: "/toh/tp-link/tl-wr1043nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr1043n_v5",
    routerPath: "tp-link~tp-link_tl-wr1043n_v5"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3420",
    page: "/toh/tp-link/tp-link_tl-mr3420_v5",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3420_v5",
    routerPath: "tp-link~tp-link_tl-mr3420_v5"
  },
  {
    brand: "TP-Link",
    model: "Archer C20 AC750",
    page: "/toh/tp-link/tp-link_archer_c20_v4",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c20_ac750_v4",
    routerPath: "tp-link~tp-link_archer_c20_ac750_v4"
  },
  {
    brand: "TP-Link",
    model: "TD-W8970",
    page: "/toh/tp-link/td-w8970_v1",
    techData: "/toh/hwdata/tp-link/tp-link_td-w8970_v1",
    routerPath: "tp-link~tp-link_td-w8970_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR740N",
    page: "/toh/tp-link/tl-wr740n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr740n_v2.1_br",
    routerPath: "tp-link~tp-link_tl-wr740n_v2.1_br"
  },
  {
    brand: "TP-Link",
    model: "TL-WR810N",
    page: "/toh/tp-link/tl-wr810n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr810n_v1.1_eu",
    routerPath: "tp-link~tp-link_tl-wr810n_v1.1_eu"
  },
  {
    brand: "TP-Link",
    model: "TL-WR810N",
    page: "/toh/tp-link/tl-wr810n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr810n_v1.0_us",
    routerPath: "tp-link~tp-link_tl-wr810n_v1.0_us"
  },
  {
    brand: "TP-Link",
    model: "Archer C50",
    page: "/toh/tp-link/archer-c50",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c50_v3",
    routerPath: "tp-link~tp-link_archer_c50_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WR802N",
    page: "/toh/tp-link/tl-wr802n_v4",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr802n_v4",
    routerPath: "tp-link~tp-link_tl-wr802n_v4"
  },
  {
    brand: "TP-Link",
    model: "RE355",
    page: "/toh/tp-link/tp-link_re355_v1",
    techData: "/toh/hwdata/tp-link/tp-link_re355",
    routerPath: "tp-link~tp-link_re355"
  },
  {
    brand: "TP-Link",
    model: "TL-WR902AC",
    page: "/toh/tp-link/tl-wr902ac_v3",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr902ac_v3",
    routerPath: "tp-link~tp-link_tl-wr902ac_v3"
  },
  {
    brand: "TP-Link",
    model: "Archer C5 AC1200",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c5_ac1200_v1",
    routerPath: "tp-link~tp-link_archer_c5_ac1200_v1"
  },
  {
    brand: "TP-Link",
    model: "Archer C60",
    page: "/toh/tp-link/tp-link_archer_c60_v2",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c60_v2",
    routerPath: "tp-link~tp-link_archer_c60_v2"
  },
  {
    brand: "TP-Link",
    model: "TL-WR940N",
    page: "/toh/tp-link/tl-wr940n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr940n_v6",
    routerPath: "tp-link~tp-link_tl-wr940n_v6"
  },
  {
    brand: "TP-Link",
    model: "Archer VR2600v",
    page: "/toh/tp-link/tp-link_archer_vr2600v_v1",
    techData: "/toh/hwdata/tp-link/tp-link_vr2600v_v1",
    routerPath: "tp-link~tp-link_vr2600v_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-WR842N",
    page: "/toh/tp-link/tl-wr842nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr842n_v5",
    routerPath: "tp-link~tp-link_tl-wr842n_v5"
  },
  {
    brand: "TP-Link",
    model: "TD-W8960N",
    page: "/toh/tp-link/td-w8960n",
    techData: "/toh/hwdata/tp-link/tp-link_td-w8960n_v3",
    routerPath: "tp-link~tp-link_td-w8960n_v3"
  },
  {
    brand: "TP-Link",
    model: "Archer C7 AC1750",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c7_v5",
    routerPath: "tp-link~tp-link_archer_c7_v5"
  },
  {
    brand: "TP-Link",
    model: "CPE210",
    page: "/toh/tp-link/cpe210",
    techData: "/toh/hwdata/tp-link/tp-link_cpe210_v2",
    routerPath: "tp-link~tp-link_cpe210_v2"
  },
  {
    brand: "TP-Link",
    model: "RE450",
    page: "/toh/tp-link/tp-link_re450",
    techData: "/toh/hwdata/tp-link/tp-link_re450_v2",
    routerPath: "tp-link~tp-link_re450_v2"
  },
  {
    brand: "TP-Link",
    model: "CPE510",
    page: "/toh/tp-link/cpe510",
    techData: "/toh/hwdata/tp-link/tp-link_cpe510_v1",
    routerPath: "tp-link~tp-link_cpe510_v1"
  },
  {
    brand: "TP-Link",
    model: "CPE210",
    page: "/toh/tp-link/cpe210",
    techData: "/toh/hwdata/tp-link/tp-link_cpe210_v1",
    routerPath: "tp-link~tp-link_cpe210_v1"
  },
  {
    brand: "TP-Link",
    model: "CPE220",
    page: "/toh/tp-link/cpe210",
    techData: "/toh/hwdata/tp-link/tp-link_cpe220_v1",
    routerPath: "tp-link~tp-link_cpe220_v1"
  },
  {
    brand: "TP-Link",
    model: "Archer C59",
    page: "/toh/tp-link/tp-link_archer_c59",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c59_v2",
    routerPath: "tp-link~tp-link_archer_c59_v2"
  },
  {
    brand: "TP-Link",
    model: "TD-W8960N",
    page: "/toh/tp-link/td-w8960n",
    techData: "/toh/hwdata/tp-link/tp-link_td-w8960n_v4",
    routerPath: "tp-link~tp-link_td-w8960n_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3020",
    page: "/toh/tp-link/tl-mr3020_v3",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3020_v3",
    routerPath: "tp-link~tp-link_tl-mr3020_v3"
  },
  {
    brand: "TP-Link",
    model: "Archer C7 AC1750",
    page: "/toh/tp-link/archer-c5-c7-wdr7500",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c7_ac1750_v3",
    routerPath: "tp-link~tp-link_archer_c7_ac1750_v3"
  },
  {
    brand: "TP-Link",
    model: "TL-WA801ND",
    page: "/toh/tp-link/tl-wa801nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wa801nd_v4",
    routerPath: "tp-link~tp-link_tl-wa801nd_v4"
  },
  {
    brand: "TP-Link",
    model: "Archer A7",
    page: "/toh/tp-link/archer_a7",
    techData: "/toh/hwdata/tp-link/tp-link_archer_a7_v5",
    routerPath: "tp-link~tp-link_archer_a7_v5"
  },
  {
    brand: "TP-Link",
    model: "Archer C6",
    page: "/toh/tp-link/tp-link_archer_c6_v2",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c6_v2_eu",
    routerPath: "tp-link~tp-link_archer_c6_v2_eu"
  },
  {
    brand: "TP-Link",
    model: "TL-WR802N",
    page: "/toh/tp-link/tl-wr802n",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr802n_v1",
    routerPath: "tp-link~tp-link_tl-wr802n_v1"
  },
  {
    brand: "TP-Link",
    model: "RE350K",
    page: "/toh/tp-link/tp-link_re350k_v1",
    techData: "/toh/hwdata/tp-link/tp-link_re350k_v1",
    routerPath: "tp-link~tp-link_re350k_v1"
  },
  {
    brand: "TP-Link",
    model: "CPE210",
    page: "/toh/tp-link/cpe210",
    techData: "/toh/hwdata/tp-link/tp-link_cpe210_v3",
    routerPath: "tp-link~tp-link_cpe210_v3"
  },
  {
    brand: "TP-Link",
    model: "Archer C50",
    page: "/toh/tp-link/archer-c50",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c50_v4",
    routerPath: "tp-link~tp-link_archer_c50_v4"
  },
  {
    brand: "TP-Link",
    model: "Archer C5",
    page: "/toh/tp-link/archer_c5_v4",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c5_v4",
    routerPath: "tp-link~tp-link_archer_c5_v4"
  },
  {
    brand: "TP-Link",
    model: "TL-WR841N",
    page: "/toh/tp-link/tl-wr841nd",
    techData: "/toh/hwdata/tp-link/tp-link_tl-wr841n_v14",
    routerPath: "tp-link~tp-link_tl-wr841n_v14"
  },
  {
    brand: "TP-Link",
    model: "CPE510",
    page: "/toh/tp-link/cpe510",
    techData: "/toh/hwdata/tp-link/tp-link_cpe510_v2",
    routerPath: "tp-link~tp-link_cpe510_v2"
  },
  {
    brand: "TP-Link",
    model: "CPE510",
    page: "/toh/tp-link/cpe510",
    techData: "/toh/hwdata/tp-link/tp-link_cpe510_v3",
    routerPath: "tp-link~tp-link_cpe510_v3"
  },
  {
    brand: "TP-Link",
    model: "RE650",
    page: "/toh/tp-link/tp-link_re650_v1",
    techData: "/toh/hwdata/tp-link/tp-link_re650_v1",
    routerPath: "tp-link~tp-link_re650_v1"
  },
  {
    brand: "TP-Link",
    model: "Archer C6",
    page: "/toh/tp-link/tp-link_archer_c6_v2",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c6_v2_us",
    routerPath: "tp-link~tp-link_archer_c6_v2_us"
  },
  {
    brand: "TP-Link",
    model: "RE200",
    page: "/toh/tp-link/re200",
    techData: "/toh/hwdata/tp-link/tp-link_re200_v1",
    routerPath: "tp-link~tp-link_re200_v1"
  },
  {
    brand: "TP-Link",
    model: "Archer C50",
    page: "/toh/tp-link/archer-c50",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c50_v5",
    routerPath: "tp-link~tp-link_archer_c50_v5"
  },
  {
    brand: "TP-Link",
    model: "RE200",
    page: "/toh/tp-link/re200",
    techData: "/toh/hwdata/tp-link/tp-link_re200_v2",
    routerPath: "tp-link~tp-link_re200_v2"
  },
  {
    brand: "TP-Link",
    model: "Archer C60",
    page: "/toh/tp-link/tp-link_archer_c60_v2",
    techData: "/toh/hwdata/tp-link/tp-link_archer_c60_v3",
    routerPath: "tp-link~tp-link_archer_c60_v3"
  },
  {
    brand: "TP-Link",
    model: "Archer D7 / D7b",
    page: "/toh/tp-link/archer-d7",
    techData: "/toh/hwdata/tp-link/tp-link_archer_d7_v1",
    routerPath: "tp-link~tp-link_archer_d7_v1"
  },
  {
    brand: "TP-Link",
    model: "TL-MR3420",
    page: "/toh/tp-link/tl-mr3420",
    techData: "/toh/hwdata/tp-link/tp-link_tl-mr3420_v3",
    routerPath: "tp-link~tp-link_tl-mr3420_v3"
  },
  {
    brand: "TRENDnet",
    model: "TEW-632BRP",
    page: "/toh/trendnet/tew632brp",
    techData: "/toh/hwdata/trendnet/trendnet_tew-632brp",
    routerPath: "trendnet~trendnet_tew-632brp"
  },
  {
    brand: "TRENDnet",
    model: "TEW-673GRU",
    page: "/toh/trendnet/tew-673gru",
    techData: "/toh/hwdata/trendnet/trendnet_tew-673gru_10r",
    routerPath: "trendnet~trendnet_tew-673gru_10r"
  },
  {
    brand: "TRENDnet",
    model: "TEW-691GR",
    page: "/toh/trendnet/tew-691gr",
    techData: "/toh/hwdata/trendnet/trendnet_tew-691gr_10r",
    routerPath: "trendnet~trendnet_tew-691gr_10r"
  },
  {
    brand: "TRENDnet",
    model: "TEW-692GR",
    page: "/toh/trendnet/tew-692gr",
    techData: "/toh/hwdata/trendnet/trendnet_tew-692gr_10r",
    routerPath: "trendnet~trendnet_tew-692gr_10r"
  },
  {
    brand: "TRENDnet",
    model: "TEW-712BR",
    page: "/toh/trendnet/tew-712br",
    techData: "/toh/hwdata/trendnet/trendnet_tew-712br_10",
    routerPath: "trendnet~trendnet_tew-712br_10"
  },
  {
    brand: "TRENDnet",
    model: "TEW-732BR",
    page: "/toh/trendnet/tew-732br",
    techData: "/toh/hwdata/trendnet/trendnet_tew-732br_10r",
    routerPath: "trendnet~trendnet_tew-732br_10r"
  },
  {
    brand: "TRENDnet",
    model: "TEW-823DRU",
    page: "/toh/trendnet/tew-823dru",
    techData: "/toh/hwdata/trendnet/trendnet_tew-823dru",
    routerPath: "trendnet~trendnet_tew-823dru"
  },
  {
    brand: "TRENDnet",
    model: "TEW-652BRP",
    page: "/toh/trendnet/tew632brp",
    techData: "/toh/hwdata/trendnet/trendnet_tew-652brp",
    routerPath: "trendnet~trendnet_tew-652brp"
  },
  {
    brand: "TRENDnet",
    model: "TEW-714TRU",
    page: "/toh/trendnet/tew-714tru",
    techData: "/toh/hwdata/trendnet/trendnet_tew-714tru_1.0r",
    routerPath: "trendnet~trendnet_tew-714tru_1.0r"
  },
  {
    brand: "TRENDnet",
    model: "TEW-829DRU",
    page: "/toh/trendnet/trendnet_tew-829dru",
    techData: "/toh/hwdata/trendnet/trendnet_tew-829dru_ac3000",
    routerPath: "trendnet~trendnet_tew-829dru_ac3000"
  },
  {
    brand: "Turris CZ.NIC",
    model: "Omnia",
    page: "/toh/turris/turris_omnia",
    techData: "/toh/hwdata/turris/turris_turris_omnia",
    routerPath: "turris~turris_turris_omnia"
  },
  {
    brand: "Ubiquiti",
    model: "AirGrid M2",
    page: "/toh/ubiquiti/airgrid",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_airgridm2",
    routerPath: "ubiquiti~ubiquiti_airgridm2"
  },
  {
    brand: "Ubiquiti",
    model: "AirGrid M5",
    page: "/toh/ubiquiti/airgrid",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_airgridm5",
    routerPath: "ubiquiti~ubiquiti_airgridm5"
  },
  {
    brand: "Ubiquiti",
    model: "AirRouter",
    page: "/toh/ubiquiti/airrouter",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_airrouter",
    routerPath: "ubiquiti~ubiquiti_airrouter"
  },
  {
    brand: "Ubiquiti",
    model: "Bullet M",
    page: "/toh/ubiquiti/bullet",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_bullet",
    routerPath: "ubiquiti~ubiquiti_bullet"
  },
  {
    brand: "Ubiquiti",
    model: "EdgeRouter X",
    page: "/toh/ubiquiti/ubiquiti_edgerouter_x_er-x_ka",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_edgerouter_x",
    routerPath: "ubiquiti~ubiquiti_edgerouter_x"
  },
  {
    brand: "Ubiquiti",
    model: "LiteStation 2",
    page: "/toh/ubiquiti/nanostation",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_litestation2",
    routerPath: "ubiquiti~ubiquiti_litestation2"
  },
  {
    brand: "Ubiquiti",
    model: "LiteStation 5",
    page: "/toh/ubiquiti/picostation5",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_litestation5",
    routerPath: "ubiquiti~ubiquiti_litestation5"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation 2",
    page: "/toh/ubiquiti/nanostation",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation2",
    routerPath: "ubiquiti~ubiquiti_nanostation2"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation 5",
    page: "/toh/ubiquiti/picostation5",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation5",
    routerPath: "ubiquiti~ubiquiti_nanostation5"
  },
  {
    brand: "Ubiquiti",
    model: "PicoStation 2",
    page: "/toh/ubiquiti/nanostation",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_picostation2",
    routerPath: "ubiquiti~ubiquiti_picostation2"
  },
  {
    brand: "Ubiquiti",
    model: "PicoStation 5",
    page: "/toh/ubiquiti/picostation5",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_picostation5",
    routerPath: "ubiquiti~ubiquiti_picostation5"
  },
  {
    brand: "Ubiquiti",
    model: "RouterStation",
    page: "/toh/ubiquiti/routerstation",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_routerstation",
    routerPath: "ubiquiti~ubiquiti_routerstation"
  },
  {
    brand: "Ubiquiti",
    model: "RouterStation Pro",
    page: "/toh/ubiquiti/routerstation.pro",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_routerstationpro",
    routerPath: "ubiquiti~ubiquiti_routerstationpro"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi AP",
    page: "/toh/ubiquiti/unifi",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi",
    routerPath: "ubiquiti~ubiquiti_unifi"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi AP AC Lite",
    page: "/toh/ubiquiti/unifiac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi_apac_lite",
    routerPath: "ubiquiti~ubiquiti_unifi_apac_lite"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi AP AC LR",
    page: "/toh/ubiquiti/unifiac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi_apac_lr",
    routerPath: "ubiquiti~ubiquiti_unifi_apac_lr"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi AP AC PRO",
    page: "/toh/ubiquiti/unifiac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi_apac_pro",
    routerPath: "ubiquiti~ubiquiti_unifi_apac_pro"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation Loco M5 xw",
    page: "/toh/ubiquiti/nanostationm5",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_loco_m5_xw",
    routerPath: "ubiquiti~ubiquiti_nanostation_loco_m5_xw"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation Loco2",
    page: "/toh/ubiquiti/nanostation",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_loco2",
    routerPath: "ubiquiti~ubiquiti_nanostation_loco2"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation Loco5",
    page: "/toh/ubiquiti/picostation5",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_loco5",
    routerPath: "ubiquiti~ubiquiti_nanostation_loco5"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation Loco M2",
    page: "/toh/ubiquiti/nanostationm2",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_loco_m2",
    routerPath: "ubiquiti~ubiquiti_nanostation_loco_m2"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation M5 xw",
    page: "/toh/ubiquiti/nanostationm5",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_m5_xw",
    routerPath: "ubiquiti~ubiquiti_nanostation_m5_xw"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation M5 xm",
    page: "/toh/ubiquiti/nanostationm5",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_m5_xm",
    routerPath: "ubiquiti~ubiquiti_nanostation_m5_xm"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation M2",
    page: "/toh/ubiquiti/nanostationm2",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_m2",
    routerPath: "ubiquiti~ubiquiti_nanostation_m2"
  },
  {
    brand: "Ubiquiti",
    model: "PicoStation M2HP",
    page: "/toh/ubiquiti/picostationm2",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_picostation_m2_hp",
    routerPath: "ubiquiti~ubiquiti_picostation_m2_hp"
  },
  {
    brand: "Ubiquiti",
    model: "NanoBeam M5",
    page: "/toh/ubiquiti/nanobeam",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanobeam_m5",
    routerPath: "ubiquiti~ubiquiti_nanobeam_m5"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi AP PRO",
    page: "/toh/ubiquiti/unifi_appro",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi_ap_pro",
    routerPath: "ubiquiti~ubiquiti_unifi_ap_pro"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi Outdoor Plus",
    page: "/toh/ubiquiti/unifi_outdoorplus",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi_outdoorplus",
    routerPath: "ubiquiti~ubiquiti_unifi_outdoorplus"
  },
  {
    brand: "Ubiquiti",
    model: "EdgeRouter Lite",
    page: "/toh/ubiquiti/edgerouter.lite",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_edgerouter_lite",
    routerPath: "ubiquiti~ubiquiti_edgerouter_lite"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi AC Mesh",
    page: "/toh/ubiquiti/unifiac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi_ac_mesh",
    routerPath: "ubiquiti~ubiquiti_unifi_ac_mesh"
  },
  {
    brand: "Ubiquiti",
    model: "EdgeRouter X-SFP",
    page: "/toh/ubiquiti/ubiquiti_edgerouter_x_er-x_ka",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_edgerouter_x-sfp",
    routerPath: "ubiquiti~ubiquiti_edgerouter_x-sfp"
  },
  {
    brand: "Ubiquiti",
    model: "AirGateway Pro (AMG-PRO)",
    page: "/toh/ubiquiti/airgatewaypro",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_airgateway_pro",
    routerPath: "ubiquiti~ubiquiti_airgateway_pro"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation AC",
    page: "/toh/ubiquiti/ubiquiti_nanostation_ac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_ac",
    routerPath: "ubiquiti~ubiquiti_nanostation_ac"
  },
  {
    brand: "Ubiquiti",
    model: "PowerBeam M5-400",
    page: "/toh/ubiquiti/powerbeam",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_powerbeam_m5-400",
    routerPath: "ubiquiti~ubiquiti_powerbeam_m5-400"
  },
  {
    brand: "Ubiquiti",
    model: "PowerBeam M5-300",
    page: "/toh/ubiquiti/powerbeam",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_powerbeam_m5-300",
    routerPath: "ubiquiti~ubiquiti_powerbeam_m5-300"
  },
  {
    brand: "Ubiquiti",
    model: "PowerBeam M2-400",
    page: "/toh/ubiquiti/powerbeam",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_powerbeam_m2-400",
    routerPath: "ubiquiti~ubiquiti_powerbeam_m2-400"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation Loco M2 xw",
    page: "/toh/ubiquiti/nanostationm2",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_loco_m2_xw",
    routerPath: "ubiquiti~ubiquiti_nanostation_loco_m2_xw"
  },
  {
    brand: "Ubiquiti",
    model: "UniFi AC Mesh Pro",
    page: "/toh/ubiquiti/unifiac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_unifi_ac_mesh_pro",
    routerPath: "ubiquiti~ubiquiti_unifi_ac_mesh_pro"
  },
  {
    brand: "Ubiquiti",
    model: "NanoStation AC loco",
    page: "/toh/ubiquiti/ubiquiti_nanostation_ac_loco",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanostation_ac_loco",
    routerPath: "ubiquiti~ubiquiti_nanostation_ac_loco"
  },
  {
    brand: "Ubiquiti",
    model: "Bullet M xw",
    page: "/toh/ubiquiti/bullet",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_bullet_m_xw",
    routerPath: "ubiquiti~ubiquiti_bullet_m_xw"
  },
  {
    brand: "Ubiquiti",
    model: "NanoBeam 5AC Gen 2",
    page: "/toh/ubiquiti/ubiquiti_nanobeam_ac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanobeam_ac",
    routerPath: "ubiquiti~ubiquiti_nanobeam_ac"
  },
  {
    brand: "Ubiquiti",
    model: "airCube ISP",
    page: "/toh/ubiquiti/ubiquiti_aircube_isp",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_aircube_isp",
    routerPath: "ubiquiti~ubiquiti_aircube_isp"
  },
  {
    brand: "Ubiquiti",
    model: "LiteBeam 5AC Gen2",
    page: "/toh/ubiquiti/litebeam_5ac_gen2",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_litebeam_5ac_gen2",
    routerPath: "ubiquiti~ubiquiti_litebeam_5ac_gen2"
  },
  {
    brand: "Ubiquiti",
    model: "NanoBeam 5AC 16",
    page: "/toh/ubiquiti/ubiquiti_nanobeam_ac",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanobeam_ac_16",
    routerPath: "ubiquiti~ubiquiti_nanobeam_ac_16"
  },
  {
    brand: "Ubiquiti",
    model: "NanoBridge M",
    page: "/toh/ubiquiti/bullet",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_nanobridge_m",
    routerPath: "ubiquiti~ubiquiti_nanobridge_m"
  },
  {
    brand: "Ubiquiti",
    model: "EdgeSwitch 8XP (ES-8XP)",
    page: "/toh/ubiquiti/ubiquiti_edgeswitch_8xp",
    techData: "/toh/hwdata/ubiquiti/ubiquiti_edgeswitch_8xp",
    routerPath: "ubiquiti~ubiquiti_edgeswitch_8xp"
  },
  {
    brand: "UniElec",
    model: "U7628-01",
    page: "/toh/unielec/unielec_u7628-01",
    techData: "/toh/hwdata/unielec/unielec_u7628-01",
    routerPath: "unielec~unielec_u7628-01"
  },
  {
    brand: "UniElec",
    model: "U7621-06",
    page: "/toh/unielec/u7621-06",
    techData: "/toh/hwdata/unielec/unielec_u7621-06_512m_64m",
    routerPath: "unielec~unielec_u7621-06_512m_64m"
  },
  {
    brand: "UniElec",
    model: "U7621-06",
    page: "/toh/unielec/u7621-06",
    techData: "/toh/hwdata/unielec/unielec_u7621-06_256m_16m",
    routerPath: "unielec~unielec_u7621-06_256m_16m"
  },
  {
    brand: "Upvel",
    model: "UR-326N4G",
    page: "/toh/upvel/ur-326n4g",
    techData: "/toh/hwdata/upvel/upvel_ur-326n4g",
    routerPath: "upvel~upvel_ur-326n4g"
  },
  {
    brand: "Upvel",
    model: "UR-336UN",
    page: "/toh/upvel/ur336un",
    techData: "/toh/hwdata/upvel/upvel_ur-336un",
    routerPath: "upvel~upvel_ur-336un"
  },
  {
    brand: "US Robotics",
    model: "USR9108",
    page: "/toh/us.robotics/usr9108",
    techData: "/toh/hwdata/usrobotics/usrobotics_usr9108_a",
    routerPath: "usrobotics~usrobotics_usr9108_a"
  },
  {
    brand: "VMware",
    model: "VMware x86 virtual machine",
    page: "/docs/guide-user/virtualization/vmware",
    techData: "/toh/hwdata/other/other_vmware_x86_virtual_machine_2015_latest",
    routerPath: "other~other_vmware_x86_virtual_machine_2015_latest"
  },
  {
    brand: "VoCore",
    model: "VoCore",
    page: "/toh/vocore/vocore",
    techData: "/toh/hwdata/vocore/vocore_vocore_v1.0",
    routerPath: "vocore~vocore_vocore_v1.0"
  },
  {
    brand: "Wallys",
    model: "DR344-NAS27",
    page: "/toh/wallys/wallys_dr344-nas27",
    techData: "/toh/hwdata/wallys/wallys_dr344-nas27",
    routerPath: "wallys~wallys_dr344-nas27"
  },
  {
    brand: "Wallys",
    model: "DR40X9",
    page: "/toh/wallys/wallys_dr40x9_v02",
    techData: "/toh/hwdata/wallys/wallys_dr40x9_v02",
    routerPath: "wallys~wallys_dr40x9_v02"
  },
  {
    brand: "Wansview",
    model: "NCS601W",
    page: "/toh/wansview/wansview_ncs601w",
    techData: "/toh/hwdata/wansview/wansview_ncs601w",
    routerPath: "wansview~wansview_ncs601w"
  },
  {
    brand: "Wavlink",
    model: "WL-WN575A3B",
    page: "/toh/wavlink/wl-wn575a3",
    techData: "/toh/hwdata/wavlink/wl-wn575a3",
    routerPath: "wavlink~wl-wn575a3"
  },
  {
    brand: "Western Digital",
    model: "My Net N600",
    page: "/toh/wd/n600",
    techData: "/toh/hwdata/westerndigital/westerndigital_mynetn600",
    routerPath: "westerndigital~westerndigital_mynetn600"
  },
  {
    brand: "Western Digital",
    model: "My Net N750",
    page: "/toh/wd/n750",
    techData: "/toh/hwdata/westerndigital/westerndigital_mynetn750",
    routerPath: "westerndigital~westerndigital_mynetn750"
  },
  {
    brand: "Western Digital",
    model: "My Net Wi-Fi Range Extender",
    page: "/toh/wd/rext",
    techData:
      "/toh/hwdata/westerndigital/westerndigital_mynetwi-fi_rangeextender",
    routerPath: "westerndigital~westerndigital_mynetwi-fi_rangeextender"
  },
  {
    brand: "Western Digital",
    model: "MyBook Live",
    page: "/toh/wd/mybooklive",
    techData: "/toh/hwdata/westerndigital/westerndigital_mybook_live_duo",
    routerPath: "westerndigital~westerndigital_mybook_live_duo"
  },
  {
    brand: "Western Digital",
    model: "MyBook Live",
    page: "/toh/wd/mybooklive",
    techData: "/toh/hwdata/westerndigital/westerndigital_mybook_live_single",
    routerPath: "westerndigital~westerndigital_mybook_live_single"
  },
  {
    brand: "Widemac",
    model: "SL-R7205",
    page: "/toh/widemac/sl-r7205",
    techData: "/toh/hwdata/widemac/widemac_sl-r7205",
    routerPath: "widemac~widemac_sl-r7205"
  },
  {
    brand: "Wiligear",
    model: "WBD-111",
    page: "/toh/wiligear/wbd-111",
    techData: "/toh/hwdata/wiligear/wiligear_wbd-111",
    routerPath: "wiligear~wiligear_wbd-111"
  },
  {
    brand: "WIZnet",
    model: "WizFi630A",
    page: "/toh/wiznet/wiznet_wizfi630a",
    techData: "/toh/hwdata/wiznet/wiznet_wizfi630a",
    routerPath: "wiznet~wiznet_wizfi630a"
  },
  {
    brand: "WRTnode",
    model: "WRTnode",
    page: "/toh/wrtnode/wrtnode",
    techData: "/toh/hwdata/wrtnode/wrtnode_wrtnode_1",
    routerPath: "wrtnode~wrtnode_wrtnode_1"
  },
  {
    brand: "Xiaomi",
    model: "Mi WiFi Mini",
    page: "/toh/xiaomi/mini",
    techData: "/toh/hwdata/xiaomi/xiaomi_mini_v1",
    routerPath: "xiaomi~xiaomi_mini_v1"
  },
  {
    brand: "Xiaomi",
    model: "MiWiFi Nano",
    page: "/toh/xiaomi/nano",
    techData: "/toh/hwdata/xiaomi/xiaomi_miwifi_nano",
    routerPath: "xiaomi~xiaomi_miwifi_nano"
  },
  {
    brand: "Xiaomi",
    model: "MiWiFi 3G",
    page: "/toh/xiaomi/mir3g",
    techData: "/toh/hwdata/xiaomi/xiaomi_miwifi_3g",
    routerPath: "xiaomi~xiaomi_miwifi_3g"
  },
  {
    brand: "Xiaomi",
    model: "Mi Router 3 Pro",
    page: "/toh/xiaomi/xiaomi_r3p_pro",
    techData: "/toh/hwdata/xiaomi/xiaomi_mi_router_3_pro",
    routerPath: "xiaomi~xiaomi_mi_router_3_pro"
  },
  {
    brand: "Xiaomi",
    model: "MiWiFi 3G",
    page: "/toh/xiaomi/mir3g",
    techData: "/toh/hwdata/xiaomi/xiaomi_miwifi_3g_v2",
    routerPath: "xiaomi~xiaomi_miwifi_3g_v2"
  },
  {
    brand: "Xiaomi",
    model: "Mi Router 4A (MIR4A)",
    page: "/inbox/toh/xiaomi/xiaomi_mi_router_4a_gigabit_edition",
    techData: "/toh/hwdata/xiaomi/xiaomi_mi_router_4a_gbit",
    routerPath: "xiaomi~xiaomi_mi_router_4a_gbit"
  },
  {
    brand: "Xunlong",
    model: "Orange Pi Plus",
    page: "/toh/xunlong/orangepiplus",
    techData: "/toh/hwdata/xunlong/xunlong_orange_pi_plus",
    routerPath: "xunlong~xunlong_orange_pi_plus"
  },
  {
    brand: "Xunlong",
    model: "Orange Pi Zero",
    page: "/toh/xunlong/orange_pi_zero",
    techData: "/toh/hwdata/xunlong/xunlong_orange_pi_zero",
    routerPath: "xunlong~xunlong_orange_pi_zero"
  },
  {
    brand: "YunCore",
    model: "SR3200",
    page: "/toh/yuncore/yuncore_sr3200",
    techData: "/toh/hwdata/yuncore/yuncore_sr3200",
    routerPath: "yuncore~yuncore_sr3200"
  },
  {
    brand: "ZBT",
    model: "WA-05",
    page: "/toh/zbt/wa-05",
    techData: "/toh/hwdata/zbt/zbt_wa-05",
    routerPath: "zbt~zbt_wa-05"
  },
  {
    brand: "ZBT",
    model: "WG2626",
    page: "/toh/zbt/wg2626",
    techData: "/toh/hwdata/zbt/zbt_wg2626",
    routerPath: "zbt~zbt_wg2626"
  },
  {
    brand: "ZBT",
    model: "WR8305RT",
    page: "/toh/zbt/wr8305rt",
    techData: "/toh/hwdata/zbt/zbt_wr8305rt",
    routerPath: "zbt~zbt_wr8305rt"
  },
  {
    brand: "ZBT",
    model: "WE826",
    page: "/toh/zbt/we-826",
    techData: "/toh/hwdata/zbt/zbt_we826_32m",
    routerPath: "zbt~zbt_we826_32m"
  },
  {
    brand: "ZBT",
    model: "WE826",
    page: "/toh/zbt/we-826",
    techData: "/toh/hwdata/zbt/zbt_we826_16m",
    routerPath: "zbt~zbt_we826_16m"
  },
  {
    brand: "ZBT",
    model: "APE522II",
    page: "/toh/zbt/ape522ii",
    techData: "/toh/hwdata/zbt/zbt_ape522ii",
    routerPath: "zbt~zbt_ape522ii"
  },
  {
    brand: "ZBT",
    model: "WE826",
    page: "/toh/zbt/we-826",
    techData: "/toh/hwdata/zbt/zbt_we826t_16m",
    routerPath: "zbt~zbt_we826t_16m"
  },
  {
    brand: "ZBT",
    model: "WE826",
    page: "/toh/zbt/we-826",
    techData: "/toh/hwdata/zbt/zbt_we826t_32m",
    routerPath: "zbt~zbt_we826t_32m"
  },
  {
    brand: "ZTE",
    model: "ZXDSL 531B(II)",
    page: "/toh/netcomm/nb6plus4w",
    techData: "/toh/hwdata/zte/zte_zxdsl531b",
    routerPath: "zte~zte_zxdsl531b"
  },
  {
    brand: "ZTE",
    model: "ZXHN H108N",
    page: "/toh/zte/zxhnh108n",
    techData: "/toh/hwdata/zte/zte_zxhn_h108n_v1",
    routerPath: "zte~zte_zxhn_h108n_v1"
  },
  {
    brand: "ZTE",
    model: "ZXV10 H201L",
    page: "/toh/zte/zxv10h201l",
    techData: "/toh/hwdata/zte/zte_zxv10h201l",
    routerPath: "zte~zte_zxv10h201l"
  },
  {
    brand: "ZyXEL",
    model: "EMG2926-Q10A",
    page: "/toh/zyxel/zyxel_nbg6716",
    techData: "/toh/hwdata/zyxel/zyxel_emg2926-q10a_1.00",
    routerPath: "zyxel~zyxel_emg2926-q10a_1.00"
  },
  {
    brand: "ZyXEL",
    model: "Keenetic",
    page: "/toh/zyxel/keenetic",
    techData: "/toh/hwdata/zyxel/zyxel_keenetic",
    routerPath: "zyxel~zyxel_keenetic"
  },
  {
    brand: "ZyXEL",
    model: "NBG-419N",
    page: "/toh/zyxel/zyxel_nbg-419n_v2",
    techData: "/toh/hwdata/zyxel/zyxel_nbg-419n_v2",
    routerPath: "zyxel~zyxel_nbg-419n_v2"
  },
  {
    brand: "ZyXEL",
    model: "NBG460N",
    page: "/toh/zyxel/nbg460n",
    techData: "/toh/hwdata/zyxel/zyxel_nbg460n_1",
    routerPath: "zyxel~zyxel_nbg460n_1"
  },
  {
    brand: "ZyXEL",
    model: "NBG550N",
    page: "/toh/zyxel/nbg460n",
    techData: "/toh/hwdata/zyxel/zyxel_nbg550n_1",
    routerPath: "zyxel~zyxel_nbg550n_1"
  },
  {
    brand: "ZyXEL",
    model: "NBG550NH",
    page: "/toh/zyxel/nbg460n",
    techData: "/toh/hwdata/zyxel/zyxel_nbg550nh_1",
    routerPath: "zyxel~zyxel_nbg550nh_1"
  },
  {
    brand: "ZyXEL",
    model: "NBG6616",
    page: "/toh/zyxel/nbg6616",
    techData: "/toh/hwdata/zyxel/zyxel_nbg6616",
    routerPath: "zyxel~zyxel_nbg6616"
  },
  {
    brand: "ZyXEL",
    model: "NBG6716",
    page: "/toh/zyxel/zyxel_nbg6716",
    techData: "/toh/hwdata/zyxel/zyxel_nbg6716_a01",
    routerPath: "zyxel~zyxel_nbg6716_a01"
  },
  {
    brand: "ZyXEL",
    model: "P-870HW-51a",
    page: "/toh/zyxel/p-870hw-51a",
    techData: "/toh/hwdata/zyxel/zyxel_p-870hw-51a_v2",
    routerPath: "zyxel~zyxel_p-870hw-51a_v2"
  },
  {
    brand: "ZyXEL",
    model: "P-2812HNU-F1",
    page: "/toh/zyxel/p2812hnu-f1",
    techData: "/toh/hwdata/zyxel/zyxel_p-2812hnu-f1",
    routerPath: "zyxel~zyxel_p-2812hnu-f1"
  },
  {
    brand: "ZyXEL",
    model: "P-2812HNU-F3",
    page: "/toh/zyxel/p2812hnu-f3",
    techData: "/toh/hwdata/zyxel/zyxel_p-2812hnu-f3",
    routerPath: "zyxel~zyxel_p-2812hnu-f3"
  },
  {
    brand: "ZyXEL",
    model: "NBG6817 (Armor Z2)",
    page: "/toh/zyxel/nbg6817",
    techData: "/toh/hwdata/zyxel/zyxel_nbg6817",
    routerPath: "zyxel~zyxel_nbg6817"
  },
  {
    brand: "ZyXEL",
    model: "Keenetic Omni",
    page: "/toh/zyxel/zyxel_keenetic_omni_rev._a",
    techData: "/toh/hwdata/zyxel/zyxel_omni",
    routerPath: "zyxel~zyxel_omni"
  },
  {
    brand: "ZyXEL",
    model: "NSA310",
    page: "/toh/zyxel/nsa310b",
    techData: "/toh/hwdata/zyxel/zyxel_nsa310b",
    routerPath: "zyxel~zyxel_nsa310b"
  },
  {
    brand: "ZyXEL",
    model: "NSA325",
    page: "/toh/zyxel/nsa325",
    techData: "/toh/hwdata/zyxel/zyxel_nsa325",
    routerPath: "zyxel~zyxel_nsa325"
  },
  {
    brand: "ZyXEL",
    model: "WAP3205",
    page: "/toh/zyxel/nbg419n",
    techData: "/toh/hwdata/zyxel/zyxel_wap3205_v1",
    routerPath: "zyxel~zyxel_wap3205_v1"
  },
  {
    brand: "ZyXEL",
    model: "NBG-419N",
    page: "/toh/zyxel/nbg419n",
    techData: "/toh/hwdata/zyxel/zyxel_nbg-419n_v1",
    routerPath: "zyxel~zyxel_nbg-419n_v1"
  },
  {
    brand: "ZyXEL",
    model: "P-2601HN-Fx",
    page: "/toh/zyxel/zyxel_p2601hnfx",
    techData: "/toh/hwdata/zyxel/zyxel_p-2601hn-fx",
    routerPath: "zyxel~zyxel_p-2601hn-fx"
  },
  {
    brand: "ZyXEL",
    model: "P-870HN-51b",
    page: "/toh/zyxel/p-870hn-51b",
    techData: "/toh/hwdata/zyxel/zyxel_p-870hn-51b",
    routerPath: "zyxel~zyxel_p-870hn-51b"
  },
  {
    brand: "ZyXEL",
    model: "P-870HN-53b",
    page: "/toh/zyxel/p-870hn-53b",
    techData: "/toh/hwdata/zyxel/zyxel_p-870hn-53b",
    routerPath: "zyxel~zyxel_p-870hn-53b"
  },
  {
    brand: "ZyXEL",
    model: "NBG6617",
    page: "/toh/zyxel/zyxel_nbg6617",
    techData: "/toh/hwdata/zyxel/zyxel_nbg6617",
    routerPath: "zyxel~zyxel_nbg6617"
  },
  {
    brand: "ZyXEL",
    model: "WAP6805",
    page: "/toh/zyxel/zyxel_wap6805",
    techData: "/toh/hwdata/zyxel/zyxel_wap6805",
    routerPath: "zyxel~zyxel_wap6805"
  }
];
