module.exports = {
  transpileDependencies: ["vuetify"],

  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "pt",
      localeDir: "translations",
      enableInSFC: false
    }
  }
};
